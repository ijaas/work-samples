# Work Samples

### Video Samples

- Web App: https://bit.ly/3uS3dnZ
- Mobile App: https://bit.ly/2QrxwDq
- Admin Dashboard: https://bit.ly/3dg1PWr

### Code Samples

- React JS: [Layout Elements](./samples/layout.tsx)
- React Native: [Application Wrapper Element](./samples/wrapper.tsx)
- Jquery: [Frontend Form Fields](./samples/jquery.forms.js)
- PHP: [Form Display & Processing](./samples/form.php)
