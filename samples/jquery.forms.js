; (function($) {

    var $win = $(window);

    $win.on("pageshow.form.processing", function() {
        $('.form-processing').remove();
    });

    $.fn.forms = function(lm, $input) {

        var $form, $input, $inputs;

        $form = this;
        $input = ($input) ? $input : $('');
        $inputs = $form.find(':input');

        //If new form element
        if (!$form.data('lm.form')) {

            $form.data('lm.form', 1);

            //Prevent Double Entry
            $form.on('submit.Await', function(event) {

                var item = $(this);

                if (item.hasClass('wpcf7-form')) return;

                if (item.hasClass('no-processing') || item.data('lm_processing') == 'stop' || item.data('processing') == 'no') {
                    item.removeClass('no-processing');
                    return true;
                }

                $inputs.off(".math");
                $inputs.trigger('blur');
                item.append("<div class='form-processing'><span><i class='fa fa-spinner fa-spin'></i>Please Wait</span></div>");

                return;

            });

            if (!$form.attr('enctype') && $form.attr('method') == 'post') $form.attr('enctype', 'multipart/form-data');

            $form.on('reset', function() {

                setTimeout(function() {
                    $form.find('.input-auto').trigger('typeahead:reset');
                    $form.find('.input-calendar').trigger('calendar:reset', { refresh: true });
                }, 500);

            });

            //Spam Protection

            $form.filter('.form-spam-protect').each(function() {

                var item = $(this),
                    action = item.attr('data-action'),
                    $field = item.find('#lm-protection');

                if (!action || !$field.size()) return;

                item.addClass('no-processing');

                item.on('submit.spamProtect', function(event) {

                    event.preventDefault();

                    if ($field.val()) {
                        var test = confirm("Spam Protection. Please click on CANCEL to prove you are not a robot.");
                        if (test) {
                            alert("Spam Detected. Sorry plesae refresh your browser and try again.");
                            return false;
                        }
                    }

                    item.attr('action', action);

                    setTimeout(function() {
                        item.removeClass('no-processing').off('submit.spamProtect');
                        item.trigger('submit');
                    }, 250);

                    return false;

                });

            });

        }

        //If no new input fields return;
        if (!$input.size()) return;


        $form.find('.tbl').each(function() {

            var item = $(this);
            $head = item.find('.tbl-head'),
                $wrap = item.closest('.tbl-fixed-wrap'),
                $filter = item.find('.tbl-head-filter'),
                $span = $filter.find('span').first(),
                $sort = $head.find('.row-sort'),
                $checkAll = item.find('th.row-check input'),
                $checks = item.find('td.row-check input'),
                $checksAlt = $(item.attr('data-checked'));

            //checkboxs
            $checks.change(function() {

                var item = $(this);
                if (item.prop("checked")) item.closest('tr').addClass('row-checked');
                else item.closest('tr').removeClass('row-checked');

                //Clone to alternative form
                if ($checksAlt.size()) {
                    $checksAlt.html();
                    $checksAlt.html($checks.filter(":checked").clone(1, 0));
                }

            }).trigger('change');

            $checkAll.change(function() {

                var item = $(this);
                if (item.prop("checked")) $checks.prop('checked', true).trigger('change');
                else $checks.prop('checked', false).trigger('change');

            });

            if ($sort.size()) {

                var $order = $('#field-s-order'),
                    $key = $('#field-s-meta_key'),
                    $by = $('#field-s-orderby');

                $sort.on('click', function(event) {

                    event.preventDefault();
                    var item = $(this),
                        $i = item.find('i.sort-icon'),
                        dir = $i.hasClass('fa-sort-up') ? "ASC" : "DESC",
                        data = item.attr('data-sort');

                    data = data.split(":", 2);

                    $order.val(dir);
                    $by.val(data[0]);
                    $key.val(data[1]);

                    $form.trigger('submit');

                });

            }

            if ($filter.size()) {

                var onscroll = function() {

                    var left = $wrap.scrollLeft() + ($wrap.width() / 2) - ($span.width() / 2);

                    $span.css({ paddingLeft: left });

                };

                if ($span.size()) {

                    $wrap.smartscroll(onscroll);

                }

                setTimeout(function() {
                    $input.not('.tbl-nochange, .auto-nochange').on('change changed typeahead:selected', function(e) {

                        $filter.show(500);
                        if ($span.size()) onscroll();

                    });
                    $form.on('reset', function() {

                        $filter.hide();
                        $checks.prop('checked', false).trigger('change');
                        $checkAll.prop('checked', false).trigger('change');

                    });
                }, 250);

            }

        });

        $input.filter('.no-process').on('click, keypress', function(e) {

            var item = $(this);

            item.closest('form').addClass('no-processing');
            return true;

        });

        var i = {};

        //Blank
        i.blank = function(els) {

            els.each(function(index, element) {

                var item = $(element);

            });

        };

        //Focus
        i.focus = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    $field = item.closest('.field');

                item.focus(function() {
                    $field.addClass("field-focus");
                }).blur(function() {
                    $field.removeClass("field-focus");
                });

            });

        };
        i.focus($input);

        //Input Number
        i.number = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    position = item.attr('data-p') ? item.attr('data-p') : 's',
                    text = item.attr('data-a') ? item.attr('data-a') : '',
                    deci = item.attr('data-d') ? item.attr('data-d') : '2',
                    empt = item.attr('data-e') ? item.attr('data-e') : 'zero',
                    sepe = (typeof (item.attr('data-s')) != "undefined") ? item.attr('data-s') : ',',
                    mini = item.attr('data-min') ? parseNumber(item.attr('data-min')) : '-9999999999999999999999999.99',
                    maxi = item.attr('data-max') ? parseNumber(item.attr('data-max')) : '9999999999999999999999999.99';

                text = (position == "p") ? text.trim() + " " : " " + text.trim();

                if (item.val()) {
                    item.val(parseNumber(item.val()));
                }

                if (deci && mini) {
                    //mini = parseFloat(mini).toFixed(deci);
                }
                if (deci && maxi) {
                    //maxi = parseFloat(maxi).toFixed(deci);
                }

                item.autoNumeric('init', {
                    currencySymbol: text,
                    currencySymbolPlacement: position,
                    emptyInputBehavior: empt,
                    minimumValue: mini,
                    maximumValue: maxi,
                    digitGroupSeparator: sepe,
                    decimalPlacesOverride: deci,
                    allowDecimalPadding: deci ? true : false,
                });

                item.attr('autocomplete', 'off');

                if (item.val()) {
                    item.autoNumeric('reSet');
                }

                item.on('change.math', function() {
                    item.autoNumeric('reSet');
                });

            });

        }
        i.number($input.filter(".input-number"));

        //Input Mask
        i.mask = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    mask = item.attr('data-mask') ? item.attr('data-mask') : false;

                if (!mask) return;

                item.mask(mask);

            });

        }
        i.mask($input.filter(".input-mask"));

        //Input Capitalize
        i.capitalcase = function(els) {

            els.each(function(index, element) {

                var item = $(element);

                item.on('keyup blur', function(event) {

                    var word = item.val().toLowerCase();

                    word = word[0].toUpperCase() + word.substr(1);

                    item.val(word);

                });

            });

        }
        i.capitalcase($input.filter(".input-capitalcase"));

        //Input Number Spinner
        i.spinner = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    step = parseFloat(item.attr('step') || 1),
                    $plus = $("<i class='fa fa-plus input-spinner-plus'></i>"),
                    $minus = $("<i class='fa fa-minus input-spinner-minus'></i>");

                item.wrap('<span class="input-wrap input-wrap-spinner"></span>');
                item.after($plus);
                item.after($minus);

                $plus.on('click', function(e) {

                    if ($plus.hasClass('disabled')) return item.trigger('change');
                    item.val(parseFloat(item.val() || 0) + step).trigger('change');

                });

                $minus.on('click', function(e) {

                    if ($minus.hasClass('disabled')) return item.trigger('change');
                    item.val(parseFloat(item.val() || 0) - step).trigger('change');

                });

                var onSpin = function(e) {

                    var min = parseNumber(item.attr('min')),
                        max = parseNumber(item.attr('max')),
                        val = parseNumber(item.val());

                    $plus.removeClass('disabled');
                    $minus.removeClass('disabled');

                    if (min && val <= min) $minus.addClass('disabled');
                    if (max && val >= max) $plus.addClass('disabled');

                }
                onSpin();

                item.on('change', onSpin);

            });

        }
        i.spinner($input.filter(".input-spinner"));

        //Input Retype
        i.retype = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    $parent = $(item.attr('data-parent'));

                if (!$parent.size()) return;

                var retype = function() {
                    item.val($parent.val());
                }
                retype();

                $parent.on('change.retype', retype)

            });

        };
        i.retype($input.filter('.input-retype'));

        //Select
        i.select = function(els) {

            els.each(function(index, element) {

                var item = $(this),
                    toggle = item.hasClass('input-toggle'),
                    checked = item.attr('data-checked'),
                    clear = item.attr('data-cantclear') ? 0 : 1,
                    prefix = item.attr('data-prefix') ? item.attr('data-prefix') : "#";

                if (item.attr('multiple')) {

                    item.addClass('input-select-multiple');
                    return;

                }

                item.wrap('<span class="input-wrap input-wrap-select"></span>');

                item.change(function() {

                    var s = item.find(":selected"), v = item.val(), prev = item.attr('data-v');

                    if (prev) item.removeClass('val-' + prev);
                    item.removeClass('val-').addClass('val-' + v).attr('data-v', v);

                    if (toggle) {

                        $(prefix + prev).hide(0);
                        $(prefix + v).show(0);

                    }

                })
                    .keyup(function() {

                        item.trigger('change');

                    });

                if (!item.hasClass('dont-auto-change')) {
                    item.trigger('change');
                }

                if (checked) {
                    item.val(checked).trigger('change');
                    item.removeAttr('data-checked');
                }

                if (clear) {
                    item.after('<i class="input-clear hidde fa fa-x" title="Clear"></i>');
                }

                item.next('.input-clear').on('click', function(event) {

                    event.preventDefault();
                    item.val('').trigger('change').trigger('focus');

                });

            });

        }
        i.select($input.filter("select"));

        // Input Radio
        i.radio = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    $field = item.closest('.field-radio'),
                    toggle = item.hasClass('input-toggle'),
                    checked = item.attr('data-checked'),
                    prefix = item.attr('data-prefix') ? item.attr('data-prefix') : "#";

                item.change(function() {

                    var item = $field.find(":checked"),
                        last = $field.find('input').not(':checked'); //$field.find(".checked").removeClass('checked').find('input');

                    //if(item.is(last)) last = $field.find('input').not(':checked');
                    last.closest('label').removeClass('checked');

                    if (item.size()) {
                        item.closest('label').addClass('checked');
                        if (toggle) {

                            if (last.size()) {
                                last.each(function(i, el) {
                                    $(prefix + $(el).val()).hide(0);
                                });
                            }

                            $(prefix + item.val()).show(0);

                        }
                    }
                    item.blur();

                });

                if (checked) {
                    if (checked == item.attr('value')) item.prop('checked', true).trigger('change');
                    item.removeAttr('data-checked');
                } else if (item.is(':checked')) {
                    item.prop('checked', true).trigger('change');
                }

            });

        };
        i.radio($input.filter(".input-radio"));

        // Input Checkbox
        i.checkbox = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    toggle = item.hasClass('input-toggle'),
                    checked = item.attr('data-checked'),
                    prefix = item.attr('data-prefix') ? item.attr('data-prefix') : "#";

                item.filter(":checked").closest('label').addClass('checked');
                item.change(function() {
                    item.not(":checked").closest('label').removeClass('checked');
                    item.filter(":checked").closest('label').addClass('checked');
                    if (toggle) {

                        if (item.is(":checked")) $(prefix + item.val()).show(0);
                        else $(prefix + item.val()).hide(0);

                    }
                });

                if (checked) {
                    if (checked == item.attr('value')) item.prop('checked', true).trigger('change');
                    item.removeAttr('data-checked');
                }

            });

        };
        i.checkbox($input.filter('.input-checkbox').not('.input-avoid'));

        //Elastic Textarea
        i.textarea = function(els) {

            els.each(function(index, element) {

                var item = $(element).trigger('blur');
                autosize(item);

            });

        };
        i.textarea($input.filter('textarea').not('.noelastic, .input-wysiwyg'));


        //wysiwyg Textarea
        i.wysiwyg = function(els) {

            els.each(function(index, element) {

                var item = $(element);

                item.trumbowyg({
                    btns: ['strong', 'em', '|', 'btnGrp-justify', 'btnGrp-lists', ['horizontalRule'], ['removeformat']],
                    removeformatPasted: true,
                    autogrow: true,
                    semantic: true,
                });

                item.on('tbwblur', function() {

                    var $content = $("<div />").html(item.trumbowyg('html'));

                    $content.find("p, strong, em, li").each(function() {

                        var $el = $(this);
                        if (!$.trim($el.text())) $el.remove();

                    });
                    $content.find("p, ul, ol").each(function() {

                        var $el = $(this);
                        if (!$.trim($el.text())) $el.remove();

                    });
                    $content.find("> br").each(function() {

                        $(this).remove();

                    });

                    var html = $content.html();

                    if (!$content.find('p, li').size() && html) {

                        html = '<p>' + html + '</p>';

                    } else if (html) {

                        html = html.replace(/<\/p>/gi, "</p>\n").replace(/<\/ul>/gi, "</ul>\n").replace(/<\/ol>/gi, "</ol>\n");

                        var lines = html.split("\n"),
                            output = [];

                        $.each(lines, function(i, line) {
                            line = $.trim(line);
                            if (line && line[0] != "<") {
                                line = "<p>" + line + "</p>";
                            }
                            output.push(line);
                        });

                        if (output) html = output.join('');

                    }

                    item.trumbowyg('html', html);

                });

            });

        };
        i.wysiwyg($input.filter('textarea.input-wysiwyg'));

        //Math Input
        i.math = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    val = parseNumber(item.val()),
                    $form = item.closest(".form"),
                    math = item.attr('data-math') ? $.parseJSON(item.attr('data-math')) : false;

                if (!math) return;

                item.removeClass('input-math-positive input-math-negative');
                if (val >= 0) item.addClass('input-math-positive');
                if (val < 0) item.addClass('input-math-negative');

                var evaluate = function(e, d) {

                    var val = eval(item.expr);
                    item.val(val).removeClass('input-math-positive input-math-negative');
                    item.trigger('change.math');
                    item.trigger('keyup');
                    if (val >= 0) item.addClass('input-math-positive');
                    if (val < 0) item.addClass('input-math-negative');

                };

                var create = function() {

                    var expr = "";

                    $.each(math, function(i, e) {

                        if (e == "string") {

                            expr += " " + i;

                        } else {

                            var $fields = $(i);

                            $fields.each(function() {
                                if (e) expr += " parseNumber($('#" + $(this).attr("id") + "').val()) " + e;
                            });
                            $fields
                                .off('.math', evaluate)
                                .on('change.math', evaluate)
                                .on('changed.math', evaluate);

                        }

                    });

                    item.expr = expr += " 0";
                    item
                        .off('recalculate')
                        .on('recalculate', evaluate)
                        .trigger('recalculate');

                };

                create();

                $win.on("lm:more_fields", function(e, lm) {
                    create();
                });

            });



        };
        i.math($input.filter('.input-math'));

        //Range Slider
        i.range = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    $parent = item.parent(),
                    $num = $('<span class="rangeslider__value" />'),
                    prefix = item.attr('data-prefix') || '',
                    suffix = item.attr('data-suffix') || '',
                    step = parseNumber(item.attr('step') || 1),
                    min = parseNumber(item.attr('min') || null),
                    max = parseNumber(item.attr('max') || null),
                    labels = item.attr('data-labels') || false,
                    value = item.val();

                item.rangeslider({
                    polyfill: false,
                    onInit: function() {
                        $num.html(prefix + addCommas(item.val()) + suffix);
                        $parent.find('.rangeslider__handle').append($num);

                        if (labels) {
                            $parent.find('.rangeslider').append('<div class="rangeslider__labels clearfix"><span class="rangeslider__min">' + prefix + addCommas(item.attr('min')) + suffix + '</span><span class="rangeslider__max">' + prefix + addCommas(item.attr('max')) + suffix + '</span>');

                            $parent.append('<i class="fa fa-angle-left rangeslider__down"></i><i class="fa fa-angle-right rangeslider__up"></i>');
                        }

                        $parent.find('.rangeslider__down').on('click', function(event) {

                            event.preventDefault();

                            var val = parseNumber(item.val()) - step;

                            if (min && val < min) val = min;

                            item.val(val).trigger('change');

                            return false;

                        });

                        $parent.find('.rangeslider__up').on('click', function(event) {

                            event.preventDefault();

                            var val = parseNumber(item.val()) + step;

                            if (max && val > max) val = max;

                            item.val(val).trigger('change');

                            return false;

                        });


                    },
                    onSlide: function(position, v) {
                        $num.html(prefix + addCommas(v) + suffix);
                    },
                    onSlideEnd: function(position, v) {
                        item.trigger('changed');
                    },
                });

                item.on('updateslider', function() {
                    item.rangeslider('update', true);
                });

            });

        };
        i.range($input.filter('.input-range'));

        //Date/Time Picker
        i.datepicker = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    id = item.attr('id'),
                    format = moment.parseFormat(moment().formatPHP(item.attr('data-f') || 'd/m/Y')),
                    details = moment.formatDetails(format),
                    d1 = item.attr('data-d1'),
                    d2 = item.attr('data-d2'),
                    pair = item.attr('data-pair'),
                    minDate = item.attr('data-min'),
                    maxDate = item.attr('data-max'),
                    mFirst = item.attr('data-month-first'),
                    now = moment(),
                    ori = item.prop('value'),
                    timer, newdate;

                //Auto Detech if month is first based on user language
                if (typeof (mFirst) == 'undefined') {

                    var lDate = new Date('January 20, 2000'),
                        lFormat = moment.parseFormat(lDate.toLocaleString());

                    if (lFormat[0] == 'm' || lFormat[0] == 'M') mFirst = true;

                }

                var timestamp = ori ? parseNumber(ori).toString() : false;
                if (timestamp && ori == timestamp && moment(timestamp, 'X', true).isValid()) {

                    var newdate = moment(timestamp, 'X');

                    if (newdate) {

                        ori = newdate.format(format);
                        item.val(ori);

                    }

                }

                if (ori) ori = moment(ori, newdate ? format : moment.parseFormat(ori));

                var $day = item.clone().addClass('input-date-date').removeAttr('required').attr('name', id + '-day').attr('maxlength', 2).attr('placeholder', 'DD').val(ori && ori.format('DD') || ''),
                    $month = item.clone().addClass('input-date-date').removeAttr('required').attr('name', id + '-month').attr('maxlength', 2).attr('placeholder', 'MM').val(ori && ori.format('MM') || ''),
                    $year = item.clone().addClass('input-date-date').removeAttr('required').attr('name', id + '-year').attr('maxlength', 4).attr('placeholder', 'YYYY').val(ori && ori.format('YYYY') || ''),
                    $hour = item.clone().addClass('input-date-time').removeAttr('required').attr('name', id + '-hour').attr('maxlength', 2).attr('placeholder', 'HH').val(ori && ori.format('HH') || ''),
                    $minute = item.clone().addClass('input-date-time').removeAttr('required').attr('name', id + '-minute').attr('maxlength', 2).attr('placeholder', 'MM').val(ori && ori.format('mm') || ''),
                    $second = item.clone().addClass('input-date-time').removeAttr('required').attr('name', id + '-second').attr('maxlength', 2).attr('placeholder', 'SS').val(ori && ori.format('ss') || ''),
                    size = Object.keys(details).length,
                    half = Math.ceil(size / 2);

                switch (size) {
                    case 1:
                        size = "sm-1-1 md-1-1 lg-1-1 xs-col-24";
                        half = "sm-1-1 md-1-1 lg-1-1 xs-col-24";
                    case 2:
                        size = "sm-1-2 md-1-2 lg-1-2 xs-col-12";
                        half = "sm-1-2 md-1-2 lg-1-2 xs-col-12";
                        break;
                    case 3:
                        size = "sm-1-4 md-1-4 lg-1-4 xs-col-6";
                        half = "sm-1-2 md-1-2 lg-1-2 xs-col-12";
                        break;
                    case 4:
                        size = "sm-1-4 md-1-4 lg-1-4 xs-col-6";
                        half = "sm-1-4 md-1-4 lg-1-4 xs-col-6";
                        break;
                    case 5:
                        size = "sm-1-6 md-1-6 lg-1-6 xs-col-4";
                        half = "sm-1-3 md-1-3 lg-1-3 xs-col-8";
                        break;
                    case 6:
                        size = "sm-1-3 md-1-6 lg-1-6 xs-col-8 md-col-4";
                        half = "sm-1-3 md-1-6 lg-1-6 xs-col-8 md-col-4";
                        break;
                }

                var $row = { a: $('<span class="column pad-5 ' + size + '" />'), y: $('<span class="column pad-5 ' + half + '" />') };

                item.wrap('<span class="input-wrap input-wrap-date row gutter-5 edge rev-5"></span>');
                item.hide(0);

                if (mFirst && mFirst != 0 && mFirst != 'false') {
                    if (details.month) item.before($row.a.clone().addClass('input-wrap input-wrap-date-date').prepend($month));
                    if (details.day) item.before($row.a.clone().addClass('input-wrap input-wrap-date-date').prepend($day));
                } else {
                    if (details.day) item.before($row.a.clone().addClass('input-wrap input-wrap-date-date').prepend($day));
                    if (details.month) item.before($row.a.clone().addClass('input-wrap input-wrap-date-date').prepend($month));
                }


                if (details.year) item.before($row.y.clone().addClass('input-wrap').prepend($year));
                if (details.hour) item.before($row.a.clone().addClass('input-wrap input-wrap-date-time').prepend($hour));
                if (details.minute) item.before($row.a.clone().addClass('input-wrap input-wrap-date-time').prepend($minute));
                if (details.second) item.before($row.a.clone().addClass('input-wrap').prepend($second));

                var $all = item.parent().find('.input-date-date, .input-date-time');

                $all.on('keypress', function(event) {

                    var key = event.which || event.keyCode;

                    if (key != 8 && key != 0 && key != 9 && (key < 48 || key > 57)) {
                        return false;
                    }

                }).on('keydown', function(event) {

                    var item = $(this),
                        length = parseInt(item.attr('maxlength') || 0),
                        key = event.which || event.keyCode;

                    if (key == 13) {
                        item.trigger('change');
                        $form.trigger('submit');
                        return;
                    }

                    if (length > 0 && length <= item.val().length && key != 9 && key != 16 && !item.prop('lm-selected')) {
                        item.closest('.input-wrap').next().find('input').trigger('focus');
                    }

                    if (item.prop('lm-selected')) item.prop('lm-selected', false);

                }).on('focus', function(event) {

                    var item = $(this);
                    item.prop('lm-selected', true).select();

                }).on('change', function(event) {

                    var date = moment(),
                        v = null,
                        isValid = true;

                    if (details.year) {
                        v = parseInt($year.val());
                        if (!v) isValid = false;
                        date.year(v);
                    }
                    if (details.month) {
                        v = parseInt($month.val());
                        if (!v) isValid = false;
                        date.month(v - 1);
                    }
                    if (details.day) {
                        v = parseInt($day.val());
                        if (!v) isValid = false;
                        date.date(v);
                    }
                    if (details.hour) {
                        v = parseInt($hour.val());
                        if (!v) isValid = false;
                        date.hour(v);
                    }
                    if (details.minute) {
                        v = parseInt($minute.val());
                        if (!v) isValid = false;
                        date.minute(v);
                    }
                    if (details.second) {
                        v = parseInt($second.val());
                        if (!v) isValid = false;
                        date.second(v);
                    }

                    if (date.isValid() && isValid) {
                        item.val(date.format(format));
                        $day.val(date.format('DD'));
                        $month.val(date.format('MM'));
                        $year.val(date.format('YYYY'));
                        $hour.val(date.format('HH'));
                        $minute.val(date.format('mm'));
                        $second.val(date.format('ss'));
                    } else {
                        item.val('');
                    }

                });

            });

        };
        i.datepicker($input.filter('.input-date'));

        //Calendar Picker
        i.calendar = function(els) {

            els.each(function(index, element) {

                var def = window.lmCalendarDefaults || {
                    format: 'd M Y',
                    hideDay: false,
                };

                var item = $(element).attr("readonly", "true"),
                    id = item.attr('id'),
                    format = moment.parseFormat(moment().formatPHP(item.attr('data-f') || def.format)),
                    details = moment.formatDetails(format),
                    d1 = item.attr('data-d1'),
                    d2 = item.attr('data-d2'),
                    pair = item.attr('data-pair'),
                    minYear = item.attr('data-min-year'),
                    maxYear = item.attr('data-max-year'),
                    now = moment(),
                    ori = item.prop('value'),
                    oriFormated = item.attr('data-date'),
                    filter = item.attr('data-filter') || false,
                    hideDay = item.attr('data-hide-on-day-change') || def.hideDay,
                    $next = $(item.attr('data-next'));

                var timer, newdate;

                var parseOptions = { preferredOrder: { '/': 'DMY', '.': 'DMY', '-': 'DMY' } };

                item.prop('data-moment-format', format);

                item.wrap('<span class="input-wrap input-wrap-calendar"></span>');

                item.on('calendar:reset', function(event, data) {

                    if (data && data.refresh) {
                        ori = item.prop('value');
                    }

                    if (oriFormated && moment(oriFormated, 'MMMM D YYYY h:mm:ss a', true).isValid()) {

                        var newdate = moment(oriFormated, 'MMMM D YYYY h:mm:ss a');

                        if (newdate) {

                            ori = newdate.format(format);
                            item.val(ori);

                            return;

                        }

                    }


                    var timestamp = ori ? parseNumber(ori).toString() : false;
                    if (timestamp && ori == timestamp && moment(timestamp, 'X', true).isValid()) {

                        var newdate = moment(timestamp, 'X');
                        if (newdate) {

                            ori = newdate.format(format);
                            item.val(ori);
                        }

                    }

                });
                item.trigger('calendar:reset');

                if (ori) ori = moment(ori, newdate ? format : moment.parseFormat(ori));

                if (!minYear) minYear = ((ori) ? ori.format('YYYY') : now.format('YYYY'));
                if (!maxYear) maxYear = ((ori) ? parseNumber(ori.format('YYYY')) + 10 : parseNumber(now.format('YYYY')) + 10);

                var draw = function() {

                    var v = item.val(),
                        set = (v) ? moment(v, moment.parseFormat(v)) : false,
                        val = {
                            year: set ? set.format('YYYY') : now.format('YYYY'),
                            month: set ? set.format('MM') : now.format('MM'),
                            date: set ? set.format('DD') : now.format('DD'),
                            hour: set ? set.format('HH') : '',
                            minute: set ? set.format('mm') : '',
                            second: set ? set.format('ss') : '',
                        },
                        m,
                        minDate = (m = item.attr('data-min')) ? moment(m, moment.parseFormat(m)) : moment('01-01-' + minYear, 'DD-MM-YYYY'),
                        maxDate = (m = item.attr('data-min')) ? moment(m, moment.parseFormat(m)) : moment('31-12-' + maxYear, 'DD-MM-YYYY');

                    var cal = {
                        wrap: $('<div class="date-picker row" />'),
                        //year:	$('<input type="range" min="'+minDate.format('YYYY')+'" max="'+maxDate.format('YYYY')+'" step="1" value="'+val.year+'" name="year" />').attr('data-v', val.year),
                        year: $('<input type="hidden" name="year" value="' + val.year + '" />'),
                        month: $('<input type="hidden" name="month" value="' + val.month + '" />'),
                        date: $('<input type="hidden" name="date" value="' + val.date + '" />'),
                        hour: $('<input type="range" min="0" max="23" step="1" value="' + val.hour + '" name="hour" />'),
                        minute: $('<input type="range" min="0" max="59" step="1" value="' + val.minute + '" name="minute" />'),
                        second: $('<input type="range" min="0" max="59" step="1" value="' + val.second + '" name="second" />'),
                        years: $('<div class="date-picker-years clearfix" />'),
                        months: $('<div class="date-picker-months clearfix" />'),
                        days: $('<div class="date-picker-days clearfix column sm-1-1 md-1-2 lg-1-2"><strong>Mo</strong><strong>Tu</strong><strong>We</strong><strong>Th</strong><strong>Fr</strong><strong>Sa</strong><strong>Su</strong></div>'),

                        mny: $('<div class="date-picker-month-year column sm-1-1 md-1-2 lg-1-2" />'),
                        time: $('<div class="date-picker-time column sm-1-1 md-1-1 lg-1-1" />'),
                        btns: $('<div class="date-picker-btns column sm-1-1 md-1-1 lg-1-1" />')
                    };

                    if (set) cal.wrap.addClass('date-picker-set');

                    for (n = 1; n <= 12; n++) {
                        var m = moment(n, 'M').format('MMM');
                        cal.months.append($('<a class="' + ((val.month == n) ? 'date-picker-current' : '') + '" href="javascript:void();" data-month="' + n + '">' + m + '</a>'));
                    }
                    for (n = 1; n < moment(val.year + "-" + val.month + "-01", "YYYY-MM-DD").isoWeekday(); n++) {
                        cal.days.append($('<span>&nbsp;</span>'));
                    }
                    for (n = 1; n <= moment(val.year + "-" + val.month, "YYYY-MM").daysInMonth(); n++) {
                        cal.days.append($('<a class="' + ((val.date == n) ? 'date-picker-current' : '') + '" href="javascript:void();" data-date="' + n + '">' + n + '</a>'));
                    }

                    cal.btns.append($('<a href="javascript:void();" data-date="' + now.format(format) + '"><i class="fa fa-circle-o"></i> ' + ((details.hour || details.minute || details.second) ? 'Now' : 'Today') + '</a>'));
                    cal.btns.append($('<a href="javascript:void();" data-date=""><i class="fa fa-x"></i> Clear</a>'));

                    cal.btns.append($('<a href="javascript:void();" data-close="1"><i class="fa fa-check"></i> Done</a>'));
                    //if(ori && set && set.format() != ori.format()) cal.btns.append($('<a href="javascript:void();" data-date="'+ori.format(format)+'"><i class="fa fa-undo"></i> Undo</a>'));

                    cal.years.append('<a href="javascript:void();" data-year="' + (Math.round(val.year) - 1) + '"><i class="fa fa-angle-left"></i></a>');
                    cal.years.append('<a href="javascript:void();" data-year="' + (Math.round(val.year) + 0) + '" class="date-picker-current">' + (Math.round(val.year) + 0) + '</a>');
                    cal.years.append('<a href="javascript:void();" data-year="' + (Math.round(val.year) + 1) + '"><i class="fa fa-angle-right"></i></a>');

                    if (details.month) cal.mny.append($("<div class='date-picker-month'></div>").append(cal.month).append(cal.months));
                    if (details.year) cal.mny.append($("<div class='date-picker-year'></div>").append(cal.year).append(cal.years));

                    cal.mny.append(cal.btns);

                    if (details.hour) cal.time.append($("<div class='date-picker-hour'><label>Hour:</label>").append(cal.hour));
                    if (details.minute) cal.time.append($("<div class='date-picker-minute'><label>Minute:</label>").append(cal.minute));
                    if (details.second) cal.time.append($("<div class='date-picker-second'><label>Second:</label>").append(cal.second));

                    if (details.day) cal.wrap.append(cal.date).append(cal.days);
                    cal.wrap.append(cal.mny);
                    if (details.hour || details.minute || details.second) cal.wrap.append(cal.time);

                    cal.input = cal.wrap.find(':input');

                    var $range = cal.input.filter('[type=range]');

                    if (filter) {
                        var fn = window[filter];
                        if (typeof fn === 'function') {
                            cal = fn(cal, val, item, format);
                        }
                    }

                    lm.$fieldModal.off('visible.calendar:range').on('visible.calendar:range', function() {

                        i.range($range);

                    });

                    cal.input.off('change.calendar').on('change.calendar', function(event, data) {

                        var item = $(this);
                        cal.wrap.trigger('update');

                        if (!data || (typeof (data) == 'object' && !data.stopRedraw)) {
                            cal.wrap.trigger('redraw');
                        }

                    });

                    cal.days.find('a').off('click.calendar').on('click.calendar', function(event) {

                        event.preventDefault();

                        var item = $(this),
                            date = item.attr('data-date');

                        if (!date) return;

                        if (hideDay) {

                            cal.date.val(date).trigger('change', { stopRedraw: 1 });
                            cal.btns.find('a[data-close="1"]').trigger('click.calendar');

                        } else {

                            cal.date.val(date).trigger('change');

                        }

                    });
                    cal.months.find('a').off('click.calendar').on('click.calendar', function(event) {

                        event.preventDefault();
                        var item = $(this),
                            month = item.attr('data-month');

                        if (!month) return;
                        cal.month.val(month).trigger('change');

                    })
                    cal.years.find('a').off('click.calendar').on('click.calendar', function(event) {

                        event.preventDefault();
                        var item = $(this),
                            year = item.attr('data-year');

                        if (!year) return;

                        cal.year.val(year).trigger('change');

                    });
                    cal.btns.find('a').off('click.calendar').on('click.calendar', function(event) {

                        event.preventDefault();
                        var $btn = $(this),
                            date = $btn.attr('data-date'),
                            close = $btn.attr('data-close');

                        if (typeof (date) != 'undefined') {
                            item.val(date);
                            item.trigger('changed');

                            if (hideDay) {

                                cal.btns.find('a[data-close="1"]').trigger('click.calendar');

                            } else {

                                cal.date.trigger('redraw');

                            }

                        }

                        if (close) {
                            lm.$fieldModal.trigger('forceclose');

                            if ($next.size()) {
                                $next.focus();
                            } else {
                                $inputs.eq($inputs.index(item) + 1).focus();
                            }
                        }

                    });

                    cal.wrap.on('update', function() {

                        var val = moment('01-01', 'DD MM');

                        cal.input.each(function() {

                            var item = $(this),
                                name = item.attr('name'),
                                num = parseNumber(item.val());

                            if (name == 'date') {
                                if (!num) num = 1;
                                else if (num > val.daysInMonth()) num = val.daysInMonth();
                            }
                            if (name == 'month') num = num - 1;
                            if (name) val.set(name, num);

                        });

                        item.val(val.format(format));
                        item.trigger('checkval');
                        item.trigger('changed');

                    })
                        .on('redraw', function() {

                            lm.$fieldModal.html(draw());
                            lm.$fieldModal.trigger('visible');

                        });

                    return cal.wrap;

                };


                item
                    .on('focus', function(event) {

                        clearTimeout(timer);

                        var ranges = function() {

                        }
                        if (lm.$fieldModal.data('drawn') != id) {

                            lm.$fieldModal.html(draw());
                            lm.$fieldModal.trigger('open', item);

                            var $last = lm.$fieldModal.find('a:last-child').last();

                            setTimeout(function() {
                                if (!$last.isOnScreen()) {
                                    lm.$both.animate({ "scrollTop": item.offset().top - 200 }, 250);
                                }
                            }, 500);

                        } else {

                            lm.$fieldModal.trigger('show', item);

                        }

                    })
                    .on('blur', function(event, data) {

                        timer = setTimeout(function() {

                            var val = item.val();

                            item.trigger('checkval');

                            if (!val) return;

                            var cformat = moment.parseFormat(val, parseOptions),
                                date = moment(val, cformat);

                            var unix = date.format('X'),
                                m,
                                minDate = (m = item.attr('data-min')) ? moment(m, moment.parseFormat(m)) : false,
                                maxDate = (m = item.attr('data-max')) ? moment(m, moment.parseFormat(m)) : false;

                            if (!minDate && !maxDate) return;

                            date = date.format(format);

                            if (minDate && unix < minDate.format('X')) return item.val('').trigger('checkval').reanimate('field-invalid');
                            if (maxDate && unix > maxDate.format('X')) return item.val('').trigger('checkval').reanimate('field-invalid');

                            if (date != val) item.val(date);

                            item.trigger('checkval');

                        }, 50);

                    })
                    .on('change checkval init', function(event) {

                        if (item.val()) item.removeClass('val-');
                        else item.addClass('val-');

                    })
                    .on('keyup', function(event) {
                        /*
                        var allowed = [8, 46, 13, 9, 37, 38, 39, 40];

                        if(allowed.indexOf(event.keyCode) > -1) return;

                        var val		= item.val().replace('.', ''),
                            num		= parseNumber(val);

                        var	cformat = moment.parseFormat(val, parseOptions),
                            date	= moment(val, cformat);

                        if(num){
                            num		= num.toString();
                            val		= num[0]+(num[1] || '');

                            if(num[1]) val += '/';
                            if(num[2]) val += (num[2] || '' )+(num[3] || '');
                            if(num[3]) val += '/';
                            if(num[4]) val += (num[4] || '')+(num[5] || '')+(num[6] || '')+(num[7] || '');

                            item.val(val);
                        }
                        */
                    });

                item.after('<i class="input-clear hidde fa fa-x" title="Clear"></i>');
                item.next('.input-clear').on('click', function(event) {

                    event.preventDefault();
                    item.val('').trigger('change').trigger('focus');

                });

                item.trigger('init');

            });

        };
        i.calendar($input.filter('.input-calendar'));


        //Date/Time Picker
        i.dateselect = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    id = item.attr('id'),
                    format = moment.parseFormat(moment().formatPHP(item.attr('data-f') || 'd/m/Y')),
                    details = moment.formatDetails(format),
                    d1 = item.attr('data-d1'),
                    d2 = item.attr('data-d2'),
                    pair = item.attr('data-pair'),
                    minDate = item.attr('data-min'),
                    maxDate = item.attr('data-max'),
                    mFirst = item.attr('data-month-first'),
                    years = { year: parseInt(item.attr('data-years-start') || moment().format('YYYY')), past: parseInt(item.attr('data-years-past') || 10), future: parseInt(item.attr('data-years-future') || 0) },
                    now = moment(),
                    ori = item.prop('value'),
                    timer, newdate;

                //Auto Detech if month is first based on user language
                if (typeof (mFirst) == 'undefined') {

                    var lDate = new Date('January 20, 2000'),
                        lFormat = moment.parseFormat(lDate.toLocaleString());

                    if (lFormat[0] == 'm' || lFormat[0] == 'M') mFirst = true;

                }

                if (ori) ori = moment(ori, moment.parseFormat(ori));

                var $select = $('<select class="input-select" />').attr('id', id).attr('name', item.attr('name')),
                    options = {
                        day: '<option value="">Day</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>',
                        month: '<option value="">Month</option><option value="01">Jan</option><option value="02">Feb</option><option value="03">Mar</option><option value="04">Apr</option><option value="05">May</option><option value="06">Jun</option><option value="07">Jul</option><option value="08">Aug</option><option value="09">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>',
                        year: '<option value="">Year</option>',
                        hour: '<option value="">Hour</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option>',
                        minute: '<option value="">Minute</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option>',
                        second: '<option value="">Second</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option>',
                    }

                while (years.future) {
                    options.year += '<option value="' + (years.year + years.future) + '">' + (years.year + years.future) + '</option>';
                    years.future--;
                }
                years.year = years.year - years.past;
                while (years.past) {
                    options.year += '<option value="' + (years.year + years.past) + '">' + (years.year + years.past) + '</option>';
                    years.past--;
                }

                var $day = $select.clone().addClass('input-date-date').attr('id', id + '-day').attr('name', id + '-day').attr('maxlength', 2).html(options.day).val(ori && ori.format('DD') || ''),
                    $month = $select.clone().addClass('input-date-date').attr('id', id + '-month').attr('name', id + '-month').attr('maxlength', 2).html(options.month).val(ori && ori.format('MM') || ''),
                    $year = $select.clone().addClass('input-date-date').attr('id', id + '-year').attr('name', id + '-year').attr('maxlength', 4).html(options.year).val(ori && ori.format('YYYY') || ''),
                    $hour = $select.clone().addClass('input-date-time').attr('id', id + '-hour').attr('name', id + '-hour').attr('maxlength', 2).html(options.hour).val(ori && ori.format('HH') || ''),
                    $minute = $select.clone().addClass('input-date-time').attr('id', id + '-minute').attr('name', id + '-minute').attr('maxlength', 2).html(options.minute).val(ori && ori.format('mm') || ''),
                    $second = $select.clone().addClass('input-date-time').attr('id', id + '-month').attr('name', id + '-second').attr('maxlength', 2).html(options.second).val(ori && ori.format('ss') || ''),
                    size = Object.keys(details).length,
                    half = Math.ceil(size / 2);


                switch (size) {
                    case 1:
                        size = "sm-1-1 md-1-1 lg-1-1";
                        half = "sm-1-1 md-1-1 lg-1-1";
                    case 2:
                        size = "sm-1-2 md-1-2 lg-1-2";
                        half = "sm-1-2 md-1-2 lg-1-2";
                        break;
                    case 3:
                        size = "sm-1-4 md-1-4 lg-1-4";
                        half = "sm-1-2 md-1-2 lg-1-2";
                        break;
                    case 4:
                        size = "sm-1-4 md-1-4 lg-1-4";
                        half = "sm-1-4 md-1-4 lg-1-4";
                        break;
                    case 5:
                        size = "sm-1-6 md-1-6 lg-1-6";
                        half = "sm-1-3 md-1-3 lg-1-3";
                        break;
                    case 6:
                        size = "sm-1-3 md-1-6 lg-1-6";
                        half = "sm-1-3 md-1-6 lg-1-6";
                        break;
                }

                var $row = { a: $('<span class="column pad-5 ' + size + '" />'), y: $('<span class="column pad-5 ' + half + '" />') };

                item.wrap('<span class="input-wrap input-wrap-dateselect row rev-5"></span>');
                item.hide(0);

                if (mFirst) {
                    if (details.month) item.before($row.a.clone().addClass('input-wrap input-wrap-dateselect-date').prepend($month));
                    if (details.day) item.before($row.a.clone().addClass('input-wrap input-wrap-dateselect-date').prepend($day));
                } else {
                    if (details.day) item.before($row.a.clone().addClass('input-wrap input-wrap-dateselect-date').prepend($day));
                    if (details.month) item.before($row.a.clone().addClass('input-wrap input-wrap-dateselect-date').prepend($month));
                }

                if (details.year) item.before($row.y.clone().addClass('input-wrap').prepend($year));
                if (details.hour) item.before($row.a.clone().addClass('input-wrap input-wrap-dateselect-time').prepend($hour));
                if (details.minute) item.before($row.a.clone().addClass('input-wrap input-wrap-dateselect-time').prepend($minute));
                if (details.second) item.before($row.a.clone().addClass('input-wrap').prepend($second));

                var $all = item.parent().find('.input-date-date, .input-date-time');

                i.select($all);

                $all.on('keypress', function(event) {

                    var key = event.which || event.keyCode;

                    if (key != 8 && key != 0 && key != 9 && (key < 48 || key > 57)) {
                        return false;
                    }

                }).on('keydown', function(event) {

                    var item = $(this),
                        length = parseInt(item.attr('maxlength') || 0),
                        key = event.which || event.keyCode;

                    if (key == 13) {
                        item.trigger('change');
                        $form.trigger('submit');
                        return;
                    }

                    if (length > 0 && length <= item.val().length && key != 9 && key != 16 && !item.prop('lm-selected')) {
                        item.closest('.input-wrap').next().find('input').trigger('focus');
                    }

                    if (item.prop('lm-selected')) item.prop('lm-selected', false);

                }).on('focus', function(event) {

                    var item = $(this);
                    item.prop('lm-selected', true).select();

                }).on('change', function(event) {

                    var date = moment(),
                        v = null,
                        isValid = true;

                    if (details.year) {
                        v = parseInt($year.val());
                        if (!v) isValid = false;
                        date.year(v);
                    }
                    if (details.month) {
                        v = parseInt($month.val());
                        if (!v) isValid = false;
                        date.month(v - 1);
                    }
                    if (details.day) {
                        v = parseInt($day.val());
                        if (!v) isValid = false;
                        date.date(v);
                    }
                    if (details.hour) {
                        v = parseInt($hour.val());
                        if (!v) isValid = false;
                        date.hour(v);
                    }
                    if (details.minute) {
                        v = parseInt($minute.val());
                        if (!v) isValid = false;
                        date.minute(v);
                    }
                    if (details.second) {
                        v = parseInt($second.val());
                        if (!v) isValid = false;
                        date.second(v);
                    }

                    if (date.isValid() && isValid) {
                        item.val(date.format(format));
                        $day.val(date.format('DD'));
                        $month.val(date.format('MM'));
                        $year.val(date.format('YYYY'));
                        $hour.val(date.format('HH'));
                        $minute.val(date.format('mm'));
                        $second.val(date.format('ss'));
                    } else {
                        item.val('');
                    }

                });

            });

        };
        i.dateselect($input.filter('.input-date-select'));


        //Input Mask
        i.datemask = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    mask = item.attr('data-mask') ? item.attr('data-mask') : "00/00/0000",
                    options = {};

                if (mask === "00/0000") {

                    mask = "AB/CDEF";

                    options.translation = {
                        'A': { pattern: /[0-1]/, optional: false },
                        'B': { pattern: /[0-9]/, optional: false },
                        'C': { pattern: /[1-2]/, optional: false },
                        'D': { pattern: /[0-9]/, optional: false },
                        'E': { pattern: /[0-9]/, optional: false },
                        'F': { pattern: /[0-9]/, optional: false },
                    };

                } else if (mask === "00/00/0000") {

                    mask = "AB/CD/EFGH";

                    options.translation = {
                        'A': { pattern: /[0-3]/, optional: false },
                        'B': { pattern: /[0-9]/, optional: false },
                        'C': { pattern: /[0-1]/, optional: false },
                        'D': { pattern: /[0-9]/, optional: false },
                        'E': { pattern: /[1-2]/, optional: false },
                        'F': { pattern: /[0-9]/, optional: false },
                        'G': { pattern: /[0-9]/, optional: false },
                        'H': { pattern: /[0-9]/, optional: false },
                    };

                } else if (mask === "00/00") {

                    mask = "AB/CD";

                    options.translation = {
                        'A': { pattern: /[0-3]/, optional: false },
                        'B': { pattern: /[0-9]/, optional: false },
                        'C': { pattern: /[0-1]/, optional: false },
                        'D': { pattern: /[0-9]/, optional: false },
                    };

                } else if (mask === "0000") {

                    mask = "ABCD";

                    options.translation = {
                        'A': { pattern: /[1-2]/, optional: false },
                        'B': { pattern: /[0-9]/, optional: false },
                        'C': { pattern: /[0-9]/, optional: false },
                        'D': { pattern: /[0-9]/, optional: false },
                    };

                }

                if (!mask) return;

                item.mask(mask, options);

            });

        }
        i.datemask($input.filter(".input-datemask"));

        i.upload = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    name = item.attr('data-name'),
                    prepend = item.attr('data-prepend'),
                    $drop = item.closest('.dropzone, .dropzone-basic'),
                    $list = $(item.attr('data-list')),
                    limit = item.attr('data-limit') || null,
                    values = item.data('values') || {},
                    type = item.attr('data-filetype'),
                    sample;

                if (type == 'image') type = /(\.|\/)(gif|jpe?g|png)$/i;
                if (type == 'video') type = /(\.|\/)(mp4|webm|avi)$/i;
                if (type == 'epub') type = /(\.|\/)(epub)$/i;
                if (type == 'pdf') type = /(\.|\/)(pdf)$/i;

                /*
                $drop.on('drop dragover', function(event) {
                    event.preventDefault();
                });
                */

                if (item.attr('data-multiple')) item.attr('multiple', true);

                $drop.find('a').on('click', function(event) {
                    event.preventDefault();
                    $drop.find('.input-upload').trigger('click');
                });

                item
                    .on('fileuploadadd', function(event, data) {

                        data.el = $('<div class="file-upload-item"><strong class="file-upload-name">' + data.files[0].name + '</strong><span class="file-upload-status">Waiting...</span><a class="file-upload-cancel" href="#"><i class="fa fa-x"></i></a></div>');
                        data.el_status = data.el.find('.file-upload-status');

                        data.el.find('.file-upload-cancel').on('click', function(event) {

                            event.preventDefault();
                            if (jqXHR) jqXHR.abort();
                            data.el.slideUp(500, function() {
                                data.el.remove();
                            });

                        });

                        $drop.append(data.el);

                        data.el.slideDown(500);

                        var jqXHR = data.submit();

                    })
                    .on('fileuploadstart', function(event, data) {

                        $form.addClass('form-uploading');

                    })
                    .on('fileuploadprogress', function(event, data) {

                        var progress = parseInt(data.loaded / data.total * 100, 10);

                        if (!data.el_progress) {
                            data.el_status.html('<span class="file-upload-progress-wrap"><span class="file-upload-progress"><span class="file-upload-progress-done"></span></span></span>');
                            data.el_progress = data.el.find('.file-upload-progress-done');
                        }

                        data.el_progress.width(progress + '%');

                    })
                    .on('fileuploadprogressall', function(event, data) {

                        var progress = parseInt(data.loaded / data.total * 100, 10);

                        if (progress >= 100) {
                            $form.removeClass('form-uploading');
                        }

                    })
                    .on('fileuploaddone', function(event, data) {

                        data.el.addClass('file-upload-complete');
                        data.el_status.html('Complete');
                        setTimeout(function() {
                            data.el.slideUp(500);
                        }, 3000);

                        $list.trigger('lm:forms:upload:new', data.result);

                    })
                    .on('fileuploadstop', function(event, data) {

                        if (data.el) {
                            data.el.addClass('file-upload-cancelled');
                            data.el_status.html('Cancelled');
                            setTimeout(function() {
                                data.el.slideUp(500);
                            }, 3000);
                        }

                    })
                    .on('fileuploadfail', function(event, data) {

                        data.el.addClass('file-upload-failed');
                        data.el_status.html('Failed');

                        lm.$win.trigger('lm:fileuploadfail', data);

                    });

                item.fileupload({
                    dropZone: $drop,
                    url: lm.options.uploadUrl || lm.options.url,
                    dataType: 'json',
                    paramName: lm.options.uploadParam || 'files',
                    autoUpload: false,
                    formData: { 'fileupload': '1' },
                    maxFileSize: item.attr('data-filesize') || 20000000,
                    acceptFileTypes: type,
                    limitMultiFileUploads: limit,
                    limitConcurrentUploads: 3,
                });

                if (!$list.size()) return;

                sample = Hogan.compile($list.html());
                $list.html('').removeClass('hidden');

                $list
                    .on('lm:forms:upload:new', function(event, data) {

                        if (typeof lm.options.uploadDataFilter == 'function') data = lm.options.uploadDataFilter(data);

                        data.fieldname = name;

                        var clone = sample.render(data),
                            $el = $(clone);

                        $el.find('img').each(function() {
                            var $img = $(this);
                            if (!$img.attr('src')) $img.attr('src', $img.attr('data-src'));
                        });

                        $el.find('.file-list-item-controls a').on('click', function(event) {

                            event.preventDefault();
                            var $a = $(this),
                                action = $a.attr('data-action');

                            switch (action) {
                                case "delete":
                                    $el.slideUp(500, function() {
                                        $el.remove();
                                        $list.trigger('lm:forms:upload:updated');
                                    });
                                    break;
                                case "up":
                                    var $prev = $el.prev();
                                    if (!$prev.size()) return;
                                    $prev.before($el);
                                    $list.trigger('lm:forms:upload:updated');
                                    break;
                                case "down":
                                    var $next = $el.next();
                                    if (!$next.size()) return;
                                    $next.after($el);
                                    $list.trigger('lm:forms:upload:updated');
                                    break;
                            }

                        });

                        if (!prepend || data.append) $list.append($el);
                        else $list.prepend($el);

                        $list.trigger('lm:forms:upload:updated');

                    })
                    .on('lm:forms:upload:updated', function(event) {

                        if (limit > 0) {

                            $list.find('.file-list-item input').each(function(index, el) {

                                $(this).removeAttr('disabled');
                                if (index >= limit) $(this).attr('disabled', 'disabled');

                            });

                        }

                    });

                $.each(values, function(i, data) {

                    if (data) {
                        data.append = true;
                        $list.trigger('lm:forms:upload:new', data);
                    }

                });

                if (limit > 0 && $list.size()) {

                    lm.$body.append("<style type='text/css'>#" + $list.attr('id') + " .file-list-item:nth-child(n+" + (parseInt(limit) + 1) + ") {opacity:0.4 !important;}</style>");

                }

            });

        };
        i.upload($input.filter('.input-upload'));

        i.fileupload = function(els) {

            els.each(function(index, element) {

                var item = $(element),
                    placeholder = item.attr('data-placeholder') || '',
                    inputclass = item.attr('data-inputclass') || '',
                    text = item.attr('data-text') || 'Browse',
                    btnclass = item.attr('data-btnclass') || 'btn',
                    id = item.attr('id') || '',
                    icon = item.attr('data-icon') || '',
                    $input = $("<input type='text' disabled class='input-text input-fileupload-input " + inputclass + "' placeholder='" + placeholder + "' />"),
                    $button = $("<label for='" + id + "' class='input-fileupload-button " + btnclass + "'>" + text + "</label>");

                if (icon) {
                    $button.addClass('btn-icon').prepend("<i class='" + icon + "'></i>");
                }

                item.wrap('<span class="input-wrap"></span>');
                item.before($input);
                item.wrap($button);

                item.on('change', function() {
                    $input.val(item.val());
                });
                item.trigger('change');

            });

        };
        i.fileupload($input.filter('.input-fileupload'));

        //AutoFill
        i.autofill = function(els) {

            els.each(function(index, element) {

                var item = $(this),
                    data = item.prop('data-fill') ? JSON.parse(item.prop('data-fill')) : [];

                if (!data) return;

                item.on('change', function() {

                    var key = item.val() || '0',
                        fill = data[key];

                    if (!fill) return;

                    $.each(fill, function(id, value) {

                        $(id).val(value).trigger('autofill').trigger('change').trigger('changed');

                    });

                });

            });

        }
        i.autofill($input.filter(".input-autofill"));


        //Autocomplete Setup / Init	/ Execute
        i.autocompleteSetup = function(els) {

            if (!window.autocompleteLoaded) return;

            els.each(function(index, element) {

                var item = $(this),
                    store = window.localStorage ? window.localStorage : false,
                    time = parseInt(new Date().getTime() / 1000);

                if (!window.autocompleteCache) window.autocompleteCache = {};
                if (!window.okToUse) window.okToUse = {};

                if (store && window.fileModTimes) {

                    //files that can be used from localstorage
                    $.each(window.fileModTimes, function(file, modtime) {

                        var savetime = parseInt(localStorage_getTime(file));
                        if (time - savetime < modtime) window.okToUse[file] = 1;

                    });

                }

                var request = [];
                item.each(function(index, element) {

                    var item = $(this),
                        url = item.attr('data-prefetch') ? item.attr('data-prefetch') : item.attr('data-url');

                    if (url && window.autocompleteCache[url]) {
                        item.prop('data-local', window.autocompleteCache[url]);
                        item.attr('data-url', url).removeAttr('data-prefetch');
                    } else if (url && store && store.getItem(url) && window.okToUse[url]) {
                        item.prop('data-local', JSON.parse(store.getItem(url)));
                        item.attr('data-url', url).removeAttr('data-prefetch');
                    } else if (url) {
                        request.push(url);
                    }

                });

                request = request.unique();

                if (request.length) {

                    $.ajax({
                        url: options.data_url + 'multi.php',
                        type: "POST",
                        data: { data: request },
                        dataType: 'json',
                    }).done(function(data) {

                        item.each(function(index, element) {
                            var item = $(this),
                                url = item.attr('data-prefetch');

                            if (url && data[url]) {
                                window.autocompleteCache[url] = data[url];
                                if (store) localStorage_setTime(url, JSON.stringify(data[url]), parseInt(new Date().getTime() / 1000) + 2);
                                item.prop('data-local', data[url]);
                                item.attr('data-url', url).removeAttr('data-prefetch');
                            }

                        });
                        i.autocompleteInit(item);

                    }).error(function() {

                        i.autocompleteInit(item);

                    });

                } else {

                    i.autocompleteInit(item);

                }

                /**/

            });

        }

        i.autocompleteInit = function(els) {

            $(els).each(function() {

                var item = $(this);

                item.wrap('<span class="input-wrap input-wrap-select"></span>');

                if (item.val() || item.attr('data-init')) return i.autocomplete(item);

                item.on('focus.autocompleteInit', function() {

                    item.off('focus.autocompleteInit');
                    item.off('autocompleteInit');
                    item.addClass('auto-focus');
                    i.autocomplete(item);

                }).on('autocompleteInit', function() {

                    item.off('focus:autocompleteInit');
                    item.off('typeahead:initialized.autocompleteInit');
                    item.off('autocompleteInit');
                    i.autocomplete(item);

                }).on('typeahead:initialized.autocompleteInit', function() {

                    item.off('typeahead:initialized.autocompleteInit');
                    item.trigger('focus');
                    setTimeout(function() { item.trigger('click.auto') }, 250);

                });

            });

        };

        i.autocomplete = function(els) {

            setTimeout(function() {

                els.each(function(index, element) {

                    var item = $(this);

                    if (item.hasClass('tt-query')) item.typeahead('destroy');
                    item.off('.auto');

                    var tokenize = function(obj) {

                        if (typeof (obj) == 'object') {

                            var tokens = [];

                            $.each(obj, function(a, b) {
                                tokens = tokens.concat(Bloodhound.tokenizers.whitespace(b));
                            })

                            return tokens;

                        }

                        return Bloodhound.tokenizers.whitespace(obj);

                    }

                    var id = item.attr("id").split("-"),
                        name = id[id.length - 1],
                        strict = item.attr("data-strict"),
                        length = item.attr("data-length") || 1,
                        limit = item.attr("data-limit") || 200,
                        display = item.attr("data-display") || 'label',
                        template = item.attr("data-template") || '<p><i class="fa fa-circle-empty"></i>{{value}}</p>',
                        local = item.prop('data-local') ? item.prop('data-local') : item.attr('data-local'),
                        prefetch = item.attr('data-prefetch'),
                        remote = item.attr('data-remote'),
                        data = { 'initialize': false, 'datumTokenizer': tokenize, 'queryTokenizer': Bloodhound.tokenizers.whitespace, 'limit': limit, 'templates': { 'suggestion': template } };

                    if (local) {
                        data.local = local;
                    }
                    if (remote) {
                        data.remote = { wildcard: '%QUERY', url: remote };
                    }
                    if (prefetch) {
                        data.prefetch = prefetch;
                    }

                    if (!data.local && !data.prefetch && !data.remote) return;

                    var dataset = new Bloodhound(data);

                    item.typeahead({ minLenght: length }, { 'name': name, 'source': dataset, 'display': display });

                    var promise = dataset.initialize();

                    var tt = item.data('tt-typeahead'),
                        ds = tt.menu.datasets[0];

                    lm.$body.append(tt.menu.$node.detach());

                    promise.done(function() {

                        if (item.hasClass('auto-focus')) {
                            setTimeout(function() {
                                item.trigger('focus');
                                item.trigger('click');
                            }, 50);
                        };

                        if (item.attr('data-autoselect')) {
                            tt.input.setQuery(item.attr('data-autoselect'));
                            item.attr('data-autoselect', "").addClass('auto-nochange');
                        };

                        var onfind = function(s) {
                            //console.log(s);
                        }
                        //dataset.search('a', onfind, onfind);

                    });

                    item.on('typeahead:open.auto', function() {

                        var off = item.offset(),
                            h = item.outerHeight();

                        tt.menu.$node.css({ width: item.outerWidth(), left: off.left, top: off.top + h - parseFloat(lm.$html.css('marginTop')) });

                    });

                    item.on('typeahead:close.auto', function() {

                        if (!tt.menu._allDatasetsEmpty()) tt.menu.empty();

                    });

                    item.on('click.auto', function() {

                        if (!tt.menu._allDatasetsEmpty()) return;

                        var suggestions = dataset.all();
                        ds._overwrite('', suggestions);

                    });

                    item.after('<i class="input-clear hidde fa fa-x" title="Clear"></i>');
                    item.next('.input-clear').on('click', function(event) {

                        event.preventDefault();
                        if ($hide && $hide.size()) $hide.val('');
                        item.typeahead('val', '').trigger('typeahead:clear');
                        item.trigger('click.auto').trigger('focus');

                    });

                    item.on('keyup blur typeahead:active typeahead:change typeahead:select typeahead:clear', function(e) {

                        if (item.val()) item.removeClass('val-');
                        else item.addClass('val-');

                    });

                    if (strict) {

                        var timer = false;
                        var $hide = $("#" + item.attr("id") + "-hidden");

                        item.on('typeahead:select.auto typeahead:autocomplete.auto', function(e, a) {

                            if (!$hide.size()) return;

                            var v = a.id || a.value || ds.displayFn(a),
                                l = ds.displayFn(a);

                            if (l != item.val()) item.val(l);

                            $hide.val(v).trigger('typeahead:change');

                            clearTimeout(timer);
                            setTimeout(function() {
                                clearTimeout(timer);
                            }, 25);
                            setTimeout(function() {
                                clearTimeout(timer);
                            }, 50);

                        });

                        item.on('typeahead:reset', function() {

                            item.val('');
                            $hide.val('');

                        });

                        item.on('change.auto', function(e) {

                            timer = setTimeout(function() {

                                var val = item.val(),
                                    found = 0;

                                if (!val && !$hide.val()) return;

                                var innerTimer = setTimeout(function() {

                                    if (found) return;
                                    item.removeClass('auto-nochange');
                                    tt.input.setQuery('');
                                    item.val('');
                                    $hide.val('');

                                }, 500);

                                if (val) {

                                    var onfind = function(suggestions) {

                                        var top = suggestions[0];

                                        if (!top) return;

                                        if (((top.id && val == top.id) || (top.value && val.toLowerCase() === top.value.toLowerCase()) || (top.label && val.toLowerCase() === top.label.toLowerCase()))) {

                                            found = 1;
                                            tt.input.setQuery(ds.displayFn(top));
                                            item.val(ds.displayFn(top));
                                            item.removeClass('auto-nochange');
                                            item.trigger('typeahead:select', top);

                                        }

                                    }
                                    dataset.search(val, onfind, onfind);

                                }

                            }, 150);


                        });

                        item.addClass('auto-nochange');
                        item.trigger('change.auto');

                    }

                });

            }, 10);

        }
        i.autocompleteSetup($input.filter(".input-auto"));

    }

    $win
        .on('lm:middle.forms', function(event, lm) {

            var $form, $input;

            $form = lm.item.find('form.form');
            $input = false;

            if (!$form.size()) {

                $form = lm.item.closest('form.form');
                $input = lm.item.find(':input');

            }

            //.tbl .textarea height
            lm.item.find('.tbl textarea').attr('rows', '1');

            //extract sample data

            $form.each(function(index, element) {

                var item = $(element),
                    $fields = ($input === false) ? item.find(':input') : $input;

                item.forms(lm, $fields);

            });

            return;

        })
        .on('lm:middle.forms:modal', function(event, lm) {

            var timer = false;
            lm.$fieldModal = lm.$body.find('.field-modal');

            if (lm.$fieldModal.size()) return;

            lm.$body.append('<div class="field-modal" />');
            lm.$fieldModal = lm.$body.find('.field-modal');

            //Add Triggers
            lm.$fieldModal
                .on('show', function(event, element) {

                    clearTimeout(timer);

                    var item = $(element),
                        offset = item.offset(),
                        height = item.outerHeight() - lm.$body.offset().top;

                    if (lm.$win.width() <= 600) offset.left = '0';

                    lm.$fieldModal.css({ 'opacity': 100, 'z-index': 99999999, 'transform': 'translateX(' + offset.left + 'px) translateY(' + (offset.top + height) + 'px)' }); //offset.top + height
                    lm.$fieldModal.trigger('visible');
                    item.trigger('modal:opened');

                })
                .on('open', function(event, element) {

                    clearTimeout(timer);

                    var item = $(element),
                        id = item.attr('id');

                    lm.$fieldModal.trigger('show', element);
                    lm.$fieldModal.data('drawn', id);

                    item.off('blur.fieldModal').on('blur.fieldModal', function(event) {

                        lm.$fieldModal.trigger('close', element);

                    });

                    lm.$win.off('mousedown.fieldModal').on('mousedown.fieldModal', function(event) {

                        lm.$fieldModal.trigger('close', element);

                    });

                    lm.$fieldModal.off('mousedown.fieldModal').on('mousedown.fieldModal', function(event) {

                        setTimeout(function() {
                            clearTimeout(timer);
                        }, 10);

                    });
                    lm.$fieldModal.off('mouseup.fieldModal').on('mouseup.fieldModal', function(event) {

                        item.trigger('focus');
                        setTimeout(function() {
                            clearTimeout(timer);
                        }, 10);

                    });

                })
                .on('close', function(event, element, force) {

                    clearTimeout(timer);

                    var item = $(element);
                    timer = setTimeout(function() {

                        if (item.is(':focus') && !force) return;
                        lm.$fieldModal.trigger('forceclose', element);

                    }, 50);

                }).on('forceclose', function(event, element) {

                    clearTimeout(timer);

                    var item = $(element);
                    var offset = lm.$fieldModal.offset();

                    if (offset.top + lm.$fieldModal.height() > lm.$body.height()) offset.top = lm.$body.height() - lm.$fieldModal.height() - 20;

                    lm.$fieldModal.data('drawn', '');
                    lm.$fieldModal.off('visible');
                    lm.$fieldModal.css({ 'opacity': '0', 'z-index': 0, 'transform': 'translateX(' + offset.left + 'px) translateY(' + (offset.top + 20) + 'px)' });
                    $win.off('mousedown.fieldModal');
                    item.trigger('modal:closed');

                });

        })
        .on('lm:middle.forms:validation', function(event, lm) {

            $('.field-invalid').removeClass('.field-invalid');
            $('.field-valid').removeClass('.field-valid');

            lm.item.find('.validation').each(function() {

                var item = $(this),
                    cls = (item.attr('data-type') == "error") ? "invalid" : item.attr('data-type'),
                    fields = $.trim(item.text());

                try {
                    $(fields).addClass('field-' + cls).closest('.field').addClass('field-' + cls);
                } catch (e) {
                    console.warn(e);
                }

            });

        })
        .on('lm:begin.forms:selectme', function(event, lm) {

            lm.item.find(".selected").each(function() {

                var item = $(this),
                    id = item.attr('id'),
                    max = item.attr('data-max'),
                    key = item.attr('data-key'),
                    top = item.attr('data-top'),
                    autoadd = item.attr('data-autoadd'),
                    $end = item.find('.selected-end').first(),
                    $ex = (item.attr('data-exclude')) ? $(item.attr('data-exclude')) : false,
                    $wrap = item.closest('table, .selected-wrap'),
                    $sample = item.find('.sample').first(),
                    sample, n;

                if (!$sample.size()) return;

                if (!id) {
                    id = 'selected-' + Math.floor(Math.random() * 1000 + 1);
                    item.attr('id', id);
                }

                if ($end.size()) {
                    $end.find('.select-from').attr('data-parent', '#' + id);
                    if ($wrap.size()) {
                        $end.remove();
                        $wrap.after($('<table style="width:100;" />').prepend($end));
                    }
                }

                $sample.find('.existingOnly').remove();
                sample = Hogan.compile($('<div>').append($sample.clone().removeClass('sample hidden hideme')).html().replace(/NNN/g, '{{n}}'));
                $sample.detach();

                item.find('.selected-item').each(function(index, element) {
                    $(this).attr('data-n', index + 1);
                });

                item.on('lm:selected', function(event, data) {

                    if (event.target != item[0]) return;

                    if (!data) data = {};
                    data.n = -1;

                    var $s = item.find('.selected-item');

                    $s.each(function(i) {
                        var no = parseFloat($(this).attr('data-n'));
                        if (no > data.n) data.n = no;
                    });
                    data.n = data.n + 1;

                    if (typeof (data.n) != 'number' || data.n <= 0) data.n = 1;
                    if (key) data.key = key.replace(/NNN/gi, data.n);

                    var clone = sample.render(data),
                        $el = $(clone);

                    if (top) item.prepend($el);
                    else item.append($el);

                    $el.attr('data-n', data.n);

                    if (max && max >= $s.size()) {
                        for (i = 0; i < max; i++) {
                            $s.first().remove();
                        }
                    }

                    var _lm = lm;
                    _lm.item = $el;

                    $el.lmStart(_lm);
                    lm.$win.trigger('lm:more_fields', _lm);

                    if ($ex && $ex.size()) {

                        var val = $ex.val().split(',');
                        val.push(data.ID);
                        $ex.val(val.join(',')).trigger('change');

                    }

                });

            });

            lm.item.find(".selected-rows").each(function() {

                var item = $(this),
                    val = item.val().split(','),
                    $parent = item.closest('form');

                $.each(val, function(index, id) {
                    $parent.find("#row-id-" + id).addClass('row-selected');
                });

            });

            lm.item.find('.select-from').on('click.select', function(event) {

                event.preventDefault();

                var item = $(this),
                    href = item.attr('href') || item.attr('data-href'),
                    $parent = item.attr('data-parent') ? $(item.attr('data-parent')) : item.closest('.selected'),
                    suffix = item.attr('data-suffix'),
                    count = item.attr('data-count') || 1,
                    values = item.attr('data-values') ? $.parseJSON(item.attr('data-values')) : false;

                lm.$selectTo = $parent;

                if (item.hasClass('select-new')) {

                    var defaults = item.attr('data-defaults') ? $.parseJSON(item.attr('data-defaults')) : {};

                    var n = parseInt(count) || 1;

                    while (n) {
                        n--;
                        $parent.trigger('lm:selected', defaults);
                    }

                    return;

                }

                if (typeof (suffix) == "string") href += suffix;

                if (typeof (values) == "object") {
                    var vals = "";
                    $.each(values, function(i, e) {
                        var $e = $(e);
                        if ($e.size() > 1) {
                            var vs = [];
                            $e.each(function() {
                                vs.push($(this).val());
                            });
                            vals += i + "=" + vs.join(",");
                        } else if ($e.size() === 1) {
                            vals += i + "=" + $e.val();
                        }
                    });
                    href += vals;
                }

                lm.$win.trigger('lm:remodal', { 'href': href, 'ajax': true });

            });

            lm.item.find('.select-me').on('click.select', function(event) {

                event.preventDefault();

                var item = $(this),
                    href = item.attr('data-link'),
                    ID = item.attr('data-ID'),
                    $parent = $('form');

                if (!href) return;

                item.closest('tr').addClass('row-selected');
                item.removeAttr('data-link');

                if (ID) {
                    $parent.find('.wp-paginate a').each(function() {

                        var item = $(this),
                            href = $('<div />').html(item.attr('href')).text();

                        item.attr('href', item.attr('href').replace('[s][selected]=', '[s][selected]=' + ID + ',').replace('%5Bs%5D%5Bselected%5D=', '%5Bs%5D%5Bselected%5D=' + ID + '%2C'));

                    });
                }

                if (typeof (href) == 'string') {

                    var load = function() {

                        $.ajax(href, {
                            type: "GET",
                            data: { json: true },
                            datatype: 'json',
                        }).done(function(data) {

                            return lm.$selectTo.trigger('lm:selected', $.parseJSON(data));

                        });

                    };

                    if (window.Pace) window.Pace.ignore(load);
                    else load();

                }
                if (typeof (href) == 'object') {

                    return lm.$selectTo.trigger('lm:selected', href);

                }

            });

            lm.item.find('.select-all').on('click.select', function(event) {

                event.preventDefault();

                var item = $(this);

                item.closest('table').find('.select-me').trigger('click');
                item.closest('tr').addClass('row-selected');

            });

            lm.item.find(".selected-import").each(function() {

                var item = $(this),
                    $table = item.attr('data-table'),
                    $inputs = item.find('textarea'),
                    $button = item.find('button');

                $button.on('click', function(event) {

                    event.preventDefault();

                    var rows = [];

                    $inputs.each(function() {

                        var $input = $(this),
                            column = $input.attr('data-column'),
                            value = $input.val(),
                            lines = $.trim(value).split("\n");

                        $input.val('');

                        $.each(lines, function(i, v) {

                            if (!rows[i]) {

                                rows[i] = {};
                                rows[i][column] = v;

                            } else {

                                rows[i][column] = v;

                            }

                        });

                    });

                    var total = rows.length,
                        n = 0;

                    item.prepend("<div class='form-processing'><span><i class='fa fa-spinner fa-spin'></i>Please Wait (<strong class='n'>0</strong> of " + total + ")</span></div>");

                    var $n = item.find('.form-processing .n');

                    setInterval(function() {

                        $n.text(n + 1);

                        var row = rows[n];

                        if (row) {
                            lm.$selectTo.trigger('lm:selected', row);
                        }

                        if (n >= total) {

                            item.find('.form-processing').remove();

                            if (lm.modal) {
                                lm.modal.close();
                            }

                        }

                        n++;

                    }, 500);

                });


            });

        })
        .on('lm:middle.forms:buttons', function(event, lm) {

            lm.item.find(".btn-reset").on('click', function(event) {

                event.preventDefault();

                var item = $(this);
                $form = $(this).attr('data-form') ? $($(this).attr('data-form')) : $(this).closest('form');

                lm.$win.trigger('lm:form.reset', $form);

            });

            lm.item.find(".btn-remove").on('click', function(event) {

                event.preventDefault();

                var item = $(this);
                $target = $(item.attr('href') || item.attr('data-href'));

                $target.remove();

            });

        })
        .on('lm:middle.forms:conditional', function(event, lm) {

            // for multiple values use | as deliminator
            // for no eqauls to use "^(?!XYZ)"

            lm.item.find('.conditional').each(function() {

                var item = $(this),
                    cond = item.attr('data-conditions'),
                    size = 0;

                if (!cond) return;

                cond = $.parseJSON(cond);
                size = Object.keys(cond).length;

                if (!size) return;

                var test = function() {

                    var matches = matched = 0;

                    $.each(cond, function(fields, expr) {

                        var $field = $(fields);
                        if (!$field.size()) return;

                        matched = 0;

                        $field.each(function(i, field) {

                            if (matched) return;
                            var $f = $(field),
                                val = $f.val(),
                                radio = $f.is('[type=radio], [type=checkbox]');

                            if (radio && (expr == 'true' || expr == 'false')) {

                                if (expr == 'true' && $f.is(':checked')) matched++;
                                else if (expr == 'false' && !$f.is(':checked')) matched++;;

                            } else {

                                if (radio && !$f.is(":checked")) return;

                                if (expr) {
                                    expr = new RegExp(expr, 'gi');
                                    if (val && val.match(expr)) matched++;
                                } else {
                                    if (!val) matched++;
                                }

                            }

                        });

                        if (matched) matches++;

                    });

                    // if(matches == size) item.show();
                    // else item.hide();

                    if (matches == size) {

                        item.show();

                        item.find(':input').not(item.find('.conditional :input')).each(function() {

                            var $input = $(this),
                                name = $input.attr('data-name'),
                                req = $input.attr('data-required');

                            if (name && !$input.attr('name')) $input.attr('name', name);
                            if (req && req != 'undefined') $input.attr('required', 'required');

                        });

                    } else {

                        item.hide();

                        item.find(':input').each(function() {

                            var $input = $(this),
                                name = $input.attr('name'),
                                req = $input.attr('required');

                            if (name) $input.attr('name', '').attr('data-name', name);
                            if (req && req != 'undefined') $input.removeAttr('required').attr('data-required', true);

                            $input.removeAttr('aria-invalid');

                            $input.next('.error').remove();

                        });
                    }

                };
                test();

                $.each(cond, function(fields, expr) {
                    $(fields).on('change changed typeahead:selected typeahead:change typeahead:clear', test);
                });

            });

        })
        .on('lm:form.reset', function(event, element) {

            var item = $(element);
            if (item.is('form')) item.trigger('reset');

        })
        .on('lm:form.clear', function(event, element) {

            var item = $(element);

            item.find(':input').each(function() {
                switch (this.type) {
                    case 'checkbox':
                    case 'radio':
                        this.checked = false;
                    default:
                        $(this).val('');
                        break;
                }
                $(this).trigger('change');
            });

            if (item.is('form')) item.trigger('reset', true);

        });


    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };


}(jQuery));

/*!
    Autosize 3.0.17
    license: MIT
    http://www.jacklmoore.com/autosize
*/
!function(e, t) { if ("function" == typeof define && define.amd) define(["exports", "module"], t); else if ("undefined" != typeof exports && "undefined" != typeof module) t(exports, module); else { var n = { exports: {} }; t(n.exports, n), e.autosize = n.exports } }(this, function(e, t) { "use strict"; function n(e) { function t() { var t = window.getComputedStyle(e, null); "vertical" === t.resize ? e.style.resize = "none" : "both" === t.resize && (e.style.resize = "horizontal"), l = "content-box" === t.boxSizing ? -(parseFloat(t.paddingTop) + parseFloat(t.paddingBottom)) : parseFloat(t.borderTopWidth) + parseFloat(t.borderBottomWidth), isNaN(l) && (l = 0), a() } function n(t) { var n = e.style.width; e.style.width = "0px", e.offsetWidth, e.style.width = n, e.style.overflowY = t, r() } function o(e) { for (var t = []; e && e.parentNode && e.parentNode instanceof Element;)e.parentNode.scrollTop && t.push({ node: e.parentNode, scrollTop: e.parentNode.scrollTop }), e = e.parentNode; return t } function r() { var t = e.style.height, n = o(e), r = document.documentElement && document.documentElement.scrollTop; e.style.height = "auto"; var i = e.scrollHeight + l; return 0 === e.scrollHeight ? void (e.style.height = t) : (e.style.height = i + "px", s = e.clientWidth, n.forEach(function(e) { e.node.scrollTop = e.scrollTop }), void (r && (document.documentElement.scrollTop = r))) } function a() { r(); var t = window.getComputedStyle(e, null), o = Math.round(parseFloat(t.height)), i = Math.round(parseFloat(e.style.height)); if (o !== i ? "visible" !== t.overflowY && n("visible") : "hidden" !== t.overflowY && n("hidden"), u !== o) { u = o; var a = d("autosize:resized"); e.dispatchEvent(a) } } if (e && e.nodeName && "TEXTAREA" === e.nodeName && !i.has(e)) { var l = null, s = e.clientWidth, u = null, c = function() { e.clientWidth !== s && a() }, p = function(t) { window.removeEventListener("resize", c, !1), e.removeEventListener("input", a, !1), e.removeEventListener("keyup", a, !1), e.removeEventListener("autosize:destroy", p, !1), e.removeEventListener("autosize:update", a, !1), i["delete"](e), Object.keys(t).forEach(function(n) { e.style[n] = t[n] }) }.bind(e, { height: e.style.height, resize: e.style.resize, overflowY: e.style.overflowY, overflowX: e.style.overflowX, wordWrap: e.style.wordWrap }); e.addEventListener("autosize:destroy", p, !1), "onpropertychange" in e && "oninput" in e && e.addEventListener("keyup", a, !1), window.addEventListener("resize", c, !1), e.addEventListener("input", a, !1), e.addEventListener("autosize:update", a, !1), i.add(e), e.style.overflowX = "hidden", e.style.wordWrap = "break-word", t() } } function o(e) { if (e && e.nodeName && "TEXTAREA" === e.nodeName) { var t = d("autosize:destroy"); e.dispatchEvent(t) } } function r(e) { if (e && e.nodeName && "TEXTAREA" === e.nodeName) { var t = d("autosize:update"); e.dispatchEvent(t) } } var i = "function" == typeof Set ? new Set : function() { var e = []; return { has: function(t) { return Boolean(e.indexOf(t) > -1) }, add: function(t) { e.push(t) }, "delete": function(t) { e.splice(e.indexOf(t), 1) } } }(), d = function(e) { return new Event(e) }; try { new Event("test") } catch (a) { d = function(e) { var t = document.createEvent("Event"); return t.initEvent(e, !0, !1), t } } var l = null; "undefined" == typeof window || "function" != typeof window.getComputedStyle ? (l = function(e) { return e }, l.destroy = function(e) { return e }, l.update = function(e) { return e }) : (l = function(e, t) { return e && Array.prototype.forEach.call(e.length ? e : [e], function(e) { return n(e, t) }), e }, l.destroy = function(e) { return e && Array.prototype.forEach.call(e.length ? e : [e], o), e }, l.update = function(e) { return e && Array.prototype.forEach.call(e.length ? e : [e], r), e }), t.exports = l });

/*!
    autoNumeric v2.0.9
    https://github.com/BobKnothe/autoNumeric/
**/
!function(e, t) { "object" == typeof exports && "object" == typeof module ? module.exports = t(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? exports.autonumeric = t(require("jquery")) : e.autonumeric = t(e.jQuery) }(this, function(e) { return function(e) { function t(i) { if (a[i]) return a[i].exports; var r = a[i] = { exports: {}, id: i, loaded: !1 }; return e[i].call(r.exports, r, r.exports, t), r.loaded = !0, r.exports } var a = {}; return t.m = e, t.c = a, t.p = "", t(0) }([function(e, t, a) { var i, r, n; a(1), a(1), function() { "use strict"; Object.defineProperty(t, "__esModule", { value: !0 }); var o = function() { function e(e, t) { for (var a = 0; a < t.length; a++) { var i = t[a]; i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i) } } return function(t, a, i) { return a && e(t.prototype, a), i && e(t, i), t } }(), l = function(e, t) { if (Array.isArray(e)) return e; if (Symbol.iterator in Object(e)) return function(e, t) { var a = [], i = !0, r = !1, n = void 0; try { for (var o, l = e[Symbol.iterator](); !(i = (o = l.next()).done) && (a.push(o.value), !t || a.length !== t); i = !0); } catch (e) { r = !0, n = e } finally { try { !i && l.return && l.return() } finally { if (r) throw n } } return a }(e, t); throw new TypeError("Invalid attempt to destructure non-iterable instance") }, s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) { return typeof e } : function(e) { return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e }, c = void 0, u = void 0, d = void 0, m = void 0, h = void 0, g = void 0, v = ["b", "caption", "cite", "code", "const", "dd", "del", "div", "dfn", "dt", "em", "h1", "h2", "h3", "h4", "h5", "h6", "ins", "kdb", "label", "li", "option", "output", "p", "q", "s", "sample", "span", "strong", "td", "th", "u"], p = { digitGroupSeparator: ",", noSeparatorOnFocus: !1, digitalGroupSpacing: "3", decimalCharacter: ".", decimalCharacterAlternative: null, currencySymbol: "", currencySymbolPlacement: "p", negativePositiveSignPlacement: null, showPositiveSign: !1, suffixText: "", overrideMinMaxLimits: null, maximumValue: "9999999999999.99", minimumValue: "-9999999999999.99", decimalPlacesOverride: null, decimalPlacesShownOnFocus: null, scaleDivisor: null, scaleDecimalPlaces: null, scaleSymbol: null, saveValueToSessionStorage: !1, onInvalidPaste: "error", roundingMethod: "S", allowDecimalPadding: !0, negativeBracketsTypeOnBlur: null, emptyInputBehavior: "focus", leadingZero: "deny", formatOnPageLoad: !0, selectNumberOnly: !1, defaultValueOverride: null, unformatOnSubmit: !1, outputFormat: null, showWarnings: !0, failOnUnknownOption: !1 }, f = 8, y = 9, S = 13, b = 32, P = 34, C = 37, w = 39, O = 40, x = 45, N = 46, k = 48, V = 65, A = 67, T = 86, F = 88, D = 91, M = 93, B = 96, L = 110, E = 112, I = 123, _ = 144, K = 145, G = 189, U = 224, j = { Unidentified: "Unidentified", Alt: "Alt", AltGr: "AltGraph", CapsLock: "CapsLock", Ctrl: "Control", Fn: "Fn", FnLock: "FnLock", Hyper: "Hyper", Meta: "Meta", Windows: "Meta", Command: "Meta", NumLock: "NumLock", ScrollLock: "ScrollLock", Shift: "Shift", Super: "Super", Symbol: "Symbol", SymbolLock: "SymbolLock", Enter: "Enter", Tab: "Tab", Space: " ", DownArrow: "ArrowDown", LeftArrow: "ArrowLeft", RightArrow: "ArrowRight", UpArrow: "ArrowUp", End: "End", Home: "Home", PageDown: "PageDown", PageUp: "PageUp", Backspace: "Backspace", Clear: "Clear", Copy: "Copy", CrSel: "CrSel", Cut: "Cut", Delete: "Delete", EraseEof: "EraseEof", ExSel: "ExSel", Insert: "Insert", Paste: "Paste", Redo: "Redo", Undo: "Undo", Accept: "Accept", Again: "Again", Attn: "Attn", Cancel: "Cancel", ContextMenu: "ContextMenu", Esc: "Escape", Execute: "Execute", Find: "Find", Finish: "Finish", Help: "Help", Pause: "Pause", Play: "Play", Props: "Props", Select: "Select", ZoomIn: "ZoomIn", ZoomOut: "ZoomOut", BrightnessDown: "BrightnessDown", BrightnessUp: "BrightnessUp", Eject: "Eject", LogOff: "LogOff", Power: "Power", PowerOff: "PowerOff", PrintScreen: "PrintScreen", Hibernate: "Hibernate", Standby: "Standby", WakeUp: "WakeUp", Compose: "Compose", Dead: "Dead", F1: "F1", F2: "F2", F3: "F3", F4: "F4", F5: "F5", F6: "F6", F7: "F7", F8: "F8", F9: "F9", F10: "F10", F11: "F11", F12: "F12", Print: "Print", num0: "0", num1: "1", num2: "2", num3: "3", num4: "4", num5: "5", num6: "6", num7: "7", num8: "8", num9: "9", numpad0: "0", numpad1: "1", numpad2: "2", numpad3: "3", numpad4: "4", numpad5: "5", numpad6: "6", numpad7: "7", numpad8: "8", numpad9: "9", a: "a", b: "b", c: "c", d: "d", e: "e", f: "f", g: "g", h: "h", i: "i", j: "j", k: "k", l: "l", m: "m", n: "n", o: "o", p: "p", q: "q", r: "r", s: "s", t: "t", u: "u", v: "v", w: "w", x: "x", y: "y", z: "z", MultiplyNumpad: "*", PlusNumpad: "+", MinusNumpad: "-", DotNumpad: ".", SlashNumpad: "/", Semicolon: ";", Equal: "=", Comma: ",", Hyphen: "-", Minus: "-", Plus: "+", Dot: ".", Slash: "/", Backquote: "`", LeftBracket: "[", RightBracket: "]", Backslash: "\\", Quote: "'", NumpadDot: ".", NumpadDotAlt: ",", NumpadMultiply: "*", NumpadPlus: "+", NumpadMinus: "-", NumpadSlash: "/", NumpadDotObsoleteBrowsers: "Decimal", NumpadMultiplyObsoleteBrowsers: "Multiply", NumpadPlusObsoleteBrowsers: "Add", NumpadMinusObsoleteBrowsers: "Subtract", NumpadSlashObsoleteBrowsers: "Divide" }, R = "-999999999999.99", $ = "999999999999.99", Z = "U", z = "deny", q = { French: { digitGroupSeparator: ".", decimalCharacter: ",", decimalCharacterAlternative: ".", currencySymbol: " €", currencySymbolPlacement: "s", selectNumberOnly: !0, roundingMethod: Z, leadingZero: z, minimumValue: R, maximumValue: $ }, NorthAmerican: { digitGroupSeparator: ",", decimalCharacter: ".", currencySymbol: "$", currencySymbolPlacement: "p", selectNumberOnly: !0, roundingMethod: Z, leadingZero: z, minimumValue: R, maximumValue: $ }, British: { digitGroupSeparator: ",", decimalCharacter: ".", currencySymbol: "£", currencySymbolPlacement: "p", selectNumberOnly: !0, roundingMethod: Z, leadingZero: z, minimumValue: R, maximumValue: $ }, Swiss: { digitGroupSeparator: "'", decimalCharacter: ".", currencySymbol: " CHF", currencySymbolPlacement: "s", selectNumberOnly: !0, roundingMethod: Z, leadingZero: z, minimumValue: R, maximumValue: $ }, Japanese: { digitGroupSeparator: ",", decimalCharacter: ".", currencySymbol: "¥", currencySymbolPlacement: "p", selectNumberOnly: !0, roundingMethod: Z, leadingZero: z, minimumValue: R, maximumValue: $ } }; q.Spanish = q.French, q.Chinese = q.Japanese, r = [a(1)], void 0 === (n = "function" == typeof (i = function(e) { function t(e) { return null === e } function a(e) { return void 0 === e } function i(e) { return null == e || "" === e } function r(e) { return "string" == typeof e || e instanceof String } function n(e) { return "boolean" == typeof e } function R(e) { var t = String(e).toLowerCase(); return "true" === t || "false" === t } function $(e) { return "object" === (void 0 === e ? "undefined" : s(e)) && null !== e && !Array.isArray(e) } function Z(e) { return !J(e) && !isNaN(parseFloat(e)) && isFinite(e) } function z(e, t) { return de(e, t.settingsClone, !0).replace(t.settingsClone.decimalCharacter, ".") } function H(e, t) { return !(!r(e) || !r(t) || "" === e || "" === t) && -1 !== e.indexOf(t) } function W(e, t) { return !(!J(t) || t === [] || a(e)) && -1 !== t.indexOf(e) } function J(e) { if ("[object Array]" === Object.prototype.toString.call([])) return Array.isArray(e) || "object" === (void 0 === e ? "undefined" : s(e)) && "[object Array]" === Object.prototype.toString.call(e); throw new Error("toString message changed for Object Array") } function Q(e) { var t = e.split("."), i = l(t, 2)[1]; return a(i) ? 0 : i.length } function Y(e) { if (void 0 === e.key || "Unidentified" === e.key) return String.fromCharCode(this.keyCodeNumber(e)); var t = void 0; switch (e.key) { case "Decimal": t = j.NumpadDot; break; case "Multiply": t = j.NumpadMultiply; break; case "Add": t = j.NumpadPlus; break; case "Subtract": t = j.NumpadMinus; break; case "Divide": t = j.NumpadSlash; break; case "Del": t = j.Dot; break; default: t = e.key }return t } function X(e, t, a) { var i = Pe(e); return Ce(t, i) > -1 && Ce(a, i) < 1 } function ee(e) { return !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1] ? H(e, "-") : te(e) } function te(e) { return "-" === e.charAt(0) } function ae(e) { return !/[1-9]/g.test(e) } function ie(e) { return te(e) ? e : "-" + e } function re(e, t, a) { for (var i = new RegExp("[0-9" + a + "-]"), r = 0, n = 0; n < t; n++)i.test(e[n]) && r++; return r } function ne(e) { return Math.max(e, e - 1) } function oe(e) { var t = {}; if (a(e.selectionStart)) { e.focus(); var i = document.selection.createRange(); t.length = i.text.length, i.moveStart("character", -e.value.length), t.end = i.text.length, t.start = t.end - t.length } else t.start = e.selectionStart, t.end = e.selectionEnd, t.length = t.end - t.start; return t } function le(e, t) { var r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null; if (i(r) && (r = t), a(e.selectionStart)) { e.focus(); var n = e.createTextRange(); n.collapse(!0), n.moveEnd("character", r), n.moveStart("character", t), n.select() } else e.selectionStart = t, e.selectionEnd = r } function se(e) { throw new Error(e) } function ce(e) { (!(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1]) && console.warn("Warning: " + e) } function ue(e, t) { return Math.max(Q(e), Q(t)) } function de(e, t, i) { if ("" !== t.currencySymbol && (e = e.replace(t.currencySymbol, "")), t.suffixText) for (; H(e, t.suffixText);)e = e.replace(t.suffixText, ""); e = e.replace(t.skipFirstAutoStrip, "$1$2"), ("s" === t.negativePositiveSignPlacement || "s" === t.currencySymbolPlacement && "p" !== t.negativePositiveSignPlacement) && ee(e) && "" !== e && (t.trailingNegative = !0), e = (e = e.replace(t.skipLastAutoStrip, "$1")).replace(t.allowedAutoStrip, ""), t.decimalCharacterAlternative && (e = e.replace(t.decimalCharacterAlternative, t.decimalCharacter)); var r = e.match(t.numRegAutoStrip); if (e = r ? [r[1], r[2], r[3]].join("") : "", "allow" === t.leadingZero || "keep" === t.leadingZero) { var n = "", o = e.split(t.decimalCharacter), s = l(o, 2), c = s[0], u = s[1], d = c; H(d, t.negativeSignCharacter) && (n = t.negativeSignCharacter, d = d.replace(t.negativeSignCharacter, "")), "" === n && d.length > t.mIntPos && "0" === d.charAt(0) && (d = d.slice(1)), "" !== n && d.length > t.mIntNeg && "0" === d.charAt(0) && (d = d.slice(1)), e = "" + n + d + (a(u) ? "" : t.decimalCharacter + u) } return (i && "deny" === t.leadingZero || !t.hasFocus && "allow" === t.leadingZero) && (e = e.replace(t.stripReg, "$1$2")), e } function me(e, t) { if ("p" === t.currencySymbolPlacement && "l" === t.negativePositiveSignPlacement || "s" === t.currencySymbolPlacement && "p" === t.negativePositiveSignPlacement) { var a = t.negativeBracketsTypeOnBlur.split(","), i = l(a, 2), r = i[0], n = i[1]; t.hasFocus ? t.hasFocus && e.charAt(0) === r && (e = (e = e.replace(r, t.negativeSignCharacter)).replace(n, "")) : e = r + (e = e.replace(t.negativeSignCharacter, "")) + n } return e } function he(e, t) { e = (e = e.replace(t.currencySymbol, "")).replace(t.digitGroupSeparator, ""), "." !== t.decimalCharacter && (e = e.replace(t.decimalCharacter, ".")), ee(e) && e.lastIndexOf("-") === e.length - 1 && (e = "-" + (e = e.replace("-", ""))); var a = Be(e, !0, !1, !1); return isNaN(a) || (e = a.toString()), e } function ge(e, a) { if (t(a) || "string" === a) return e; var i = void 0; switch (a) { case "number": i = Number(e); break; case ".-": i = ee(e) ? e.replace("-", "") + "-" : e; break; case ",": case "-,": i = e.replace(".", ","); break; case ",-": i = ee(i = e.replace(".", ",")) ? i.replace("-", "") + "-" : i; break; case ".": case "-.": i = e; break; default: se("The given outputFormat [" + a + "] option is not recognized.") }return i } function ve(e, t) { return "." !== t.decimalCharacter && (e = e.replace(t.decimalCharacter, ".")), "-" !== t.negativeSignCharacter && "" !== t.negativeSignCharacter && (e = e.replace(t.negativeSignCharacter, "-")), e.match(/\d/) || (e += "0"), e } function pe(e, t) { return "-" !== t.negativeSignCharacter && "" !== t.negativeSignCharacter && (e = e.replace("-", t.negativeSignCharacter)), "." !== t.decimalCharacter && (e = e.replace(".", t.decimalCharacter)), e } function fe(e, t, a) { return "" === e || e === t.negativeSignCharacter ? "always" === t.emptyInputBehavior || a ? "l" === t.negativePositiveSignPlacement ? e + t.currencySymbol + t.suffixText : t.currencySymbol + e + t.suffixText : e : null } function ye(e, i) { i.strip && (e = de(e, i, !1)), i.trailingNegative && !ee(e) && (e = "-" + e); var r = fe(e, i, !0), n = ee(e), o = ae(e); if (n && (e = e.replace("-", "")), !t(r)) return r; i.digitalGroupSpacing = i.digitalGroupSpacing.toString(); var s = void 0; switch (i.digitalGroupSpacing) { case "2": s = /(\d)((\d)(\d{2}?)+)$/; break; case "2s": s = /(\d)((?:\d{2}){0,2}\d{3}(?:(?:\d{2}){2}\d{3})*?)$/; break; case "4": s = /(\d)((\d{4}?)+)$/; break; default: s = /(\d)((\d{3}?)+)$/ }var c = e.split(i.decimalCharacter), u = l(c, 2), d = u[0], m = u[1]; if (i.decimalCharacterAlternative && a(m)) { var h = e.split(i.decimalCharacterAlternative), g = l(h, 2); d = g[0], m = g[1] } if ("" !== i.digitGroupSeparator) for (; s.test(d);)d = d.replace(s, "$1" + i.digitGroupSeparator + "$2"); if (0 === i.decimalPlacesOverride || a(m) ? e = d : (m.length > i.decimalPlacesOverride && (m = m.substring(0, i.decimalPlacesOverride)), e = d + i.decimalCharacter + m), i.trailingNegative = !1, "p" === i.currencySymbolPlacement) if (n) switch (i.negativePositiveSignPlacement) { case "l": e = "" + i.negativeSignCharacter + i.currencySymbol + e; break; case "r": e = "" + i.currencySymbol + i.negativeSignCharacter + e; break; case "s": e = "" + i.currencySymbol + e + i.negativeSignCharacter, i.trailingNegative = !0 } else if (i.showPositiveSign && !o) switch (i.negativePositiveSignPlacement) { case "l": e = "" + i.positiveSignCharacter + i.currencySymbol + e; break; case "r": e = "" + i.currencySymbol + i.positiveSignCharacter + e; break; case "s": e = "" + i.currencySymbol + e + i.positiveSignCharacter } else e = i.currencySymbol + e; if ("s" === i.currencySymbolPlacement) if (n) switch (i.negativePositiveSignPlacement) { case "r": e = "" + e + i.currencySymbol + i.negativeSignCharacter, i.trailingNegative = !0; break; case "l": e = "" + e + i.negativeSignCharacter + i.currencySymbol, i.trailingNegative = !0; break; case "p": e = "" + i.negativeSignCharacter + e + i.currencySymbol } else if (i.showPositiveSign && !o) switch (i.negativePositiveSignPlacement) { case "r": e = "" + e + i.currencySymbol + i.positiveSignCharacter; break; case "l": e = "" + e + i.positiveSignCharacter + i.currencySymbol; break; case "p": e = "" + i.positiveSignCharacter + e + i.currencySymbol } else e += i.currencySymbol; return null !== i.negativeBracketsTypeOnBlur && (i.rawValue < 0 || te(e)) && (e = me(e, i)), e + i.suffixText } function Se(e, t) { var a = void 0; switch (t) { case 0: a = /(\.(?:\d*[1-9])?)0*$/; break; case 1: a = /(\.\d(?:\d*[1-9])?)0*$/; break; default: a = new RegExp("(\\.\\d{" + t + "}(?:\\d*[1-9])?)0*") }return e = e.replace(a, "$1"), 0 === t && (e = e.replace(/\.$/, "")), e } function be(e, t) { if (e = "" === e ? "0" : e.toString(), "N05" === t.roundingMethod || "CHF" === t.roundingMethod || "U05" === t.roundingMethod || "D05" === t.roundingMethod) { switch (t.roundingMethod) { case "N05": e = (Math.round(20 * e) / 20).toString(); break; case "U05": e = (Math.ceil(20 * e) / 20).toString(); break; default: e = (Math.floor(20 * e) / 20).toString() }return H(e, ".") ? e.length - e.indexOf(".") < 3 ? e + "0" : e : e + ".00" } var a, i = "", r = 0, n = ""; a = t.allowDecimalPadding ? t.decimalPlacesOverride : 0, te(e) && (n = "-", e = e.replace("-", "")), e.match(/^\d/) || (e = "0" + e), 0 === Number(e) && (n = ""), (Number(e) > 0 && "keep" !== t.leadingZero || e.length > 0 && "allow" === t.leadingZero) && (e = e.replace(/^0*(\d)/, "$1")); var o = e.lastIndexOf("."), l = -1 === o, s = l ? e.length - 1 : o, c = e.length - 1 - s; if (c <= t.decimalPlacesOverride) { if (i = e, c < a) { l && (i += t.decimalCharacter); for (var u = "000000"; c < a;)i += u = u.substring(0, a - c), c += u.length } else c > a ? i = Se(i, a) : 0 === c && 0 === a && (i = i.replace(/\.$/, "")); return 0 === Number(i) ? i : n + i } var d; d = l ? t.decimalPlacesOverride - 1 : t.decimalPlacesOverride + o; var m = Number(e.charAt(d + 1)), h = "." === e.charAt(d) ? e.charAt(d - 1) % 2 : e.charAt(d) % 2, g = e.substring(0, d + 1).split(""); if (m > 4 && "S" === t.roundingMethod || m > 4 && "A" === t.roundingMethod && "" === n || m > 5 && "A" === t.roundingMethod && "-" === n || m > 5 && "s" === t.roundingMethod || m > 5 && "a" === t.roundingMethod && "" === n || m > 4 && "a" === t.roundingMethod && "-" === n || m > 5 && "B" === t.roundingMethod || 5 === m && "B" === t.roundingMethod && 1 === h || m > 0 && "C" === t.roundingMethod && "" === n || m > 0 && "F" === t.roundingMethod && "-" === n || m > 0 && "U" === t.roundingMethod) for (r = g.length - 1; r >= 0; r -= 1)if ("." !== g[r]) { if (g[r] = +g[r] + 1, g[r] < 10) break; r > 0 && (g[r] = "0") } return i = Se((g = g.slice(0, d + 1)).join(""), a), 0 === Number(i) ? i : n + i } function Pe(e) { var t = {}, a = void 0, i = void 0, r = void 0, n = void 0; if (0 === e && 1 / e < 0 && (e = "-0"), te(e = e.toString()) ? (e = e.slice(1), t.s = -1) : t.s = 1, (a = e.indexOf(".")) > -1 && (e = e.replace(".", "")), a < 0 && (a = e.length), (i = -1 === e.search(/[1-9]/i) ? e.length : e.search(/[1-9]/i)) === (r = e.length)) t.e = 0, t.c = [0]; else { for (n = r - 1; "0" === e.charAt(n); n -= 1)r -= 1; for (r -= 1, t.e = a - i - 1, t.c = [], a = 0; i <= r; i += 1)t.c[a] = +e.charAt(i), a += 1 } return t } function Ce(e, t) { var a = t.c, i = e.c, r = t.s, n = e.s, o = t.e, l = e.e; if (!a[0] || !i[0]) return a[0] ? r : i[0] ? -n : 0; if (r !== n) return r; var s = r < 0; if (o !== l) return o > l ^ s ? 1 : -1; for (r = -1, n = (o = a.length) < (l = i.length) ? o : l, r += 1; r < n; r += 1)if (a[r] !== i[r]) return a[r] > i[r] ^ s ? 1 : -1; return o === l ? 0 : o > l ^ s ? 1 : -1 } function we(e, t) { e = (e = e.toString()).replace(",", "."); var a = Pe(t.minimumValue), i = Pe(t.maximumValue), r = Pe(e), n = void 0; switch (t.overrideMinMaxLimits) { case "floor": n = [Ce(a, r) > -1, !0]; break; case "ceiling": n = [!0, Ce(i, r) < 1]; break; case "ignore": n = [!0, !0]; break; default: n = [Ce(a, r) > -1, Ce(i, r) < 1] }return n } function Oe(t) { return r(t) && (t = "#" + t.replace(/(:|\.|\[|]|,|=)/g, "\\$1")), e(t) } function xe(e, t) { var i = arguments.length > 2 && void 0 !== arguments[2] && arguments[2], r = e.data("autoNumeric"); r || (r = {}, e.data("autoNumeric", r)); var n = r.holder; return (i || a(n) && t) && (n = new Ee(e.get(0), t), r.holder = n), n } function Ne(e, t) { return "" === e ? "" : 0 === Number(e) && "keep" !== t.leadingZero ? "0" : ("keep" !== t.leadingZero && H(e = e.replace(/^(-)?0+(?=\d)/g, "$1"), ".") && (e = e.replace(/(\.[0-9]*?)0+$/, "$1")), e = e.replace(/\.$/, "")) } function ke(e, t, i) { if (t.saveValueToSessionStorage) { var r = "" === e.name || a(e.name) ? "AUTO_" + e.id : "AUTO_" + decodeURIComponent(e.name), n = void 0, o = void 0; if (!1 === function() { var e = "modernizr"; try { return sessionStorage.setItem(e, e), sessionStorage.removeItem(e), !0 } catch (e) { return !1 } }()) switch (i) { case "set": document.cookie = r + "=" + t.rawValue + "; expires= ; path=/"; break; case "wipe": (n = new Date).setTime(n.getTime() + -864e5), o = "; expires=" + n.toUTCString(), document.cookie = r + "='' ;" + o + "; path=/"; break; case "get": return function(e) { for (var t = e + "=", a = document.cookie.split(";"), i = "", r = 0; r < a.length; r += 1) { for (i = a[r]; " " === i.charAt(0);)i = i.substring(1, i.length); if (0 === i.indexOf(t)) return i.substring(t.length, i.length) } return null }(r) } else switch (i) { case "set": sessionStorage.setItem(r, t.rawValue); break; case "wipe": sessionStorage.removeItem(r); break; case "get": return sessionStorage.getItem(r) } } } function Ve() { var t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0], a = Oe(arguments[1]), i = e("form").index(a), r = e("form:eq(" + i + ")")[0], n = [], o = [], c = /^(?:submit|button|image|reset|file)$/i, u = /^(?:input|select|textarea|keygen)/i, d = /^(?:checkbox|radio)$/i, m = /^(?:button|checkbox|color|date|datetime|datetime-local|email|file|image|month|number|password|radio|range|reset|search|submit|time|url|week)/i, h = 0; if (e.each(r, function(e, t) { "" === t.name || !u.test(t.localName) || c.test(t.type) || t.disabled || !t.checked && d.test(t.type) ? o.push(-1) : (o.push(h), h++) }), h = 0, e.each(r, function(e, t) { "input" !== t.localName || "" !== t.type && "text" !== t.type && "hidden" !== t.type && "tel" !== t.type ? (n.push(-1), "input" === t.localName && m.test(t.type) && h++) : (n.push(h), h++) }), t) { var g = a.serializeArray(); return e.each(g, function(t, a) { var r = o.indexOf(t); if (r > -1 && n[r] > -1) { var l = e("form:eq(" + i + ") input:eq(" + n[r] + ")"), c = l.data("autoNumeric"); "object" === (void 0 === c ? "undefined" : s(c)) && (a.value = l.autoNumeric("getLocalized").toString()) } }), g } var v, p, f = (v = a.serialize(), p = v.split("&"), e.each(p, function(t) { var a = p[t].split("="), r = l(a, 2), c = r[0], u = r[1], d = o.indexOf(t); if (d > -1 && n[d] > -1) { var m = e("form:eq(" + i + ") input:eq(" + n[d] + ")"), h = m.data("autoNumeric"); if ("object" === (void 0 === h ? "undefined" : s(h)) && null !== u) { var g = m.autoNumeric("getLocalized").toString(); p[t] = c + "=" + g } } }), { v: p.join("&") }); if ("object" === (void 0 === f ? "undefined" : s(f))) return f.v } function Ae(e, t, a) { var i = t.settingsClone; if ("focusin" === a.type || "mouseenter" === a.type && !e.is(":focus") && "focus" === i.emptyInputBehavior) { i.hasFocus = !0, null !== i.negativeBracketsTypeOnBlur && "" !== i.negativeSignCharacter && e.val(me(a.target.value, i)); var r = de(a.target.value, i, !0); r = Ne(r = he(r, i), i), i.trailingNegative && (r = "-" + r); var n = void 0; i.decimalPlacesShownOnFocus ? (i.decimalPlacesOverride = i.decimalPlacesShownOnFocus, n = be(i.rawValue, i), e.val(ye(n, i))) : i.scaleDivisor ? (i.decimalPlacesOverride = Number(i.oDec), n = be(i.rawValue, i), e.val(ye(n, i))) : i.noSeparatorOnFocus ? (i.digitGroupSeparator = "", i.currencySymbol = "", i.suffixText = "", n = be(i.rawValue, i), e.val(ye(n, i))) : r !== i.rawValue && e.autoNumeric("set", r), t.valueOnFocus = a.target.value, t.lastVal = t.valueOnFocus; var o = fe(t.valueOnFocus, i, !0); null !== o && "" !== o && "focus" === i.emptyInputBehavior && (e.val(o), o === i.currencySymbol && "s" === i.currencySymbolPlacement && le(a.target, 0, 0)) } } function Te(e, t, a) { if (!e.is(":focus")) { var i = a.target.value, r = i, n = t.settingsClone; if (n.hasFocus = !1, n.saveValueToSessionStorage && ke(a.target, n, "set"), !0 === n.noSeparatorOnFocus && (n.digitGroupSeparator = n.oSep, n.currencySymbol = n.oSign, n.suffixText = n.oSuffix), null !== n.decimalPlacesShownOnFocus && (n.decimalPlacesOverride = n.oDec, n.allowDecimalPadding = n.oPad, n.negativeBracketsTypeOnBlur = n.oBracket), "" !== (i = de(i, n, !0))) { n.trailingNegative && !ee(i) && (i = "-" + i, n.trailingNegative = !1); var o = we(i, n), s = l(o, 2), c = s[0], u = s[1]; null === fe(i, n, !1) && c && u ? (i = ve(i, n), n.rawValue = Ne(i, n), n.scaleDivisor && (i = (i /= n.scaleDivisor).toString()), n.decimalPlacesOverride = n.scaleDivisor && n.scaleDecimalPlaces ? Number(n.scaleDecimalPlaces) : n.decimalPlacesOverride, i = pe(i = be(i, n), n)) : (c || e.trigger("autoNumeric:minExceeded"), u || e.trigger("autoNumeric:maxExceeded"), i = n.rawValue) } else "zero" === n.emptyInputBehavior ? (n.rawValue = "0", i = be("0", n)) : n.rawValue = ""; var d = fe(i, n, !1); null === d && (d = ye(i, n)), d !== r && (d = n.scaleSymbol ? d + n.scaleSymbol : d, e.val(d)), d !== t.valueOnFocus && (e.change(), delete t.valueOnFocus) } } function Fe(e) { var t = { aSep: "digitGroupSeparator", nSep: "noSeparatorOnFocus", dGroup: "digitalGroupSpacing", aDec: "decimalCharacter", altDec: "decimalCharacterAlternative", aSign: "currencySymbol", pSign: "currencySymbolPlacement", pNeg: "negativePositiveSignPlacement", aSuffix: "suffixText", oLimits: "overrideMinMaxLimits", vMax: "maximumValue", vMin: "minimumValue", mDec: "decimalPlacesOverride", eDec: "decimalPlacesShownOnFocus", scaleDecimal: "scaleDecimalPlaces", aStor: "saveValueToSessionStorage", mRound: "roundingMethod", aPad: "allowDecimalPadding", nBracket: "negativeBracketsTypeOnBlur", wEmpty: "emptyInputBehavior", lZero: "leadingZero", aForm: "formatOnPageLoad", sNumber: "selectNumberOnly", anDefault: "defaultValueOverride", unSetOnSubmit: "unformatOnSubmit", outputType: "outputFormat", debug: "showWarnings", digitGroupSeparator: !0, noSeparatorOnFocus: !0, digitalGroupSpacing: !0, decimalCharacter: !0, decimalCharacterAlternative: !0, currencySymbol: !0, currencySymbolPlacement: !0, negativePositiveSignPlacement: !0, showPositiveSign: !0, suffixText: !0, overrideMinMaxLimits: !0, maximumValue: !0, minimumValue: !0, decimalPlacesOverride: !0, decimalPlacesShownOnFocus: !0, scaleDivisor: !0, scaleDecimalPlaces: !0, scaleSymbol: !0, saveValueToSessionStorage: !0, onInvalidPaste: !0, roundingMethod: !0, allowDecimalPadding: !0, negativeBracketsTypeOnBlur: !0, emptyInputBehavior: !0, leadingZero: !0, formatOnPageLoad: !0, selectNumberOnly: !0, defaultValueOverride: !0, unformatOnSubmit: !0, outputFormat: !0, showWarnings: !0, failOnUnknownOption: !0, hasFocus: !0, runOnce: !0, rawValue: !0, trailingNegative: !0, caretFix: !0, throwInput: !0, strip: !0, tagList: !0, negativeSignCharacter: !0, positiveSignCharacter: !0, mIntPos: !0, mIntNeg: !0, oDec: !0, oPad: !0, oBracket: !0, oSep: !0, oSign: !0, oSuffix: !0, aNegRegAutoStrip: !0, skipFirstAutoStrip: !0, skipLastAutoStrip: !0, allowedAutoStrip: !0, numRegAutoStrip: !0, stripReg: !0, holder: !0 }; for (var a in e) if (e.hasOwnProperty(a)) { if (!0 === t[a]) continue; t.hasOwnProperty(a) ? (ce("You are using the deprecated option name '" + a + "'. Please use '" + t[a] + "' instead from now on. The old option name will be dropped soon.", !0), e[t[a]] = e[a], delete e[a]) : e.failOnUnknownOption && se("Option name '" + a + "' is unknown. Please fix the options passed to autoNumeric") } } function De(r, n) { var o, s, c, u, d, m, g, f, y, S, b, P = arguments.length > 2 && void 0 !== arguments[2] && arguments[2], C = n.data("autoNumeric"); if (!P && t(r) || Fe(r), P || a(C)) { if (P) C = e.extend(C, r); else { var w = n.data(); C = e.extend({}, p, w, r, { hasFocus: !1, runOnce: !1, rawValue: "", trailingNegative: !1, caretFix: !1, throwInput: !0, strip: !0, tagList: v }) } return b = C, e.each(b, function(e, t) { "true" !== t && "false" !== t || (b[e] = "true" === t), "number" == typeof t && "aScale" !== e && (b[e] = t.toString()) }), function(e) { if (t(e.negativePositiveSignPlacement)) if (a(e) || !i(e.negativePositiveSignPlacement) || i(e.currencySymbol)) e.negativePositiveSignPlacement = "l"; else switch (e.currencySymbolPlacement) { case "s": e.negativePositiveSignPlacement = "p"; break; case "p": e.negativePositiveSignPlacement = "l" } }(C), C.negativeSignCharacter = C.minimumValue < 0 ? "-" : "", C.positiveSignCharacter = C.maximumValue >= 0 ? "+" : "", y = n, S = C, e.each(S, function(e, t) { "function" == typeof t ? S[e] = t(y, S, e) : "function" == typeof y.autoNumeric[t] && (S[e] = y.autoNumeric[t](y, S, e)) }), d = (u = C).maximumValue.toString().split("."), m = l(d, 1)[0], g = u.minimumValue || 0 === u.minimumValue ? u.minimumValue.toString().split(".") : [], f = l(g, 1)[0], m = m.replace("-", ""), f = f.replace("-", ""), u.mIntPos = Math.max(m.length, 1), u.mIntNeg = Math.max(f.length, 1), t((c = C).decimalPlacesOverride) && (c.decimalPlacesOverride = ue(c.minimumValue, c.maximumValue)), c.oDec = String(c.decimalPlacesOverride), c.decimalPlacesOverride = Number(c.decimalPlacesOverride), t((s = C).decimalCharacterAlternative) && Number(s.decimalPlacesOverride) > 0 && ("." === s.decimalCharacter && "," !== s.digitGroupSeparator ? s.decimalCharacterAlternative = "," : "," === s.decimalCharacter && "." !== s.digitGroupSeparator && (s.decimalCharacterAlternative = ".")), function(e) { var t = "[0-9]", a = e.negativeSignCharacter ? "([-\\" + e.negativeSignCharacter + "]?)" : "(-?)"; e.aNegRegAutoStrip = a; var i; i = e.negativeSignCharacter ? "\\" + e.negativeSignCharacter : "", e.skipFirstAutoStrip = new RegExp(a + "[^-" + i + "\\" + e.decimalCharacter + t + "].*?(" + t + "|\\" + e.decimalCharacter + t + ")"), e.skipLastAutoStrip = new RegExp("([0-9]\\" + e.decimalCharacter + "?)[^\\" + e.decimalCharacter + t + "][^0-9]*$"); var r = "-0123456789\\" + e.decimalCharacter; e.allowedAutoStrip = new RegExp("[^" + r + "]", "g"), e.numRegAutoStrip = new RegExp(a + "(?:\\" + e.decimalCharacter + "?(" + t + "+\\" + e.decimalCharacter + t + "+)|(" + t + "*(?:\\" + e.decimalCharacter + t + "*)?))"), e.stripReg = new RegExp("^" + e.aNegRegAutoStrip + "0*(" + t + ")") }(C), h(C, !1), (o = C).oDec = o.decimalPlacesOverride, o.oPad = o.allowDecimalPadding, o.oBracket = o.negativeBracketsTypeOnBlur, o.oSep = o.digitGroupSeparator, o.oSign = o.currencySymbol, o.oSuffix = o.suffixText, n.data("autoNumeric", C), C } return null } function Me(e, t) { var a = void 0; return e = parseNumber(e), Z(Number(e)) ? a = e : (a = he(e.toString(), t), Z(Number(a)) || (ce('The value "' + e + '" being "set" is not numeric and therefore cannot be used appropriately.', t.showWarnings), a = NaN)), a } function Be(e) { var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], a = arguments.length > 2 && void 0 !== arguments[2] && arguments[2], i = arguments.length > 3 && void 0 !== arguments[3] && arguments[3], r = e.toString(); if ("" === r) return e; a && (r = r.replace(/٫/, ".")), i && (r = r.replace(/٬/g, "")), r = r.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(e) { return e.charCodeAt(0) - 1632 }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(e) { return e.charCodeAt(0) - 1776 }); var n = Number(r); return isNaN(n) ? n : (t && (r = n), r) } function Le(e) { var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : document, a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null, i = void 0; window.CustomEvent ? i = new CustomEvent(e, { detail: a, bubbles: !1, cancelable: !1 }) : (i = document.createEvent("CustomEvent")).initCustomEvent(e, !0, !0, { detail: a }), t.dispatchEvent(i) } var Ee = function() { function t(a, i) { (function(e, t) { if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function") })(this, t), this.settings = i, this.that = a, this.$that = e(a), this.formatted = !1, this.settingsClone = i, this.value = a.value } return o(t, [{ key: "_updateAutoNumericHolderProperties", value: function() { this.value = this.that.value, this.selection = oe(this.that), this.processed = !1, this.formatted = !1 } }, { key: "_updateAutoNumericHolderEventKeycode", value: function(e) { var t; this.eventKeyCode = void 0 === (t = e).which ? t.keyCode : t.which } }, { key: "_setSelection", value: function(e, t, i) { e = Math.max(e, 0), t = Math.min(t, this.that.value.length), this.selection = { start: e, end: t, length: t - e }, (a(i) || i) && le(this.that, e, t) } }, { key: "_setCaretPosition", value: function(e, t) { this._setSelection(e, e, t) } }, { key: "_getLeftAndRightPartAroundTheSelection", value: function() { var e = this.value; return [e.substring(0, this.selection.start), e.substring(this.selection.end, e.length)] } }, { key: "_getUnformattedLeftAndRightPartAroundTheSelection", value: function() { var e = this.settingsClone, t = this._getLeftAndRightPartAroundTheSelection(), a = l(t, 2), i = a[0], r = a[1]; "" === i && "" === r && (e.trailingNegative = !1); var n = !0; return this.eventKeyCode === G && 0 === Number(i) && (n = !1), i = de(i, this.settingsClone, n), r = de(r, this.settingsClone, !1), e.trailingNegative && !ee(i) && (i = "-" + i, r = "-" === r ? "" : r, e.trailingNegative = !1), [i, r] } }, { key: "_normalizeParts", value: function(e, t) { var a = this.settingsClone, i = !0; if (this.eventKeyCode === G && 0 === Number(e) && (i = !1), e = de(e, a, i), t = de(t, a, !1), "deny" !== a.leadingZero || this.eventKeyCode !== k && this.eventKeyCode !== B || 0 !== Number(e) || H(e, a.decimalCharacter) || "" === t || (e = e.substring(0, e.length - 1)), a.trailingNegative && !ee(e) && (e = "-" + e, a.trailingNegative = !1), this.newValue = e + t, a.decimalCharacter) { var r = this.newValue.match(new RegExp("^" + a.aNegRegAutoStrip + "\\" + a.decimalCharacter)); r && (e = e.replace(r[1], r[1] + "0"), this.newValue = e + t) } return [e, t] } }, { key: "_setValueParts", value: function(e, t) { var a = arguments.length > 2 && void 0 !== arguments[2] && arguments[2], i = this.settingsClone, r = this._normalizeParts(e, t), n = we(this.newValue, i), o = l(n, 2), s = o[0], c = o[1], u = r[0].length; if (this.newValue = r.join(""), s && c) { this.newValue = function(e, t, a) { if (e = a ? be(e, t) : e, t.decimalCharacter && t.decimalPlacesOverride) { var i = e.split(t.decimalCharacter), r = l(i, 2), n = r[0], o = r[1]; if (o && o.length > t.decimalPlacesOverride) if (t.decimalPlacesOverride > 0) { var s = o.substring(0, t.decimalPlacesOverride); e = "" + n + t.decimalCharacter + s } else e = n } return e }(this.newValue, i, a); var d = H(this.newValue, ",") ? this.newValue.replace(",", ".") : this.newValue; return "" === d || d === i.negativeSignCharacter ? i.rawValue = "zero" === i.emptyInputBehavior ? "0" : "" : i.rawValue = Ne(d, i), u > this.newValue.length && (u = this.newValue.length), 1 === u && "0" === r[0] && "deny" === i.leadingZero && (u = "" === r[1] || "0" === r[0] && "" !== r[1] ? 1 : 0), this.value = this.newValue, this._setCaretPosition(u, !1), !0 } return s ? c || this.$that.trigger("autoNumeric:maxExceeded") : this.$that.trigger("autoNumeric:minExceeded"), !1 } }, { key: "_getSignPosition", value: function() { var e = this.settingsClone, t = e.currencySymbol, a = this.that; if (t) { var i = t.length; if ("p" === e.currencySymbolPlacement) return e.negativeSignCharacter && a.value && a.value.charAt(0) === e.negativeSignCharacter ? [1, i + 1] : [0, i]; var r = a.value.length; return [r - i, r] } return [1e3, -1] } }, { key: "_expandSelectionOnSign", value: function(e) { var t = this._getSignPosition(), a = this.selection; a.start < t[1] && a.end > t[0] && ((a.start < t[0] || a.end > t[1]) && this.value.substring(Math.max(a.start, t[0]), Math.min(a.end, t[1])).match(/^\s*$/) ? a.start < t[0] ? this._setSelection(a.start, t[0], e) : this._setSelection(t[1], a.end, e) : this._setSelection(Math.min(a.start, t[0]), Math.max(a.end, t[1]), e)) } }, { key: "_checkPaste", value: function() { if (!a(this.valuePartsBeforePaste)) { var e = this.valuePartsBeforePaste, t = this._getLeftAndRightPartAroundTheSelection(), i = l(t, 2), r = i[0], n = i[1]; delete this.valuePartsBeforePaste; var o = r.substr(0, e[0].length) + de(r.substr(e[0].length), this.settingsClone, !0); this._setValueParts(o, n, !0) || (this.value = e.join(""), this._setCaretPosition(e[0].length, !1)) } } }, { key: "_skipAlways", value: function(e) { if ((e.ctrlKey || e.metaKey) && "keyup" === e.type && !a(this.valuePartsBeforePaste) || e.shiftKey && this.eventKeyCode === x) return this._checkPaste(), !1; if (this.eventKeyCode >= E && this.eventKeyCode <= I || this.eventKeyCode >= D && this.eventKeyCode <= M || this.eventKeyCode >= y && this.eventKeyCode < b || this.eventKeyCode < f && (0 === e.which || e.which === this.eventKeyCode) || this.eventKeyCode === _ || this.eventKeyCode === K || this.eventKeyCode === x || this.eventKeyCode === U) return !0; if ((e.ctrlKey || e.metaKey) && this.eventKeyCode === V) { if (this.settings.selectNumberOnly) { e.preventDefault(); var t, i = this.that.value.length, r = this.settings.currencySymbol.length, n = ee(this.that.value) ? 1 : 0, o = this.settings.suffixText.length, l = this.settings.currencySymbolPlacement, s = this.settings.negativePositiveSignPlacement; t = "s" === l ? 0 : "l" === s && 1 === n && r > 0 ? r + 1 : r; var c = void 0; if ("p" === l) c = i - o; else switch (s) { case "l": c = i - (o + r); break; case "r": c = r > 0 ? i - (r + n + o) : i - (r + o); break; default: c = i - (r + o) }le(this.that, t, c) } return !0 } return !e.ctrlKey && !e.metaKey || this.eventKeyCode !== A && this.eventKeyCode !== T && this.eventKeyCode !== F ? !(!e.ctrlKey && !e.metaKey) || (this.eventKeyCode === C || this.eventKeyCode === w ? ("keydown" !== e.type || e.shiftKey || (this.eventKeyCode !== C || this.that.value.charAt(this.selection.start - 2) !== this.settingsClone.digitGroupSeparator && this.that.value.charAt(this.selection.start - 2) !== this.settingsClone.decimalCharacter ? this.eventKeyCode !== w || this.that.value.charAt(this.selection.start + 1) !== this.settingsClone.digitGroupSeparator && this.that.value.charAt(this.selection.start + 1) !== this.settingsClone.decimalCharacter || this._setCaretPosition(this.selection.start + 1) : this._setCaretPosition(this.selection.start - 1)), !0) : this.eventKeyCode >= P && this.eventKeyCode <= O) : ("keydown" === e.type && this._expandSelectionOnSign(), this.eventKeyCode !== T && this.eventKeyCode !== x || ("keydown" === e.type || "keypress" === e.type ? a(this.valuePartsBeforePaste) && (this.valuePartsBeforePaste = this._getLeftAndRightPartAroundTheSelection()) : this._checkPaste()), "keydown" === e.type || "keypress" === e.type || this.eventKeyCode === A) } }, { key: "_processCharacterDeletionIfTrailingNegativeSign", value: function(e) { var t = l(e, 2), a = t[0], i = t[1], r = this.settingsClone; return "p" === r.currencySymbolPlacement && "s" === r.negativePositiveSignPlacement && (this.eventKeyCode === f ? (r.caretFix = this.selection.start >= this.value.indexOf(r.suffixText) && "" !== r.suffixText, "-" === this.value.charAt(this.selection.start - 1) ? a = a.substring(1) : this.selection.start <= this.value.length - r.suffixText.length && (a = a.substring(0, a.length - 1))) : (r.caretFix = this.selection.start >= this.value.indexOf(r.suffixText) && "" !== r.suffixText, this.selection.start >= this.value.indexOf(r.currencySymbol) + r.currencySymbol.length && (i = i.substring(1, i.length)), ee(a) && "-" === this.value.charAt(this.selection.start) && (a = a.substring(1)))), "s" === r.currencySymbolPlacement && "l" === r.negativePositiveSignPlacement && (r.caretFix = this.selection.start >= this.value.indexOf(r.negativeSignCharacter) + r.negativeSignCharacter.length, this.eventKeyCode === f ? this.selection.start === this.value.indexOf(r.negativeSignCharacter) + r.negativeSignCharacter.length && H(this.value, r.negativeSignCharacter) ? a = a.substring(1) : "-" !== a && (this.selection.start <= this.value.indexOf(r.negativeSignCharacter) || !H(this.value, r.negativeSignCharacter)) && (a = a.substring(0, a.length - 1)) : ("-" === a[0] && (i = i.substring(1)), this.selection.start === this.value.indexOf(r.negativeSignCharacter) && H(this.value, r.negativeSignCharacter) && (a = a.substring(1)))), "s" === r.currencySymbolPlacement && "r" === r.negativePositiveSignPlacement && (r.caretFix = this.selection.start >= this.value.indexOf(r.negativeSignCharacter) + r.negativeSignCharacter.length, this.eventKeyCode === f ? this.selection.start === this.value.indexOf(r.negativeSignCharacter) + r.negativeSignCharacter.length ? a = a.substring(1) : "-" !== a && this.selection.start <= this.value.indexOf(r.negativeSignCharacter) - r.currencySymbol.length ? a = a.substring(0, a.length - 1) : "" === a || H(this.value, r.negativeSignCharacter) || (a = a.substring(0, a.length - 1)) : (r.caretFix = this.selection.start >= this.value.indexOf(r.currencySymbol) && "" !== r.currencySymbol, this.selection.start === this.value.indexOf(r.negativeSignCharacter) && (a = a.substring(1)), i = i.substring(1))), [a, i] } }, { key: "_processCharacterDeletion", value: function() { var e = this.settingsClone, t = void 0, a = void 0; if (this.selection.length) { this._expandSelectionOnSign(!1); var i = this._getUnformattedLeftAndRightPartAroundTheSelection(), r = l(i, 2); t = r[0], a = r[1] } else { var n = this._getUnformattedLeftAndRightPartAroundTheSelection(), o = l(n, 2); if (t = o[0], a = o[1], "" === t && "" === a && (e.throwInput = !1), ("p" === e.currencySymbolPlacement && "s" === e.negativePositiveSignPlacement || "s" === e.currencySymbolPlacement && ("l" === e.negativePositiveSignPlacement || "r" === e.negativePositiveSignPlacement)) && ee(this.value)) { var s = this._processCharacterDeletionIfTrailingNegativeSign([t, a]), c = l(s, 2); t = c[0], a = c[1] } else this.eventKeyCode === f ? t = t.substring(0, t.length - 1) : a = a.substring(1, a.length) } this._setValueParts(t, a) } }, { key: "_processCharacterInsertion", value: function(e) { var t = this.settingsClone, a = this._getUnformattedLeftAndRightPartAroundTheSelection(), i = l(a, 2), r = i[0], n = i[1]; t.throwInput = !0; var o = Y(e); if (o === t.decimalCharacter || t.decimalCharacterAlternative && o === t.decimalCharacterAlternative || ("." === o || "," === o) && this.eventKeyCode === L) return !t.decimalPlacesOverride || !t.decimalCharacter || !(!t.negativeSignCharacter || !H(n, t.negativeSignCharacter)) || !!H(r, t.decimalCharacter) || n.indexOf(t.decimalCharacter) > 0 || (0 === n.indexOf(t.decimalCharacter) && (n = n.substr(1)), this._setValueParts(r + t.decimalCharacter, n), !0); if (("-" === o || "+" === o) && "-" === t.negativeSignCharacter) return !t || ("p" === t.currencySymbolPlacement && "s" === t.negativePositiveSignPlacement || "s" === t.currencySymbolPlacement && "p" !== t.negativePositiveSignPlacement ? ("" === r && H(n, t.negativeSignCharacter) && (r = t.negativeSignCharacter, n = n.substring(1, n.length)), r = te(r) || H(r, t.negativeSignCharacter) ? r.substring(1, r.length) : "-" === o ? t.negativeSignCharacter + r : r) : ("" === r && H(n, t.negativeSignCharacter) && (r = t.negativeSignCharacter, n = n.substring(1, n.length)), r = r.charAt(0) === t.negativeSignCharacter ? r.substring(1, r.length) : "-" === o ? t.negativeSignCharacter + r : r), this._setValueParts(r, n), !0); var s = Number(o); return s >= 0 && s <= 9 ? (t.negativeSignCharacter && "" === r && H(n, t.negativeSignCharacter) && (r = t.negativeSignCharacter, n = n.substring(1, n.length)), t.maximumValue <= 0 && t.minimumValue < t.maximumValue && !H(this.value, t.negativeSignCharacter) && "0" !== o && (r = t.negativeSignCharacter + r), this._setValueParts(r + o, n), !0) : (t.throwInput = !1, !1) } }, { key: "_formatValue", value: function(t) { var a = this.settingsClone, i = this.value, r = this._getUnformattedLeftAndRightPartAroundTheSelection(), n = l(r, 1)[0]; if (("" === a.digitGroupSeparator || "" !== a.digitGroupSeparator && !H(i, a.digitGroupSeparator)) && ("" === a.currencySymbol || "" !== a.currencySymbol && !H(i, a.currencySymbol))) { var o = i.split(a.decimalCharacter), s = l(o, 1)[0], c = ""; ee(s) && (c = "-", s = s.replace("-", ""), n = n.replace("-", "")), "" === c && s.length > a.mIntPos && "0" === n.charAt(0) && (n = n.slice(1)), "-" === c && s.length > a.mIntNeg && "0" === n.charAt(0) && (n = n.slice(1)), n = c + n } var u, d, m, h = ye(this.value, this.settingsClone), g = h.length; if (h) { var v = n.split(""); ("s" === a.negativePositiveSignPlacement || "s" === a.currencySymbolPlacement && "p" !== a.negativePositiveSignPlacement) && "-" === v[0] && "" !== a.negativeSignCharacter && (v.shift(), this.eventKeyCode !== f && this.eventKeyCode !== N || !a.caretFix || ("s" === a.currencySymbolPlacement && "l" === a.negativePositiveSignPlacement && (v.push("-"), a.caretFix = "keydown" === t.type), "p" === a.currencySymbolPlacement && "s" === a.negativePositiveSignPlacement && (v.push("-"), a.caretFix = "keydown" === t.type), "s" === a.currencySymbolPlacement && "r" === a.negativePositiveSignPlacement && (u = a.currencySymbol.split(""), d = ["\\", "^", "$", ".", "|", "?", "*", "+", "(", ")", "["], m = [], e.each(u, function(e, t) { W(t = u[e], d) ? m.push("\\" + t) : m.push(t) }), this.eventKeyCode === f && m.push("-"), v.push(m.join("")), a.caretFix = "keydown" === t.type))); for (var p = 0; p < v.length; p++)v[p].match("\\d") || (v[p] = "\\" + v[p]); var y = new RegExp("^.*?" + v.join(".*?")), S = h.match(y); S ? (g = S[0].length, a.showPositiveSign && (0 === g && S.input.charAt(0) === a.positiveSignCharacter && (g = 1 === S.input.indexOf(a.currencySymbol) ? a.currencySymbol.length + 1 : 1), 0 === g && S.input.charAt(a.currencySymbol.length) === a.positiveSignCharacter && (g = a.currencySymbol.length + 1)), (0 === g && h.charAt(0) !== a.negativeSignCharacter || 1 === g && h.charAt(0) === a.negativeSignCharacter) && a.currencySymbol && "p" === a.currencySymbolPlacement && (g = this.settingsClone.currencySymbol.length + (te(h) ? 1 : 0))) : (a.currencySymbol && "s" === a.currencySymbolPlacement && (g -= a.currencySymbol.length), a.suffixText && (g -= a.suffixText.length)) } h === this.that.value && (h !== this.that.value || this.eventKeyCode !== k && this.eventKeyCode !== B) || (this.that.value = h, this._setCaretPosition(g)), this.formatted = !0 } }]), t }(), Ie = { init: function(a) { return this.each(function() { var r = e(this), n = function(e) { var t = e.is("input[type=text], input[type=hidden], input[type=tel], input:not([type])"); t || "input" !== e.prop("tagName").toLowerCase() || se('The input type "' + e.prop("type") + '" is not supported by autoNumeric'); var a = e.prop("tagName").toLowerCase(); return "input" === a || W(a, v) || se("The <" + a + "> tag is not supported by autoNumeric"), t }(r), o = De(a, r, !1); if (t(o)) return this; var l, s, c = xe(r, o, !1); !o.runOnce && o.formatOnPageLoad && function(e, t, a) { var r = !0; if (t) { var n = a.val(), o = Me(n, e); if (e.formatOnPageLoad && "" !== n && i(a.attr("value"))) isNaN(o) || 1 / 0 === o ? se("The value [" + n + "] used in the input is not a valid value autoNumeric can work with.") : (a.autoNumeric("set", o), r = !1); else if (null !== e.defaultValueOverride && e.defaultValueOverride.toString() !== n || null === e.defaultValueOverride && "" !== n && n !== a.attr("value") || "" !== n && "hidden" === a.attr("type") && !Z(o)) { if ((null !== e.decimalPlacesShownOnFocus && e.saveValueToSessionStorage || e.scaleDivisor && e.saveValueToSessionStorage) && (e.rawValue = ke(a[0], e, "get")), !e.saveValueToSessionStorage) { var l = void 0; null !== e.negativeBracketsTypeOnBlur && "" !== e.negativeSignCharacter ? (e.hasFocus = !0, l = me(n, e)) : l = n, ("s" === e.negativePositiveSignPlacement || "p" !== e.negativePositiveSignPlacement && "s" === e.currencySymbolPlacement) && "" !== e.negativeSignCharacter && ee(n) ? e.rawValue = e.negativeSignCharacter + de(l, e, !0) : e.rawValue = de(l, e, !0) } r = !1 } if ("" === n) switch (e.emptyInputBehavior) { case "focus": r = !1; break; case "always": a.val(e.currencySymbol), r = !1; break; case "zero": a.autoNumeric("set", "0"), r = !1 } else r && n === a.attr("value") && a.autoNumeric("set", n) } W(a.prop("tagName").toLowerCase(), e.tagList) && "" !== a.text() && (null !== e.defaultValueOverride ? e.defaultValueOverride === a.text() && a.autoNumeric("set", a.text()) : a.autoNumeric("set", a.text())) }(o, n, r), o.runOnce = !0, n && (this.addEventListener("focusin", function(e) { Ae(r, c, e) }, !1), this.addEventListener("mouseenter", function(e) { Ae(r, c, e) }, !1), this.addEventListener("blur", function(e) { Te(r, c, e) }, !1), this.addEventListener("mouseleave", function(e) { Te(r, c, e) }, !1), this.addEventListener("keydown", function(e) { var t, a; a = e, (t = c)._updateAutoNumericHolderEventKeycode(a), t.initialValueOnKeydown = a.target.value, t.that.readOnly ? t.processed = !0 : (t.eventKeyCode === S && t.valueOnFocus !== a.target.value && (Le("change", a.target), t.valueOnFocus = a.target.value), t._updateAutoNumericHolderProperties(a), t._skipAlways(a) ? t.processed = !0 : t.eventKeyCode === f || t.eventKeyCode === N ? (t._processCharacterDeletion(), t.processed = !0, t._formatValue(a), a.target.value !== t.lastVal && t.settingsClone.throwInput && (Le("input", a.target), a.preventDefault()), t.lastVal = a.target.value, t.settingsClone.throwInput = !0) : t.formatted = !1) }, !1), this.addEventListener("keypress", function(e) { !function(e, t) { var a = Y(t); if (a !== j.Insert) { var i = e.processed; if (e._updateAutoNumericHolderProperties(t), !e._skipAlways(t)) { if (i) return void t.preventDefault(); if (e._processCharacterInsertion(t)) { if (e._formatValue(t), t.target.value !== e.lastVal && e.settingsClone.throwInput) Le("input", t.target), t.preventDefault(); else { if ((a === e.settings.decimalCharacter || a === e.settings.decimalCharacterAlternative) && oe(t.target).start === oe(t.target).end && oe(t.target).start === t.target.value.indexOf(e.settings.decimalCharacter)) { var r = oe(t.target).start + 1; le(t.target, r, r) } t.preventDefault() } return e.lastVal = t.target.value, void (e.settingsClone.throwInput = !0) } t.preventDefault(), e.formatted = !1 } } }(c, e) }, !1), this.addEventListener("keyup", function(e) { !function(e, t, a) { e._updateAutoNumericHolderProperties(a); var i = e._skipAlways(a); delete e.valuePartsBeforePaste, i || "" === a.target.value || (a.target.value === e.settingsClone.currencySymbol ? "s" === e.settingsClone.currencySymbolPlacement ? le(a.target, 0, 0) : le(a.target, e.settingsClone.currencySymbol.length, e.settingsClone.currencySymbol.length) : e.eventKeyCode === y && le(a.target, 0, a.target.value.length), (a.target.value === e.settingsClone.suffixText || "" === e.settingsClone.rawValue && "" !== e.settingsClone.currencySymbol && "" !== e.settingsClone.suffixText) && le(a.target, 0, 0), null !== e.settingsClone.decimalPlacesShownOnFocus && e.settingsClone.saveValueToSessionStorage && ke(a.target, t, "set"), e.formatted || e._formatValue(a), a.target.value !== e.initialValueOnKeydown && Le("autoNumeric:formatted", a.target)) }(c, o, e) }, !1), this.addEventListener("blur", function(e) { var t, a; t = c, (a = e).target.value !== t.valueOnFocus && Le("change", a.target) }, !1), this.addEventListener("paste", function(e) { !function(e, t, a) { a.preventDefault(); var i = a.clipboardData.getData("text/plain"), r = a.target.value, n = a.target.selectionStart || 0, o = a.target.selectionEnd || 0, l = o - n, s = !1; l === r.length && (s = !0); var c = te(i); c && (i = i.slice(1, i.length)); var u, d = z(i, t); if ("." === (u = "." === d ? "." : Be(d, !1, !1, !1)) || Z(u) && "" !== u) { var m = void 0, h = void 0, g = te(h = "" === a.target.value ? "" : e.autoNumeric("get")), v = void 0, p = void 0; c && !g ? (h = "-" + h, g = !0, v = !0) : v = !1; var f, y, S, b = !1; switch (t.settings.onInvalidPaste) { case "truncate": case "replace": var P = r.slice(0, n), C = r.slice(o, r.length); p = z(n !== o ? P + C : r, t), g && (p = ie(p)), m = ne(re(r, n, t.settings.decimalCharacter)), v && m++; var w = p.slice(0, m), O = p.slice(m, p.length); "." === u && (H(w, ".") && (b = !0, w = w.replace(".", "")), O = O.replace(".", "")); for (var x = Pe(t.settings.minimumValue), N = Pe(t.settings.maximumValue), k = p, V = 0, A = w; V < u.length && X(p = (A += u[V]) + O, x, N);)k = p, V++; if (m += V, "truncate" === t.settings.onInvalidPaste) { p = k, b && m--; break } for (var T = m, F = k.length; V < u.length && T < F;)if ("." !== k[T]) { if (f = k, y = T, S = u[V], !X(p = "" + f.substr(0, y) + S + f.substr(y + S.length), x, N)) break; k = p, V++, T++ } else T++; m = T, b && m--, p = k; break; case "error": case "ignore": case "clamp": default: var D = r.slice(0, n), M = r.slice(o, r.length); if (p = z(n !== o ? D + M : r, t), g && (p = ie(p)), m = ne(re(r, n, t.settings.decimalCharacter)), v && m++, w = p.slice(0, m), O = p.slice(m, p.length), "." === u && (H(w, ".") && (b = !0, w = w.replace(".", "")), O = O.replace(".", "")), p = "" + w + u + O, n === o) m = ne(re(r, n, t.settings.decimalCharacter)) + u.length; else if (s) m = p.length; else if ("" === O) m = ne(re(r, n, t.settings.decimalCharacter)) + u.length; else { var B = ne(re(r, o, t.settings.decimalCharacter)), L = a.target.value.slice(n, o); m = B - l + function(e, t) { for (var a = 0, i = 0; i < t.length; i++)t[i] === e && a++; return a }(t.settings.digitGroupSeparator, L) + u.length } s || (v && m++, b && m--) }if (Z(p) && "" !== p) { var E, I, _ = !1, K = !1; try { e.autoNumeric("set", p), _ = !0 } catch (a) { var G = void 0; switch (t.settings.onInvalidPaste) { case "clamp": E = p, I = t.settings, G = Math.max(I.minimumValue, Math.min(I.maximumValue, E)); try { e.autoNumeric("set", G) } catch (e) { se("Fatal error: Unable to set the clamped value '" + G + "'.") } K = !0, _ = !0, p = G; break; case "error": case "truncate": case "replace": se("The pasted value '" + i + "' results in a value '" + p + "' that is outside of the minimum [" + t.settings.minimumValue + "] and maximum [" + t.settings.maximumValue + "] value range."); case "ignore": default: return } } var U = void 0; if (_) switch (t.settings.onInvalidPaste) { case "clamp": if (K) { "s" === t.settings.currencySymbolPlacement ? le(a.target, a.target.value.length - t.settings.currencySymbol.length) : le(a.target, a.target.value.length); break } case "error": case "ignore": case "truncate": case "replace": default: U = function(e, t, a, i) { var r = a.length, n = e.length, o = void 0, l = 0; for (o = 0; o < r && l < n && l < t; o++)(e[l] === a[o] || "." === e[l] && a[o] === i) && l++; return o }(p, m, a.target.value, t.settings.decimalCharacter), le(a.target, U) }_ && r !== a.target.value && Le("input", a.target) } else "error" === t.settings.onInvalidPaste && se("The pasted value '" + i + "' would result into an invalid content '" + p + "'.") } else "error" === t.settings.onInvalidPaste && se("The pasted value '" + i + "' is not a valid paste content.") }(r, c, e) }, !1), s = c, (l = r).closest("form").on("submit.autoNumeric", function() { if (s) { var e = s.settingsClone; e.unformatOnSubmit && l.val(e.rawValue) } })) }) }, destroy: function() { return e(this).each(function() { var e = Oe(this), t = e.data("autoNumeric"); "object" === (void 0 === t ? "undefined" : s(t)) && (e.val(""), ke(e[0], t, "wipe"), e.removeData("autoNumeric"), e.off(".autoNumeric")) }) }, wipe: function() { return e(this).each(function() { var e = Oe(this), t = e.data("autoNumeric"); "object" === (void 0 === t ? "undefined" : s(t)) && (e.val(""), t.rawValue = "", ke(e[0], t, "wipe")) }) }, update: function(t) { return e(this).each(function() { var e = Oe(this), a = e.autoNumeric("get"); if (xe(e, De(t, e, !0), !0), "" !== e.val() || "" !== e.text()) return e.autoNumeric("set", a) }) }, set: function(t) { return e(this).each(function() { if (null !== t && !a(t)) { var e = Oe(this), i = e.data("autoNumeric"); "object" !== (void 0 === i ? "undefined" : s(i)) && se('Initializing autoNumeric is required prior to calling the "set" method.'), i.trailingNegative = !1; var r = e.is("input[type=text], input[type=hidden], input[type=tel], input:not([type])"), n = Me(t, i); if (n && (n = parseFloat(n)), isNaN(n)) return e.val(""); if ("" === n) return e.val(""); var o = we(n, i), c = l(o, 2), u = c[0], d = c[1]; if (ae(n) && (n = "0"), !u || !d) { i.rawValue = "", ke(e[0], i, "wipe"); var m = n; return n = "", u || e.trigger("autoNumeric:minExceeded"), d || e.trigger("autoNumeric:maxExceeded"), se("The value [" + m + "] being set falls outside of the minimumValue [" + i.minimumValue + "] and maximumValue [" + i.maximumValue + "] range set for this element"), e.val("") } if (r || W(e.prop("tagName").toLowerCase(), i.tagList)) { var h = !1, g = void 0; i.decimalPlacesShownOnFocus && (g = i.decimalPlacesOverride, i.decimalPlacesOverride = Number(i.decimalPlacesShownOnFocus), n = be(n, i), h = !0, i.decimalPlacesOverride = g), i.scaleDivisor && !i.onOff && (n = be(n, i), i.rawValue = Ne(n.replace(i.decimalCharacter, "."), i), n = Me(n, i), n = (n /= i.scaleDivisor).toString(), i.scaleDecimalPlaces && (g = i.decimalPlacesOverride, i.decimalPlacesOverride = Number(i.scaleDecimalPlaces), n = be(n, i), h = !0)), h || (n = be(n, i)), i.scaleDivisor || (i.rawValue = Ne(n.replace(i.decimalCharacter, "."), i)), n = ye(n = pe(n, i), i), i.scaleDivisor && i.scaleDecimalPlaces && !i.onOff && (i.decimalPlacesOverride = g) } return i.saveValueToSessionStorage && (i.decimalPlacesShownOnFocus || i.scaleDivisor) && ke(e[0], i, "set"), !i.hasFocus && i.scaleSymbol && (n += i.scaleSymbol), r ? e.val(n) : !!W(e.prop("tagName").toLowerCase(), i.tagList) && e.text(n) } }) }, unSet: function() { return e(this).each(function() { var e = Oe(this), t = e.data("autoNumeric"); "object" === (void 0 === t ? "undefined" : s(t)) && (t.hasFocus = !0, e.val(e.autoNumeric("getLocalized"))) }) }, reSet: function() { return e(this).each(function() { var e = Oe(this), t = e.data("autoNumeric"); "object" === (void 0 === t ? "undefined" : s(t)) && e.autoNumeric("set", e.val()) }) }, get: function() { var e = Oe(this), t = e.is("input[type=text], input[type=hidden], input[type=tel], input:not([type])"), a = e.data("autoNumeric"); "object" !== (void 0 === a ? "undefined" : s(a)) && se('Initializing autoNumeric is required prior to calling the "get" method.'); var r = ""; if (t ? r = e.eq(0).val() : W(e.prop("tagName").toLowerCase(), a.tagList) ? r = e.eq(0).text() : se('The "<' + e.prop("tagName").toLowerCase() + '>" tag is not supported by autoNumeric'), a.decimalPlacesShownOnFocus || a.scaleDivisor) r = a.rawValue; else { var n = ee(r); if (!/\d/.test(r) && "focus" === a.emptyInputBehavior) return ""; "" !== r && null !== a.negativeBracketsTypeOnBlur && (a.hasFocus = !0, r = me(r, a)), (a.runOnce || !1 === a.formatOnPageLoad) && (r = Ne((r = de(r, a, !0)).replace(a.decimalCharacter, "."), a), a.trailingNegative && n && !ee(r) && 0 !== Number(r) && (r = "-" + r)), ("" !== r || "" === r && "zero" === a.emptyInputBehavior) && (r = ve(r, a)) } return function(e) { var t = e.split("."), a = l(t, 2), r = a[0], n = a[1]; if (i(n)) return r; var o = n.replace(/0+$/g, ""); return "" === o ? r : r + "." + o }(r) }, getLocalized: function() { var e = Oe(this), t = e.autoNumeric("get"), a = e.data("autoNumeric"); return 0 === Number(t) && "keep" !== a.leadingZero && (t = "0"), ge(t, a.outputFormat) }, getNumber: function() { return ge(Oe(this).autoNumeric("get"), "number") }, getFormatted: function() { return this.hasOwnProperty("0") && "value" in this[0] || se("Unable to get the formatted string from the element."), this[0].value }, getString: function() { return Ve(!1, this) }, getArray: function() { return Ve(!0, this) }, getSettings: function() { return this.data("autoNumeric") } }; e.fn.autoNumeric = function(e) { if (Ie[e]) { for (var t = arguments.length, a = Array(t > 1 ? t - 1 : 0), i = 1; i < t; i++)a[i - 1] = arguments[i]; return Ie[e].apply(this, a) } return "object" !== (void 0 === e ? "undefined" : s(e)) && e ? void se('Method "' + e + '" is not supported by autoNumeric') : Ie.init.apply(this, [e]) }, d = function() { return p }, e.fn.autoNumeric.defaults = p, m = function() { return q }, e.fn.autoNumeric.lang = q, c = function(i) { var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null; if (a(i) || null === i) return null; r(i) || Z(i) || se('The value "' + i + '" being "set" is not numeric and therefore cannot be used appropriately.'); var o = e.extend({}, p, { strip: !1 }, n); i < 0 && (o.negativeSignCharacter = "-"), t(o.decimalPlacesOverride) && (o.decimalPlacesOverride = ue(o.minimumValue, o.maximumValue)); var s = Me(i, o); isNaN(s) && se("The value [" + s + "] that you are trying to format is not a recognized number."); var c = we(s, o), u = l(c, 2), d = u[0], m = u[1]; return d && m || (Le("autoFormat.autoNumeric", document, "Range test failed"), se("The value [" + s + "] being set falls outside of the minimumValue [" + o.minimumValue + "] and maximumValue [" + o.maximumValue + "] range set for this element")), ye(s = pe(s = be(s, o), o), o) }, e.fn.autoFormat = c, u = function(t, i) { if (a(t) || null === t) return null; if (Z(t)) return Number(t); (J(t) || $(t)) && se("A number or a string representing a number is needed to be able to unformat it, [" + t + "] given."); var r = e.extend({}, p, { strip: !1 }, i), n = "-0123456789\\" + r.decimalCharacter, o = new RegExp("[^" + n + "]", "gi"); return ee(t = t.toString()) ? r.negativeSignCharacter = "-" : r.negativeBracketsTypeOnBlur && r.negativeBracketsTypeOnBlur.split(",")[0] === t.charAt(0) && (r.negativeSignCharacter = "-", r.hasFocus = !0, t = me(t, r)), ge(t = (t = t.replace(o, "")).replace(r.decimalCharacter, "."), r.outputFormat) }, e.fn.autoUnformat = u, h = function(a) { var o = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1]; (i(a) || !$(a) || function(e) { for (var t in e) if (e.hasOwnProperty(t)) return !1; return !0 }(a)) && se("The userOptions are invalid ; it should be a valid object, [" + a + "] given."), t(a) || Fe(a); var l; R((l = o ? e.extend({}, p, a) : a).showWarnings) || n(l.showWarnings) || se("The debug option 'showWarnings' is invalid ; it should be either 'false' or 'true', [" + l.showWarnings + "] given."); var s, c = /^[0-9]+$/, u = /^-?[0-9]+(\.?[0-9]+)?$/; W(l.digitGroupSeparator, [",", ".", " ", " ", " ", " ", "", "'", "٬", "˙"]) || se("The thousand separator character option 'digitGroupSeparator' is invalid ; it should be ',', '.', '٬', '˙', \"'\", ' ', ' ', ' ', ' ' or empty (''), [" + l.digitGroupSeparator + "] given."), R(l.noSeparatorOnFocus) || n(l.noSeparatorOnFocus) || se("The 'noSeparatorOnFocus' option is invalid ; it should be either 'false' or 'true', [" + l.noSeparatorOnFocus + "] given."), c.test(l.digitalGroupSpacing) || se("The digital grouping for thousand separator option 'digitalGroupSpacing' is invalid ; it should be a positive integer, [" + l.digitalGroupSpacing + "] given."), W(l.decimalCharacter, [",", ".", "·", "٫", "⎖"]) || se("The decimal separator character option 'decimalCharacter' is invalid ; it should be '.', ',', '·', '⎖' or '٫', [" + l.decimalCharacter + "] given."), l.decimalCharacter === l.digitGroupSeparator && se("autoNumeric will not function properly when the decimal character 'decimalCharacter' [" + l.decimalCharacter + "] and the thousand separator 'digitGroupSeparator' [" + l.digitGroupSeparator + "] are the same character."), t(l.decimalCharacterAlternative) || r(l.decimalCharacterAlternative) || se("The alternate decimal separator character option 'decimalCharacterAlternative' is invalid ; it should be a string, [" + l.decimalCharacterAlternative + "] given."), "" === l.currencySymbol || r(l.currencySymbol) || se("The currency symbol option 'currencySymbol' is invalid ; it should be a string, [" + l.currencySymbol + "] given."), W(l.currencySymbolPlacement, ["p", "s"]) || se("The placement of the currency sign option 'currencySymbolPlacement' is invalid ; it should either be 'p' (prefix) or 's' (suffix), [" + l.currencySymbolPlacement + "] given."), W(l.negativePositiveSignPlacement, ["p", "s", "l", "r", null]) || se("The placement of the negative sign option 'negativePositiveSignPlacement' is invalid ; it should either be 'p' (prefix), 's' (suffix), 'l' (left), 'r' (right) or 'null', [" + l.negativePositiveSignPlacement + "] given."), R(l.showPositiveSign) || n(l.showPositiveSign) || se("The show positive sign option 'showPositiveSign' is invalid ; it should be either 'false' or 'true', [" + l.showPositiveSign + "] given."), (!r(l.suffixText) || "" !== l.suffixText && (ee(l.suffixText) || /[0-9]+/.test(l.suffixText))) && se("The additional suffix option 'suffixText' is invalid ; it should not contains the negative sign '-' nor any numerical characters, [" + l.suffixText + "] given."), t(l.overrideMinMaxLimits) || W(l.overrideMinMaxLimits, ["ceiling", "floor", "ignore"]) || se("The override min & max limits option 'overrideMinMaxLimits' is invalid ; it should either be 'ceiling', 'floor' or 'ignore', [" + l.overrideMinMaxLimits + "] given."), r(l.maximumValue) && u.test(l.maximumValue) || se("The maximum possible value option 'maximumValue' is invalid ; it should be a string that represents a positive or negative number, [" + l.maximumValue + "] given."), r(l.minimumValue) && u.test(l.minimumValue) || se("The minimum possible value option 'minimumValue' is invalid ; it should be a string that represents a positive or negative number, [" + l.minimumValue + "] given."), parseFloat(l.minimumValue) > parseFloat(l.maximumValue) && se("The minimum possible value option is greater than the maximum possible value option ; 'minimumValue' [" + l.minimumValue + "] should be smaller than 'maximumValue' [" + l.maximumValue + "]."), t(l.decimalPlacesOverride) || "number" == typeof (s = l.decimalPlacesOverride) && parseFloat(s) === parseInt(s, 10) && !isNaN(s) && l.decimalPlacesOverride >= 0 || r(l.decimalPlacesOverride) && c.test(l.decimalPlacesOverride) || se("The maximum number of decimal places option 'decimalPlacesOverride' is invalid ; it should be a positive integer, [" + l.decimalPlacesOverride + "] given."); var d = ue(l.minimumValue, l.maximumValue); t(l.decimalPlacesOverride) || d === Number(l.decimalPlacesOverride) || ce("Setting 'decimalPlacesOverride' to [" + l.decimalPlacesOverride + "] will override the decimals declared in 'minimumValue' [" + l.minimumValue + "] and 'maximumValue' [" + l.maximumValue + "].", l.showWarnings), l.allowDecimalPadding || t(l.decimalPlacesOverride) || ce("Setting 'allowDecimalPadding' to [false] will override the current 'decimalPlacesOverride' setting [" + l.decimalPlacesOverride + "].", l.showWarnings), t(l.decimalPlacesShownOnFocus) || r(l.decimalPlacesShownOnFocus) && c.test(l.decimalPlacesShownOnFocus) || se("The number of expanded decimal places option 'decimalPlacesShownOnFocus' is invalid ; it should be a positive integer, [" + l.decimalPlacesShownOnFocus + "] given."), !t(l.decimalPlacesShownOnFocus) && !t(l.decimalPlacesOverride) && Number(l.decimalPlacesOverride) > Number(l.decimalPlacesShownOnFocus) && ce("The extended decimal places 'decimalPlacesShownOnFocus' [" + l.decimalPlacesShownOnFocus + "] should be greater than the 'decimalPlacesOverride' [" + l.decimalPlacesOverride + "] value. Currently, this will limit the ability of your client to manually change some of the decimal places. Do you really want to do that?", l.showWarnings), t(l.scaleDivisor) || /^[0-9]+(\.?[0-9]+)?$/.test(l.scaleDivisor) || se("The scale divisor option 'scaleDivisor' is invalid ; it should be a positive number, preferably an integer, [" + l.scaleDivisor + "] given."), t(l.scaleDecimalPlaces) || c.test(l.scaleDecimalPlaces) || se("The scale number of decimals option 'scaleDecimalPlaces' is invalid ; it should be a positive integer, [" + l.scaleDecimalPlaces + "] given."), t(l.scaleSymbol) || r(l.scaleSymbol) || se("The scale symbol option 'scaleSymbol' is invalid ; it should be a string, [" + l.scaleSymbol + "] given."), R(l.saveValueToSessionStorage) || n(l.saveValueToSessionStorage) || se("The save to session storage option 'saveValueToSessionStorage' is invalid ; it should be either 'false' or 'true', [" + l.saveValueToSessionStorage + "] given."), W(l.onInvalidPaste, ["error", "ignore", "clamp", "truncate", "replace"]) || se("The paste behavior option 'onInvalidPaste' is invalid ; it should either be 'error', 'ignore', 'clamp', 'truncate' or 'replace' (cf. documentation), [" + l.onInvalidPaste + "] given."), W(l.roundingMethod, ["S", "A", "s", "a", "B", "U", "D", "C", "F", "N05", "CHF", "U05", "D05"]) || se("The rounding method option 'roundingMethod' is invalid ; it should either be 'S', 'A', 's', 'a', 'B', 'U', 'D', 'C', 'F', 'N05', 'CHF', 'U05' or 'D05' (cf. documentation), [" + l.roundingMethod + "] given."), R(l.allowDecimalPadding) || n(l.allowDecimalPadding) || se("The control decimal padding option 'allowDecimalPadding' is invalid ; it should be either 'false' or 'true', [" + l.allowDecimalPadding + "] given."), t(l.negativeBracketsTypeOnBlur) || W(l.negativeBracketsTypeOnBlur, ["(,)", "[,]", "<,>", "{,}"]) || se("The brackets for negative values option 'negativeBracketsTypeOnBlur' is invalid ; it should either be '(,)', '[,]', '<,>' or '{,}', [" + l.negativeBracketsTypeOnBlur + "] given."), W(l.emptyInputBehavior, ["focus", "press", "always", "zero"]) || se("The display on empty string option 'emptyInputBehavior' is invalid ; it should either be 'focus', 'press', 'always' or 'zero', [" + l.emptyInputBehavior + "] given."), W(l.leadingZero, ["allow", "deny", "keep"]) || se("The leading zero behavior option 'leadingZero' is invalid ; it should either be 'allow', 'deny' or 'keep', [" + l.leadingZero + "] given."), R(l.formatOnPageLoad) || n(l.formatOnPageLoad) || se("The format on initialization option 'formatOnPageLoad' is invalid ; it should be either 'false' or 'true', [" + l.formatOnPageLoad + "] given."), R(l.selectNumberOnly) || n(l.selectNumberOnly) || se("The select number only option 'selectNumberOnly' is invalid ; it should be either 'false' or 'true', [" + l.selectNumberOnly + "] given."), t(l.defaultValueOverride) || "" === l.defaultValueOverride || u.test(l.defaultValueOverride) || se("The unformatted default value option 'defaultValueOverride' is invalid ; it should be a string that represents a positive or negative number, [" + l.defaultValueOverride + "] given."), R(l.unformatOnSubmit) || n(l.unformatOnSubmit) || se("The remove formatting on submit option 'unformatOnSubmit' is invalid ; it should be either 'false' or 'true', [" + l.unformatOnSubmit + "] given."), t(l.outputFormat) || W(l.outputFormat, ["string", "number", ".", "-.", ",", "-,", ".-", ",-"]) || se("The custom locale format option 'outputFormat' is invalid ; it should either be null, 'string', 'number', '.', '-.', ',', '-,', '.-' or ',-', [" + l.outputFormat + "] given."), R(l.failOnUnknownOption) || n(l.failOnUnknownOption) || se("The debug option 'failOnUnknownOption' is invalid ; it should be either 'false' or 'true', [" + l.failOnUnknownOption + "] given.") }, e.fn.autoValidate = h, g = function(e) { var t = !0; try { h(e) } catch (e) { t = !1 } return t }, function() { function e(e, t) { t = t || { bubbles: !1, cancelable: !1, detail: void 0 }; var a = document.createEvent("CustomEvent"); return a.initCustomEvent(e, t.bubbles, t.cancelable, t.detail), a } "function" != typeof window.CustomEvent && (e.prototype = window.Event.prototype, window.CustomEvent = e) }() }) ? i.apply(t, r) : i) || (e.exports = n), t.default = { format: c, unFormat: u, getDefaultConfig: d, getLanguages: m, validate: h, areSettingsValid: g } }.call(window) }, function(t, a) { t.exports = e }]) });


// jQuery Mask Plugin v1.14.11
// github.com/igorescobar/jQuery-Mask-Plugin
var $jscomp = { scope: {}, findInternal: function(a, l, d) { a instanceof String && (a = String(a)); for (var p = a.length, h = 0; h < p; h++) { var b = a[h]; if (l.call(d, b, h, a)) return { i: h, v: b } } return { i: -1, v: void 0 } } }; $jscomp.defineProperty = "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, l, d) { if (d.get || d.set) throw new TypeError("ES3 does not support getters and setters."); a != Array.prototype && a != Object.prototype && (a[l] = d.value) };
$jscomp.getGlobal = function(a) { return "undefined" != typeof window && window === a ? a : "undefined" != typeof global && null != global ? global : a }; $jscomp.global = $jscomp.getGlobal(this); $jscomp.polyfill = function(a, l, d, p) { if (l) { d = $jscomp.global; a = a.split("."); for (p = 0; p < a.length - 1; p++) { var h = a[p]; h in d || (d[h] = {}); d = d[h] } a = a[a.length - 1]; p = d[a]; l = l(p); l != p && null != l && $jscomp.defineProperty(d, a, { configurable: !0, writable: !0, value: l }) } };
$jscomp.polyfill("Array.prototype.find", function(a) { return a ? a : function(a, d) { return $jscomp.findInternal(this, a, d).v } }, "es6-impl", "es3");
(function(a, l, d) { "function" === typeof define && define.amd ? define(["jquery"], a) : "object" === typeof exports ? module.exports = a(require("jquery")) : a(l || d) })(function(a) {
    var l = function(b, e, f) {
        var c = {
            invalid: [], getCaret: function() { try { var a, r = 0, g = b.get(0), e = document.selection, f = g.selectionStart; if (e && -1 === navigator.appVersion.indexOf("MSIE 10")) a = e.createRange(), a.moveStart("character", -c.val().length), r = a.text.length; else if (f || "0" === f) r = f; return r } catch (C) { } }, setCaret: function(a) {
                try {
                    if (b.is(":focus")) {
                        var c,
                            g = b.get(0); g.setSelectionRange ? g.setSelectionRange(a, a) : (c = g.createTextRange(), c.collapse(!0), c.moveEnd("character", a), c.moveStart("character", a), c.select())
                    }
                } catch (B) { }
            }, events: function() {
                b.on("keydown.mask", function(a) { b.data("mask-keycode", a.keyCode || a.which); b.data("mask-previus-value", b.val()); b.data("mask-previus-caret-pos", c.getCaret()); c.maskDigitPosMapOld = c.maskDigitPosMap }).on(a.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", c.behaviour).on("paste.mask drop.mask", function() {
                    setTimeout(function() { b.keydown().keyup() },
                        100)
                }).on("change.mask", function() { b.data("changed", !0) }).on("blur.mask", function() { d === c.val() || b.data("changed") || b.trigger("change"); b.data("changed", !1) }).on("blur.mask", function() { d = c.val() }).on("focus.mask", function(b) { !0 === f.selectOnFocus && a(b.target).select() }).on("focusout.mask", function() { f.clearIfNotMatch && !h.test(c.val()) && c.val("") })
            }, getRegexMask: function() {
                for (var a = [], b, c, f, n, d = 0; d < e.length; d++)(b = m.translation[e.charAt(d)]) ? (c = b.pattern.toString().replace(/.{1}$|^.{1}/g, ""), f = b.optional,
                    (b = b.recursive) ? (a.push(e.charAt(d)), n = { digit: e.charAt(d), pattern: c }) : a.push(f || b ? c + "?" : c)) : a.push(e.charAt(d).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")); a = a.join(""); n && (a = a.replace(new RegExp("(" + n.digit + "(.*" + n.digit + ")?)"), "($1)?").replace(new RegExp(n.digit, "g"), n.pattern)); return new RegExp(a)
            }, destroyEvents: function() { b.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask ")) }, val: function(a) {
                var c = b.is("input") ? "val" : "text"; if (0 < arguments.length) {
                    if (b[c]() !== a) b[c](a);
                    c = b
                } else c = b[c](); return c
            }, calculateCaretPosition: function() {
                var a = b.data("mask-previus-value") || "", e = c.getMasked(), g = c.getCaret(); if (a !== e) {
                    var f = b.data("mask-previus-caret-pos") || 0, e = e.length, d = a.length, m = a = 0, h = 0, l = 0, k; for (k = g; k < e && c.maskDigitPosMap[k]; k++)m++; for (k = g - 1; 0 <= k && c.maskDigitPosMap[k]; k--)a++; for (k = g - 1; 0 <= k; k--)c.maskDigitPosMap[k] && h++; for (k = f - 1; 0 <= k; k--)c.maskDigitPosMapOld[k] && l++; g > d ? g = e : f >= g && f !== d ? c.maskDigitPosMapOld[g] || (f = g, g = g - (l - h) - a, c.maskDigitPosMap[g] && (g = f)) : g > f && (g =
                        g + (h - l) + m)
                } return g
            }, behaviour: function(f) { f = f || window.event; c.invalid = []; var e = b.data("mask-keycode"); if (-1 === a.inArray(e, m.byPassKeys)) { var e = c.getMasked(), g = c.getCaret(); setTimeout(function() { c.setCaret(c.calculateCaretPosition()) }, 10); c.val(e); c.setCaret(g); return c.callbacks(f) } }, getMasked: function(a, b) {
                var g = [], d = void 0 === b ? c.val() : b + "", n = 0, h = e.length, q = 0, l = d.length, k = 1, r = "push", p = -1, t = 0, y = [], v, z; f.reverse ? (r = "unshift", k = -1, v = 0, n = h - 1, q = l - 1, z = function() { return -1 < n && -1 < q }) : (v = h - 1, z = function() {
                    return n <
                        h && q < l
                }); for (var A; z();) { var x = e.charAt(n), w = d.charAt(q), u = m.translation[x]; if (u) w.match(u.pattern) ? (g[r](w), u.recursive && (-1 === p ? p = n : n === v && (n = p - k), v === p && (n -= k)), n += k) : w === A ? (t--, A = void 0) : u.optional ? (n += k, q -= k) : u.fallback ? (g[r](u.fallback), n += k, q -= k) : c.invalid.push({ p: q, v: w, e: u.pattern }), q += k; else { if (!a) g[r](x); w === x ? (y.push(q), q += k) : (A = x, y.push(q + t), t++); n += k } } d = e.charAt(v); h !== l + 1 || m.translation[d] || g.push(d); g = g.join(""); c.mapMaskdigitPositions(g, y, l); return g
            }, mapMaskdigitPositions: function(a,
                b, e) { a = f.reverse ? a.length - e : 0; c.maskDigitPosMap = {}; for (e = 0; e < b.length; e++)c.maskDigitPosMap[b[e] + a] = 1 }, callbacks: function(a) { var h = c.val(), g = h !== d, m = [h, a, b, f], q = function(a, b, c) { "function" === typeof f[a] && b && f[a].apply(this, c) }; q("onChange", !0 === g, m); q("onKeyPress", !0 === g, m); q("onComplete", h.length === e.length, m); q("onInvalid", 0 < c.invalid.length, [h, a, b, c.invalid, f]) }
        }; b = a(b); var m = this, d = c.val(), h; e = "function" === typeof e ? e(c.val(), void 0, b, f) : e; m.mask = e; m.options = f; m.remove = function() {
            var a = c.getCaret();
            c.destroyEvents(); c.val(m.getCleanVal()); c.setCaret(a); return b
        }; m.getCleanVal = function() { return c.getMasked(!0) }; m.getMaskedVal = function(a) { return c.getMasked(!1, a) }; m.init = function(d) {
            d = d || !1; f = f || {}; m.clearIfNotMatch = a.jMaskGlobals.clearIfNotMatch; m.byPassKeys = a.jMaskGlobals.byPassKeys; m.translation = a.extend({}, a.jMaskGlobals.translation, f.translation); m = a.extend(!0, {}, m, f); h = c.getRegexMask(); if (d) c.events(), c.val(c.getMasked()); else {
                f.placeholder && b.attr("placeholder", f.placeholder); b.data("mask") &&
                    b.attr("autocomplete", "off"); d = 0; for (var l = !0; d < e.length; d++) { var g = m.translation[e.charAt(d)]; if (g && g.recursive) { l = !1; break } } l && b.attr("maxlength", e.length); c.destroyEvents(); c.events(); d = c.getCaret(); c.val(c.getMasked()); c.setCaret(d)
            }
        }; m.init(!b.is("input"))
    }; a.maskWatchers = {}; var d = function() {
        var b = a(this), e = {}, f = b.attr("data-mask"); b.attr("data-mask-reverse") && (e.reverse = !0); b.attr("data-mask-clearifnotmatch") && (e.clearIfNotMatch = !0); "true" === b.attr("data-mask-selectonfocus") && (e.selectOnFocus =
            !0); if (p(b, f, e)) return b.data("mask", new l(this, f, e))
    }, p = function(b, e, f) { f = f || {}; var c = a(b).data("mask"), d = JSON.stringify; b = a(b).val() || a(b).text(); try { return "function" === typeof e && (e = e(b)), "object" !== typeof c || d(c.options) !== d(f) || c.mask !== e } catch (t) { } }, h = function(a) { var b = document.createElement("div"), d; a = "on" + a; d = a in b; d || (b.setAttribute(a, "return;"), d = "function" === typeof b[a]); return d }; a.fn.mask = function(b, d) {
        d = d || {}; var e = this.selector, c = a.jMaskGlobals, h = c.watchInterval, c = d.watchInputs || c.watchInputs,
            t = function() { if (p(this, b, d)) return a(this).data("mask", new l(this, b, d)) }; a(this).each(t); e && "" !== e && c && (clearInterval(a.maskWatchers[e]), a.maskWatchers[e] = setInterval(function() { a(document).find(e).each(t) }, h)); return this
    }; a.fn.masked = function(a) { return this.data("mask").getMaskedVal(a) }; a.fn.unmask = function() { clearInterval(a.maskWatchers[this.selector]); delete a.maskWatchers[this.selector]; return this.each(function() { var b = a(this).data("mask"); b && b.remove().removeData("mask") }) }; a.fn.cleanVal = function() { return this.data("mask").getCleanVal() };
    a.applyDataMask = function(b) { b = b || a.jMaskGlobals.maskElements; (b instanceof a ? b : a(b)).filter(a.jMaskGlobals.dataMaskAttr).each(d) }; h = {
        maskElements: "input,td,span,div", dataMaskAttr: "*[data-mask]", dataMask: !0, watchInterval: 300, watchInputs: !0, useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) && h("input"), watchDataMask: !1, byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91], translation: {
            0: { pattern: /\d/ }, 9: { pattern: /\d/, optional: !0 }, "#": { pattern: /\d/, recursive: !0 }, A: { pattern: /[a-zA-Z0-9]/ },
            S: { pattern: /[a-zA-Z]/ }
        }
    }; a.jMaskGlobals = a.jMaskGlobals || {}; h = a.jMaskGlobals = a.extend(!0, {}, h, a.jMaskGlobals); h.dataMask && a.applyDataMask(); setInterval(function() { a.jMaskGlobals.watchDataMask && a.applyDataMask() }, h.watchInterval)
}, window.jQuery, window.Zepto);


/*!
    moment.js v2.12.0
*/
!function(a, b) { "object" == typeof exports && "undefined" != typeof module ? module.exports = b() : "function" == typeof define && define.amd ? define(b) : a.moment = b() }(this, function() {
    "use strict"; function a() { return Zc.apply(null, arguments) } function b(a) { Zc = a } function c(a) { return a instanceof Array || "[object Array]" === Object.prototype.toString.call(a) } function d(a) { return a instanceof Date || "[object Date]" === Object.prototype.toString.call(a) } function e(a, b) { var c, d = []; for (c = 0; c < a.length; ++c)d.push(b(a[c], c)); return d } function f(a, b) { return Object.prototype.hasOwnProperty.call(a, b) } function g(a, b) { for (var c in b) f(b, c) && (a[c] = b[c]); return f(b, "toString") && (a.toString = b.toString), f(b, "valueOf") && (a.valueOf = b.valueOf), a } function h(a, b, c, d) { return Ia(a, b, c, d, !0).utc() } function i() { return { empty: !1, unusedTokens: [], unusedInput: [], overflow: -2, charsLeftOver: 0, nullInput: !1, invalidMonth: null, invalidFormat: !1, userInvalidated: !1, iso: !1 } } function j(a) { return null == a._pf && (a._pf = i()), a._pf } function k(a) { if (null == a._isValid) { var b = j(a); a._isValid = !(isNaN(a._d.getTime()) || !(b.overflow < 0) || b.empty || b.invalidMonth || b.invalidWeekday || b.nullInput || b.invalidFormat || b.userInvalidated), a._strict && (a._isValid = a._isValid && 0 === b.charsLeftOver && 0 === b.unusedTokens.length && void 0 === b.bigHour) } return a._isValid } function l(a) { var b = h(NaN); return null != a ? g(j(b), a) : j(b).userInvalidated = !0, b } function m(a) { return void 0 === a } function n(a, b) { var c, d, e; if (m(b._isAMomentObject) || (a._isAMomentObject = b._isAMomentObject), m(b._i) || (a._i = b._i), m(b._f) || (a._f = b._f), m(b._l) || (a._l = b._l), m(b._strict) || (a._strict = b._strict), m(b._tzm) || (a._tzm = b._tzm), m(b._isUTC) || (a._isUTC = b._isUTC), m(b._offset) || (a._offset = b._offset), m(b._pf) || (a._pf = j(b)), m(b._locale) || (a._locale = b._locale), $c.length > 0) for (c in $c) d = $c[c], e = b[d], m(e) || (a[d] = e); return a } function o(b) { n(this, b), this._d = new Date(null != b._d ? b._d.getTime() : NaN), _c === !1 && (_c = !0, a.updateOffset(this), _c = !1) } function p(a) { return a instanceof o || null != a && null != a._isAMomentObject } function q(a) { return 0 > a ? Math.ceil(a) : Math.floor(a) } function r(a) { var b = +a, c = 0; return 0 !== b && isFinite(b) && (c = q(b)), c } function s(a, b, c) { var d, e = Math.min(a.length, b.length), f = Math.abs(a.length - b.length), g = 0; for (d = 0; e > d; d++)(c && a[d] !== b[d] || !c && r(a[d]) !== r(b[d])) && g++; return g + f } function t(b) { a.suppressDeprecationWarnings === !1 && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + b) } function u(a, b) { var c = !0; return g(function() { return c && (t(a + "\nArguments: " + Array.prototype.slice.call(arguments).join(", ") + "\n" + (new Error).stack), c = !1), b.apply(this, arguments) }, b) } function v(a, b) { ad[a] || (t(b), ad[a] = !0) } function w(a) { return a instanceof Function || "[object Function]" === Object.prototype.toString.call(a) } function x(a) { return "[object Object]" === Object.prototype.toString.call(a) } function y(a) { var b, c; for (c in a) b = a[c], w(b) ? this[c] = b : this["_" + c] = b; this._config = a, this._ordinalParseLenient = new RegExp(this._ordinalParse.source + "|" + /\d{1,2}/.source) } function z(a, b) { var c, d = g({}, a); for (c in b) f(b, c) && (x(a[c]) && x(b[c]) ? (d[c] = {}, g(d[c], a[c]), g(d[c], b[c])) : null != b[c] ? d[c] = b[c] : delete d[c]); return d } function A(a) { null != a && this.set(a) } function B(a) { return a ? a.toLowerCase().replace("_", "-") : a } function C(a) { for (var b, c, d, e, f = 0; f < a.length;) { for (e = B(a[f]).split("-"), b = e.length, c = B(a[f + 1]), c = c ? c.split("-") : null; b > 0;) { if (d = D(e.slice(0, b).join("-"))) return d; if (c && c.length >= b && s(e, c, !0) >= b - 1) break; b-- } f++ } return null } function D(a) { var b = null; if (!cd[a] && "undefined" != typeof module && module && module.exports) try { b = bd._abbr, require("./locale/" + a), E(b) } catch (c) { } return cd[a] } function E(a, b) { var c; return a && (c = m(b) ? H(a) : F(a, b), c && (bd = c)), bd._abbr } function F(a, b) { return null !== b ? (b.abbr = a, null != cd[a] ? (v("defineLocaleOverride", "use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale"), b = z(cd[a]._config, b)) : null != b.parentLocale && (null != cd[b.parentLocale] ? b = z(cd[b.parentLocale]._config, b) : v("parentLocaleUndefined", "specified parentLocale is not defined yet")), cd[a] = new A(b), E(a), cd[a]) : (delete cd[a], null) } function G(a, b) { if (null != b) { var c; null != cd[a] && (b = z(cd[a]._config, b)), c = new A(b), c.parentLocale = cd[a], cd[a] = c, E(a) } else null != cd[a] && (null != cd[a].parentLocale ? cd[a] = cd[a].parentLocale : null != cd[a] && delete cd[a]); return cd[a] } function H(a) { var b; if (a && a._locale && a._locale._abbr && (a = a._locale._abbr), !a) return bd; if (!c(a)) { if (b = D(a)) return b; a = [a] } return C(a) } function I() { return Object.keys(cd) } function J(a, b) { var c = a.toLowerCase(); dd[c] = dd[c + "s"] = dd[b] = a } function K(a) { return "string" == typeof a ? dd[a] || dd[a.toLowerCase()] : void 0 } function L(a) { var b, c, d = {}; for (c in a) f(a, c) && (b = K(c), b && (d[b] = a[c])); return d } function M(b, c) { return function(d) { return null != d ? (O(this, b, d), a.updateOffset(this, c), this) : N(this, b) } } function N(a, b) { return a.isValid() ? a._d["get" + (a._isUTC ? "UTC" : "") + b]() : NaN } function O(a, b, c) { a.isValid() && a._d["set" + (a._isUTC ? "UTC" : "") + b](c) } function P(a, b) { var c; if ("object" == typeof a) for (c in a) this.set(c, a[c]); else if (a = K(a), w(this[a])) return this[a](b); return this } function Q(a, b, c) { var d = "" + Math.abs(a), e = b - d.length, f = a >= 0; return (f ? c ? "+" : "" : "-") + Math.pow(10, Math.max(0, e)).toString().substr(1) + d } function R(a, b, c, d) { var e = d; "string" == typeof d && (e = function() { return this[d]() }), a && (hd[a] = e), b && (hd[b[0]] = function() { return Q(e.apply(this, arguments), b[1], b[2]) }), c && (hd[c] = function() { return this.localeData().ordinal(e.apply(this, arguments), a) }) } function S(a) { return a.match(/\[[\s\S]/) ? a.replace(/^\[|\]$/g, "") : a.replace(/\\/g, "") } function T(a) { var b, c, d = a.match(ed); for (b = 0, c = d.length; c > b; b++)hd[d[b]] ? d[b] = hd[d[b]] : d[b] = S(d[b]); return function(e) { var f = ""; for (b = 0; c > b; b++)f += d[b] instanceof Function ? d[b].call(e, a) : d[b]; return f } } function U(a, b) { return a.isValid() ? (b = V(b, a.localeData()), gd[b] = gd[b] || T(b), gd[b](a)) : a.localeData().invalidDate() } function V(a, b) { function c(a) { return b.longDateFormat(a) || a } var d = 5; for (fd.lastIndex = 0; d >= 0 && fd.test(a);)a = a.replace(fd, c), fd.lastIndex = 0, d -= 1; return a } function W(a, b, c) { zd[a] = w(b) ? b : function(a, d) { return a && c ? c : b } } function X(a, b) { return f(zd, a) ? zd[a](b._strict, b._locale) : new RegExp(Y(a)) } function Y(a) { return Z(a.replace("\\", "").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(a, b, c, d, e) { return b || c || d || e })) } function Z(a) { return a.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&") } function $(a, b) { var c, d = b; for ("string" == typeof a && (a = [a]), "number" == typeof b && (d = function(a, c) { c[b] = r(a) }), c = 0; c < a.length; c++)Ad[a[c]] = d } function _(a, b) { $(a, function(a, c, d, e) { d._w = d._w || {}, b(a, d._w, d, e) }) } function aa(a, b, c) { null != b && f(Ad, a) && Ad[a](b, c._a, c, a) } function ba(a, b) { return new Date(Date.UTC(a, b + 1, 0)).getUTCDate() } function ca(a, b) { return c(this._months) ? this._months[a.month()] : this._months[Kd.test(b) ? "format" : "standalone"][a.month()] } function da(a, b) { return c(this._monthsShort) ? this._monthsShort[a.month()] : this._monthsShort[Kd.test(b) ? "format" : "standalone"][a.month()] } function ea(a, b, c) { var d, e, f; for (this._monthsParse || (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = []), d = 0; 12 > d; d++) { if (e = h([2e3, d]), c && !this._longMonthsParse[d] && (this._longMonthsParse[d] = new RegExp("^" + this.months(e, "").replace(".", "") + "$", "i"), this._shortMonthsParse[d] = new RegExp("^" + this.monthsShort(e, "").replace(".", "") + "$", "i")), c || this._monthsParse[d] || (f = "^" + this.months(e, "") + "|^" + this.monthsShort(e, ""), this._monthsParse[d] = new RegExp(f.replace(".", ""), "i")), c && "MMMM" === b && this._longMonthsParse[d].test(a)) return d; if (c && "MMM" === b && this._shortMonthsParse[d].test(a)) return d; if (!c && this._monthsParse[d].test(a)) return d } } function fa(a, b) { var c; if (!a.isValid()) return a; if ("string" == typeof b) if (/^\d+$/.test(b)) b = r(b); else if (b = a.localeData().monthsParse(b), "number" != typeof b) return a; return c = Math.min(a.date(), ba(a.year(), b)), a._d["set" + (a._isUTC ? "UTC" : "") + "Month"](b, c), a } function ga(b) { return null != b ? (fa(this, b), a.updateOffset(this, !0), this) : N(this, "Month") } function ha() { return ba(this.year(), this.month()) } function ia(a) { return this._monthsParseExact ? (f(this, "_monthsRegex") || ka.call(this), a ? this._monthsShortStrictRegex : this._monthsShortRegex) : this._monthsShortStrictRegex && a ? this._monthsShortStrictRegex : this._monthsShortRegex } function ja(a) { return this._monthsParseExact ? (f(this, "_monthsRegex") || ka.call(this), a ? this._monthsStrictRegex : this._monthsRegex) : this._monthsStrictRegex && a ? this._monthsStrictRegex : this._monthsRegex } function ka() { function a(a, b) { return b.length - a.length } var b, c, d = [], e = [], f = []; for (b = 0; 12 > b; b++)c = h([2e3, b]), d.push(this.monthsShort(c, "")), e.push(this.months(c, "")), f.push(this.months(c, "")), f.push(this.monthsShort(c, "")); for (d.sort(a), e.sort(a), f.sort(a), b = 0; 12 > b; b++)d[b] = Z(d[b]), e[b] = Z(e[b]), f[b] = Z(f[b]); this._monthsRegex = new RegExp("^(" + f.join("|") + ")", "i"), this._monthsShortRegex = this._monthsRegex, this._monthsStrictRegex = new RegExp("^(" + e.join("|") + ")$", "i"), this._monthsShortStrictRegex = new RegExp("^(" + d.join("|") + ")$", "i") } function la(a) { var b, c = a._a; return c && -2 === j(a).overflow && (b = c[Cd] < 0 || c[Cd] > 11 ? Cd : c[Dd] < 1 || c[Dd] > ba(c[Bd], c[Cd]) ? Dd : c[Ed] < 0 || c[Ed] > 24 || 24 === c[Ed] && (0 !== c[Fd] || 0 !== c[Gd] || 0 !== c[Hd]) ? Ed : c[Fd] < 0 || c[Fd] > 59 ? Fd : c[Gd] < 0 || c[Gd] > 59 ? Gd : c[Hd] < 0 || c[Hd] > 999 ? Hd : -1, j(a)._overflowDayOfYear && (Bd > b || b > Dd) && (b = Dd), j(a)._overflowWeeks && -1 === b && (b = Id), j(a)._overflowWeekday && -1 === b && (b = Jd), j(a).overflow = b), a } function ma(a) { var b, c, d, e, f, g, h = a._i, i = Pd.exec(h) || Qd.exec(h); if (i) { for (j(a).iso = !0, b = 0, c = Sd.length; c > b; b++)if (Sd[b][1].exec(i[1])) { e = Sd[b][0], d = Sd[b][2] !== !1; break } if (null == e) return void (a._isValid = !1); if (i[3]) { for (b = 0, c = Td.length; c > b; b++)if (Td[b][1].exec(i[3])) { f = (i[2] || " ") + Td[b][0]; break } if (null == f) return void (a._isValid = !1) } if (!d && null != f) return void (a._isValid = !1); if (i[4]) { if (!Rd.exec(i[4])) return void (a._isValid = !1); g = "Z" } a._f = e + (f || "") + (g || ""), Ba(a) } else a._isValid = !1 } function na(b) { var c = Ud.exec(b._i); return null !== c ? void (b._d = new Date(+c[1])) : (ma(b), void (b._isValid === !1 && (delete b._isValid, a.createFromInputFallback(b)))) } function oa(a, b, c, d, e, f, g) { var h = new Date(a, b, c, d, e, f, g); return 100 > a && a >= 0 && isFinite(h.getFullYear()) && h.setFullYear(a), h } function pa(a) { var b = new Date(Date.UTC.apply(null, arguments)); return 100 > a && a >= 0 && isFinite(b.getUTCFullYear()) && b.setUTCFullYear(a), b } function qa(a) { return ra(a) ? 366 : 365 } function ra(a) { return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0 } function sa() { return ra(this.year()) } function ta(a, b, c) { var d = 7 + b - c, e = (7 + pa(a, 0, d).getUTCDay() - b) % 7; return -e + d - 1 } function ua(a, b, c, d, e) { var f, g, h = (7 + c - d) % 7, i = ta(a, d, e), j = 1 + 7 * (b - 1) + h + i; return 0 >= j ? (f = a - 1, g = qa(f) + j) : j > qa(a) ? (f = a + 1, g = j - qa(a)) : (f = a, g = j), { year: f, dayOfYear: g } } function va(a, b, c) { var d, e, f = ta(a.year(), b, c), g = Math.floor((a.dayOfYear() - f - 1) / 7) + 1; return 1 > g ? (e = a.year() - 1, d = g + wa(e, b, c)) : g > wa(a.year(), b, c) ? (d = g - wa(a.year(), b, c), e = a.year() + 1) : (e = a.year(), d = g), { week: d, year: e } } function wa(a, b, c) { var d = ta(a, b, c), e = ta(a + 1, b, c); return (qa(a) - d + e) / 7 } function xa(a, b, c) { return null != a ? a : null != b ? b : c } function ya(b) { var c = new Date(a.now()); return b._useUTC ? [c.getUTCFullYear(), c.getUTCMonth(), c.getUTCDate()] : [c.getFullYear(), c.getMonth(), c.getDate()] } function za(a) { var b, c, d, e, f = []; if (!a._d) { for (d = ya(a), a._w && null == a._a[Dd] && null == a._a[Cd] && Aa(a), a._dayOfYear && (e = xa(a._a[Bd], d[Bd]), a._dayOfYear > qa(e) && (j(a)._overflowDayOfYear = !0), c = pa(e, 0, a._dayOfYear), a._a[Cd] = c.getUTCMonth(), a._a[Dd] = c.getUTCDate()), b = 0; 3 > b && null == a._a[b]; ++b)a._a[b] = f[b] = d[b]; for (; 7 > b; b++)a._a[b] = f[b] = null == a._a[b] ? 2 === b ? 1 : 0 : a._a[b]; 24 === a._a[Ed] && 0 === a._a[Fd] && 0 === a._a[Gd] && 0 === a._a[Hd] && (a._nextDay = !0, a._a[Ed] = 0), a._d = (a._useUTC ? pa : oa).apply(null, f), null != a._tzm && a._d.setUTCMinutes(a._d.getUTCMinutes() - a._tzm), a._nextDay && (a._a[Ed] = 24) } } function Aa(a) { var b, c, d, e, f, g, h, i; b = a._w, null != b.GG || null != b.W || null != b.E ? (f = 1, g = 4, c = xa(b.GG, a._a[Bd], va(Ja(), 1, 4).year), d = xa(b.W, 1), e = xa(b.E, 1), (1 > e || e > 7) && (i = !0)) : (f = a._locale._week.dow, g = a._locale._week.doy, c = xa(b.gg, a._a[Bd], va(Ja(), f, g).year), d = xa(b.w, 1), null != b.d ? (e = b.d, (0 > e || e > 6) && (i = !0)) : null != b.e ? (e = b.e + f, (b.e < 0 || b.e > 6) && (i = !0)) : e = f), 1 > d || d > wa(c, f, g) ? j(a)._overflowWeeks = !0 : null != i ? j(a)._overflowWeekday = !0 : (h = ua(c, d, e, f, g), a._a[Bd] = h.year, a._dayOfYear = h.dayOfYear) } function Ba(b) { if (b._f === a.ISO_8601) return void ma(b); b._a = [], j(b).empty = !0; var c, d, e, f, g, h = "" + b._i, i = h.length, k = 0; for (e = V(b._f, b._locale).match(ed) || [], c = 0; c < e.length; c++)f = e[c], d = (h.match(X(f, b)) || [])[0], d && (g = h.substr(0, h.indexOf(d)), g.length > 0 && j(b).unusedInput.push(g), h = h.slice(h.indexOf(d) + d.length), k += d.length), hd[f] ? (d ? j(b).empty = !1 : j(b).unusedTokens.push(f), aa(f, d, b)) : b._strict && !d && j(b).unusedTokens.push(f); j(b).charsLeftOver = i - k, h.length > 0 && j(b).unusedInput.push(h), j(b).bigHour === !0 && b._a[Ed] <= 12 && b._a[Ed] > 0 && (j(b).bigHour = void 0), b._a[Ed] = Ca(b._locale, b._a[Ed], b._meridiem), za(b), la(b) } function Ca(a, b, c) { var d; return null == c ? b : null != a.meridiemHour ? a.meridiemHour(b, c) : null != a.isPM ? (d = a.isPM(c), d && 12 > b && (b += 12), d || 12 !== b || (b = 0), b) : b } function Da(a) { var b, c, d, e, f; if (0 === a._f.length) return j(a).invalidFormat = !0, void (a._d = new Date(NaN)); for (e = 0; e < a._f.length; e++)f = 0, b = n({}, a), null != a._useUTC && (b._useUTC = a._useUTC), b._f = a._f[e], Ba(b), k(b) && (f += j(b).charsLeftOver, f += 10 * j(b).unusedTokens.length, j(b).score = f, (null == d || d > f) && (d = f, c = b)); g(a, c || b) } function Ea(a) { if (!a._d) { var b = L(a._i); a._a = e([b.year, b.month, b.day || b.date, b.hour, b.minute, b.second, b.millisecond], function(a) { return a && parseInt(a, 10) }), za(a) } } function Fa(a) { var b = new o(la(Ga(a))); return b._nextDay && (b.add(1, "d"), b._nextDay = void 0), b } function Ga(a) { var b = a._i, e = a._f; return a._locale = a._locale || H(a._l), null === b || void 0 === e && "" === b ? l({ nullInput: !0 }) : ("string" == typeof b && (a._i = b = a._locale.preparse(b)), p(b) ? new o(la(b)) : (c(e) ? Da(a) : e ? Ba(a) : d(b) ? a._d = b : Ha(a), k(a) || (a._d = null), a)) } function Ha(b) { var f = b._i; void 0 === f ? b._d = new Date(a.now()) : d(f) ? b._d = new Date(+f) : "string" == typeof f ? na(b) : c(f) ? (b._a = e(f.slice(0), function(a) { return parseInt(a, 10) }), za(b)) : "object" == typeof f ? Ea(b) : "number" == typeof f ? b._d = new Date(f) : a.createFromInputFallback(b) } function Ia(a, b, c, d, e) { var f = {}; return "boolean" == typeof c && (d = c, c = void 0), f._isAMomentObject = !0, f._useUTC = f._isUTC = e, f._l = c, f._i = a, f._f = b, f._strict = d, Fa(f) } function Ja(a, b, c, d) { return Ia(a, b, c, d, !1) } function Ka(a, b) { var d, e; if (1 === b.length && c(b[0]) && (b = b[0]), !b.length) return Ja(); for (d = b[0], e = 1; e < b.length; ++e)(!b[e].isValid() || b[e][a](d)) && (d = b[e]); return d } function La() { var a = [].slice.call(arguments, 0); return Ka("isBefore", a) } function Ma() { var a = [].slice.call(arguments, 0); return Ka("isAfter", a) } function Na(a) { var b = L(a), c = b.year || 0, d = b.quarter || 0, e = b.month || 0, f = b.week || 0, g = b.day || 0, h = b.hour || 0, i = b.minute || 0, j = b.second || 0, k = b.millisecond || 0; this._milliseconds = +k + 1e3 * j + 6e4 * i + 36e5 * h, this._days = +g + 7 * f, this._months = +e + 3 * d + 12 * c, this._data = {}, this._locale = H(), this._bubble() } function Oa(a) { return a instanceof Na } function Pa(a, b) { R(a, 0, 0, function() { var a = this.utcOffset(), c = "+"; return 0 > a && (a = -a, c = "-"), c + Q(~~(a / 60), 2) + b + Q(~~a % 60, 2) }) } function Qa(a, b) { var c = (b || "").match(a) || [], d = c[c.length - 1] || [], e = (d + "").match(Zd) || ["-", 0, 0], f = +(60 * e[1]) + r(e[2]); return "+" === e[0] ? f : -f } function Ra(b, c) { var e, f; return c._isUTC ? (e = c.clone(), f = (p(b) || d(b) ? +b : +Ja(b)) - +e, e._d.setTime(+e._d + f), a.updateOffset(e, !1), e) : Ja(b).local() } function Sa(a) { return 15 * -Math.round(a._d.getTimezoneOffset() / 15) } function Ta(b, c) { var d, e = this._offset || 0; return this.isValid() ? null != b ? ("string" == typeof b ? b = Qa(wd, b) : Math.abs(b) < 16 && (b = 60 * b), !this._isUTC && c && (d = Sa(this)), this._offset = b, this._isUTC = !0, null != d && this.add(d, "m"), e !== b && (!c || this._changeInProgress ? ib(this, cb(b - e, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, a.updateOffset(this, !0), this._changeInProgress = null)), this) : this._isUTC ? e : Sa(this) : null != b ? this : NaN } function Ua(a, b) { return null != a ? ("string" != typeof a && (a = -a), this.utcOffset(a, b), this) : -this.utcOffset() } function Va(a) { return this.utcOffset(0, a) } function Wa(a) { return this._isUTC && (this.utcOffset(0, a), this._isUTC = !1, a && this.subtract(Sa(this), "m")), this } function Xa() { return this._tzm ? this.utcOffset(this._tzm) : "string" == typeof this._i && this.utcOffset(Qa(vd, this._i)), this } function Ya(a) { return this.isValid() ? (a = a ? Ja(a).utcOffset() : 0, (this.utcOffset() - a) % 60 === 0) : !1 } function Za() { return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset() } function $a() { if (!m(this._isDSTShifted)) return this._isDSTShifted; var a = {}; if (n(a, this), a = Ga(a), a._a) { var b = a._isUTC ? h(a._a) : Ja(a._a); this._isDSTShifted = this.isValid() && s(a._a, b.toArray()) > 0 } else this._isDSTShifted = !1; return this._isDSTShifted } function _a() { return this.isValid() ? !this._isUTC : !1 } function ab() { return this.isValid() ? this._isUTC : !1 } function bb() { return this.isValid() ? this._isUTC && 0 === this._offset : !1 } function cb(a, b) { var c, d, e, g = a, h = null; return Oa(a) ? g = { ms: a._milliseconds, d: a._days, M: a._months } : "number" == typeof a ? (g = {}, b ? g[b] = a : g.milliseconds = a) : (h = $d.exec(a)) ? (c = "-" === h[1] ? -1 : 1, g = { y: 0, d: r(h[Dd]) * c, h: r(h[Ed]) * c, m: r(h[Fd]) * c, s: r(h[Gd]) * c, ms: r(h[Hd]) * c }) : (h = _d.exec(a)) ? (c = "-" === h[1] ? -1 : 1, g = { y: db(h[2], c), M: db(h[3], c), w: db(h[4], c), d: db(h[5], c), h: db(h[6], c), m: db(h[7], c), s: db(h[8], c) }) : null == g ? g = {} : "object" == typeof g && ("from" in g || "to" in g) && (e = fb(Ja(g.from), Ja(g.to)), g = {}, g.ms = e.milliseconds, g.M = e.months), d = new Na(g), Oa(a) && f(a, "_locale") && (d._locale = a._locale), d } function db(a, b) { var c = a && parseFloat(a.replace(",", ".")); return (isNaN(c) ? 0 : c) * b } function eb(a, b) { var c = { milliseconds: 0, months: 0 }; return c.months = b.month() - a.month() + 12 * (b.year() - a.year()), a.clone().add(c.months, "M").isAfter(b) && --c.months, c.milliseconds = +b - +a.clone().add(c.months, "M"), c } function fb(a, b) { var c; return a.isValid() && b.isValid() ? (b = Ra(b, a), a.isBefore(b) ? c = eb(a, b) : (c = eb(b, a), c.milliseconds = -c.milliseconds, c.months = -c.months), c) : { milliseconds: 0, months: 0 } } function gb(a) { return 0 > a ? -1 * Math.round(-1 * a) : Math.round(a) } function hb(a, b) { return function(c, d) { var e, f; return null === d || isNaN(+d) || (v(b, "moment()." + b + "(period, number) is deprecated. Please use moment()." + b + "(number, period)."), f = c, c = d, d = f), c = "string" == typeof c ? +c : c, e = cb(c, d), ib(this, e, a), this } } function ib(b, c, d, e) { var f = c._milliseconds, g = gb(c._days), h = gb(c._months); b.isValid() && (e = null == e ? !0 : e, f && b._d.setTime(+b._d + f * d), g && O(b, "Date", N(b, "Date") + g * d), h && fa(b, N(b, "Month") + h * d), e && a.updateOffset(b, g || h)) } function jb(a, b) { var c = a || Ja(), d = Ra(c, this).startOf("day"), e = this.diff(d, "days", !0), f = -6 > e ? "sameElse" : -1 > e ? "lastWeek" : 0 > e ? "lastDay" : 1 > e ? "sameDay" : 2 > e ? "nextDay" : 7 > e ? "nextWeek" : "sameElse", g = b && (w(b[f]) ? b[f]() : b[f]); return this.format(g || this.localeData().calendar(f, this, Ja(c))) } function kb() { return new o(this) } function lb(a, b) { var c = p(a) ? a : Ja(a); return this.isValid() && c.isValid() ? (b = K(m(b) ? "millisecond" : b), "millisecond" === b ? +this > +c : +c < +this.clone().startOf(b)) : !1 } function mb(a, b) { var c = p(a) ? a : Ja(a); return this.isValid() && c.isValid() ? (b = K(m(b) ? "millisecond" : b), "millisecond" === b ? +c > +this : +this.clone().endOf(b) < +c) : !1 } function nb(a, b, c) { return this.isAfter(a, c) && this.isBefore(b, c) } function ob(a, b) { var c, d = p(a) ? a : Ja(a); return this.isValid() && d.isValid() ? (b = K(b || "millisecond"), "millisecond" === b ? +this === +d : (c = +d, +this.clone().startOf(b) <= c && c <= +this.clone().endOf(b))) : !1 } function pb(a, b) { return this.isSame(a, b) || this.isAfter(a, b) } function qb(a, b) { return this.isSame(a, b) || this.isBefore(a, b) } function rb(a, b, c) { var d, e, f, g; return this.isValid() ? (d = Ra(a, this), d.isValid() ? (e = 6e4 * (d.utcOffset() - this.utcOffset()), b = K(b), "year" === b || "month" === b || "quarter" === b ? (g = sb(this, d), "quarter" === b ? g /= 3 : "year" === b && (g /= 12)) : (f = this - d, g = "second" === b ? f / 1e3 : "minute" === b ? f / 6e4 : "hour" === b ? f / 36e5 : "day" === b ? (f - e) / 864e5 : "week" === b ? (f - e) / 6048e5 : f), c ? g : q(g)) : NaN) : NaN } function sb(a, b) { var c, d, e = 12 * (b.year() - a.year()) + (b.month() - a.month()), f = a.clone().add(e, "months"); return 0 > b - f ? (c = a.clone().add(e - 1, "months"), d = (b - f) / (f - c)) : (c = a.clone().add(e + 1, "months"), d = (b - f) / (c - f)), -(e + d) } function tb() { return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ") } function ub() { var a = this.clone().utc(); return 0 < a.year() && a.year() <= 9999 ? w(Date.prototype.toISOString) ? this.toDate().toISOString() : U(a, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : U(a, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]") } function vb(b) { var c = U(this, b || a.defaultFormat); return this.localeData().postformat(c) } function wb(a, b) { return this.isValid() && (p(a) && a.isValid() || Ja(a).isValid()) ? cb({ to: this, from: a }).locale(this.locale()).humanize(!b) : this.localeData().invalidDate() } function xb(a) { return this.from(Ja(), a) } function yb(a, b) { return this.isValid() && (p(a) && a.isValid() || Ja(a).isValid()) ? cb({ from: this, to: a }).locale(this.locale()).humanize(!b) : this.localeData().invalidDate() } function zb(a) { return this.to(Ja(), a) } function Ab(a) { var b; return void 0 === a ? this._locale._abbr : (b = H(a), null != b && (this._locale = b), this) } function Bb() { return this._locale } function Cb(a) { switch (a = K(a)) { case "year": this.month(0); case "quarter": case "month": this.date(1); case "week": case "isoWeek": case "day": this.hours(0); case "hour": this.minutes(0); case "minute": this.seconds(0); case "second": this.milliseconds(0) }return "week" === a && this.weekday(0), "isoWeek" === a && this.isoWeekday(1), "quarter" === a && this.month(3 * Math.floor(this.month() / 3)), this } function Db(a) { return a = K(a), void 0 === a || "millisecond" === a ? this : this.startOf(a).add(1, "isoWeek" === a ? "week" : a).subtract(1, "ms") } function Eb() { return +this._d - 6e4 * (this._offset || 0) } function Fb() { return Math.floor(+this / 1e3) } function Gb() { return this._offset ? new Date(+this) : this._d } function Hb() { var a = this; return [a.year(), a.month(), a.date(), a.hour(), a.minute(), a.second(), a.millisecond()] } function Ib() { var a = this; return { years: a.year(), months: a.month(), date: a.date(), hours: a.hours(), minutes: a.minutes(), seconds: a.seconds(), milliseconds: a.milliseconds() } } function Jb() { return this.isValid() ? this.toISOString() : null } function Kb() { return k(this) } function Lb() { return g({}, j(this)) } function Mb() { return j(this).overflow } function Nb() { return { input: this._i, format: this._f, locale: this._locale, isUTC: this._isUTC, strict: this._strict } } function Ob(a, b) { R(0, [a, a.length], 0, b) } function Pb(a) { return Tb.call(this, a, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy) } function Qb(a) { return Tb.call(this, a, this.isoWeek(), this.isoWeekday(), 1, 4) } function Rb() { return wa(this.year(), 1, 4) } function Sb() { var a = this.localeData()._week; return wa(this.year(), a.dow, a.doy) } function Tb(a, b, c, d, e) { var f; return null == a ? va(this, d, e).year : (f = wa(a, d, e), b > f && (b = f), Ub.call(this, a, b, c, d, e)) } function Ub(a, b, c, d, e) { var f = ua(a, b, c, d, e), g = pa(f.year, 0, f.dayOfYear); return this.year(g.getUTCFullYear()), this.month(g.getUTCMonth()), this.date(g.getUTCDate()), this } function Vb(a) { return null == a ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (a - 1) + this.month() % 3) } function Wb(a) { return va(a, this._week.dow, this._week.doy).week } function Xb() { return this._week.dow } function Yb() { return this._week.doy } function Zb(a) { var b = this.localeData().week(this); return null == a ? b : this.add(7 * (a - b), "d") } function $b(a) { var b = va(this, 1, 4).week; return null == a ? b : this.add(7 * (a - b), "d") } function _b(a, b) { return "string" != typeof a ? a : isNaN(a) ? (a = b.weekdaysParse(a), "number" == typeof a ? a : null) : parseInt(a, 10) } function ac(a, b) { return c(this._weekdays) ? this._weekdays[a.day()] : this._weekdays[this._weekdays.isFormat.test(b) ? "format" : "standalone"][a.day()] } function bc(a) { return this._weekdaysShort[a.day()] } function cc(a) { return this._weekdaysMin[a.day()] } function dc(a, b, c) { var d, e, f; for (this._weekdaysParse || (this._weekdaysParse = [], this._minWeekdaysParse = [], this._shortWeekdaysParse = [], this._fullWeekdaysParse = []), d = 0; 7 > d; d++) { if (e = Ja([2e3, 1]).day(d), c && !this._fullWeekdaysParse[d] && (this._fullWeekdaysParse[d] = new RegExp("^" + this.weekdays(e, "").replace(".", ".?") + "$", "i"), this._shortWeekdaysParse[d] = new RegExp("^" + this.weekdaysShort(e, "").replace(".", ".?") + "$", "i"), this._minWeekdaysParse[d] = new RegExp("^" + this.weekdaysMin(e, "").replace(".", ".?") + "$", "i")), this._weekdaysParse[d] || (f = "^" + this.weekdays(e, "") + "|^" + this.weekdaysShort(e, "") + "|^" + this.weekdaysMin(e, ""), this._weekdaysParse[d] = new RegExp(f.replace(".", ""), "i")), c && "dddd" === b && this._fullWeekdaysParse[d].test(a)) return d; if (c && "ddd" === b && this._shortWeekdaysParse[d].test(a)) return d; if (c && "dd" === b && this._minWeekdaysParse[d].test(a)) return d; if (!c && this._weekdaysParse[d].test(a)) return d } } function ec(a) { if (!this.isValid()) return null != a ? this : NaN; var b = this._isUTC ? this._d.getUTCDay() : this._d.getDay(); return null != a ? (a = _b(a, this.localeData()), this.add(a - b, "d")) : b } function fc(a) { if (!this.isValid()) return null != a ? this : NaN; var b = (this.day() + 7 - this.localeData()._week.dow) % 7; return null == a ? b : this.add(a - b, "d") } function gc(a) { return this.isValid() ? null == a ? this.day() || 7 : this.day(this.day() % 7 ? a : a - 7) : null != a ? this : NaN } function hc(a) { var b = Math.round((this.clone().startOf("day") - this.clone().startOf("year")) / 864e5) + 1; return null == a ? b : this.add(a - b, "d") } function ic() { return this.hours() % 12 || 12 } function jc(a, b) { R(a, 0, 0, function() { return this.localeData().meridiem(this.hours(), this.minutes(), b) }) } function kc(a, b) { return b._meridiemParse } function lc(a) { return "p" === (a + "").toLowerCase().charAt(0) } function mc(a, b, c) { return a > 11 ? c ? "pm" : "PM" : c ? "am" : "AM" } function nc(a, b) { b[Hd] = r(1e3 * ("0." + a)) } function oc() { return this._isUTC ? "UTC" : "" } function pc() { return this._isUTC ? "Coordinated Universal Time" : "" } function qc(a) { return Ja(1e3 * a) } function rc() { return Ja.apply(null, arguments).parseZone() } function sc(a, b, c) { var d = this._calendar[a]; return w(d) ? d.call(b, c) : d } function tc(a) { var b = this._longDateFormat[a], c = this._longDateFormat[a.toUpperCase()]; return b || !c ? b : (this._longDateFormat[a] = c.replace(/MMMM|MM|DD|dddd/g, function(a) { return a.slice(1) }), this._longDateFormat[a]) } function uc() { return this._invalidDate } function vc(a) { return this._ordinal.replace("%d", a) } function wc(a) { return a } function xc(a, b, c, d) { var e = this._relativeTime[c]; return w(e) ? e(a, b, c, d) : e.replace(/%d/i, a) } function yc(a, b) { var c = this._relativeTime[a > 0 ? "future" : "past"]; return w(c) ? c(b) : c.replace(/%s/i, b) } function zc(a, b, c, d) { var e = H(), f = h().set(d, b); return e[c](f, a) } function Ac(a, b, c, d, e) { if ("number" == typeof a && (b = a, a = void 0), a = a || "", null != b) return zc(a, b, c, e); var f, g = []; for (f = 0; d > f; f++)g[f] = zc(a, f, c, e); return g } function Bc(a, b) { return Ac(a, b, "months", 12, "month") } function Cc(a, b) { return Ac(a, b, "monthsShort", 12, "month") } function Dc(a, b) { return Ac(a, b, "weekdays", 7, "day") } function Ec(a, b) { return Ac(a, b, "weekdaysShort", 7, "day") } function Fc(a, b) { return Ac(a, b, "weekdaysMin", 7, "day") } function Gc() { var a = this._data; return this._milliseconds = xe(this._milliseconds), this._days = xe(this._days), this._months = xe(this._months), a.milliseconds = xe(a.milliseconds), a.seconds = xe(a.seconds), a.minutes = xe(a.minutes), a.hours = xe(a.hours), a.months = xe(a.months), a.years = xe(a.years), this } function Hc(a, b, c, d) { var e = cb(b, c); return a._milliseconds += d * e._milliseconds, a._days += d * e._days, a._months += d * e._months, a._bubble() } function Ic(a, b) { return Hc(this, a, b, 1) } function Jc(a, b) { return Hc(this, a, b, -1) } function Kc(a) { return 0 > a ? Math.floor(a) : Math.ceil(a) } function Lc() { var a, b, c, d, e, f = this._milliseconds, g = this._days, h = this._months, i = this._data; return f >= 0 && g >= 0 && h >= 0 || 0 >= f && 0 >= g && 0 >= h || (f += 864e5 * Kc(Nc(h) + g), g = 0, h = 0), i.milliseconds = f % 1e3, a = q(f / 1e3), i.seconds = a % 60, b = q(a / 60), i.minutes = b % 60, c = q(b / 60), i.hours = c % 24, g += q(c / 24), e = q(Mc(g)), h += e, g -= Kc(Nc(e)), d = q(h / 12), h %= 12, i.days = g, i.months = h, i.years = d, this } function Mc(a) { return 4800 * a / 146097 } function Nc(a) { return 146097 * a / 4800 } function Oc(a) { var b, c, d = this._milliseconds; if (a = K(a), "month" === a || "year" === a) return b = this._days + d / 864e5, c = this._months + Mc(b), "month" === a ? c : c / 12; switch (b = this._days + Math.round(Nc(this._months)), a) { case "week": return b / 7 + d / 6048e5; case "day": return b + d / 864e5; case "hour": return 24 * b + d / 36e5; case "minute": return 1440 * b + d / 6e4; case "second": return 86400 * b + d / 1e3; case "millisecond": return Math.floor(864e5 * b) + d; default: throw new Error("Unknown unit " + a) } } function Pc() { return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * r(this._months / 12) } function Qc(a) { return function() { return this.as(a) } } function Rc(a) { return a = K(a), this[a + "s"]() } function Sc(a) { return function() { return this._data[a] } } function Tc() { return q(this.days() / 7) } function Uc(a, b, c, d, e) { return e.relativeTime(b || 1, !!c, a, d) } function Vc(a, b, c) { var d = cb(a).abs(), e = Ne(d.as("s")), f = Ne(d.as("m")), g = Ne(d.as("h")), h = Ne(d.as("d")), i = Ne(d.as("M")), j = Ne(d.as("y")), k = e < Oe.s && ["s", e] || 1 >= f && ["m"] || f < Oe.m && ["mm", f] || 1 >= g && ["h"] || g < Oe.h && ["hh", g] || 1 >= h && ["d"] || h < Oe.d && ["dd", h] || 1 >= i && ["M"] || i < Oe.M && ["MM", i] || 1 >= j && ["y"] || ["yy", j]; return k[2] = b, k[3] = +a > 0, k[4] = c, Uc.apply(null, k) } function Wc(a, b) { return void 0 === Oe[a] ? !1 : void 0 === b ? Oe[a] : (Oe[a] = b, !0) } function Xc(a) { var b = this.localeData(), c = Vc(this, !a, b); return a && (c = b.pastFuture(+this, c)), b.postformat(c) } function Yc() { var a, b, c, d = Pe(this._milliseconds) / 1e3, e = Pe(this._days), f = Pe(this._months); a = q(d / 60), b = q(a / 60), d %= 60, a %= 60, c = q(f / 12), f %= 12; var g = c, h = f, i = e, j = b, k = a, l = d, m = this.asSeconds(); return m ? (0 > m ? "-" : "") + "P" + (g ? g + "Y" : "") + (h ? h + "M" : "") + (i ? i + "D" : "") + (j || k || l ? "T" : "") + (j ? j + "H" : "") + (k ? k + "M" : "") + (l ? l + "S" : "") : "P0D" } var Zc, $c = a.momentProperties = [], _c = !1, ad = {}; a.suppressDeprecationWarnings = !1; var bd, cd = {}, dd = {}, ed = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g, fd = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, gd = {}, hd = {}, id = /\d/, jd = /\d\d/, kd = /\d{3}/, ld = /\d{4}/, md = /[+-]?\d{6}/, nd = /\d\d?/, od = /\d\d\d\d?/, pd = /\d\d\d\d\d\d?/, qd = /\d{1,3}/, rd = /\d{1,4}/, sd = /[+-]?\d{1,6}/, td = /\d+/, ud = /[+-]?\d+/, vd = /Z|[+-]\d\d:?\d\d/gi, wd = /Z|[+-]\d\d(?::?\d\d)?/gi, xd = /[+-]?\d+(\.\d{1,3})?/, yd = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, zd = {}, Ad = {}, Bd = 0, Cd = 1, Dd = 2, Ed = 3, Fd = 4, Gd = 5, Hd = 6, Id = 7, Jd = 8; R("M", ["MM", 2], "Mo", function() { return this.month() + 1 }), R("MMM", 0, 0, function(a) { return this.localeData().monthsShort(this, a) }), R("MMMM", 0, 0, function(a) { return this.localeData().months(this, a) }), J("month", "M"), W("M", nd), W("MM", nd, jd), W("MMM", function(a, b) { return b.monthsShortRegex(a) }), W("MMMM", function(a, b) { return b.monthsRegex(a) }), $(["M", "MM"], function(a, b) { b[Cd] = r(a) - 1 }), $(["MMM", "MMMM"], function(a, b, c, d) { var e = c._locale.monthsParse(a, d, c._strict); null != e ? b[Cd] = e : j(c).invalidMonth = a }); var Kd = /D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/, Ld = "January_February_March_April_May_June_July_August_September_October_November_December".split("_"), Md = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"), Nd = yd, Od = yd, Pd = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/, Qd = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/, Rd = /Z|[+-]\d\d(?::?\d\d)?/, Sd = [["YYYYYY-MM-DD", /[+-]\d{6}-\d\d-\d\d/], ["YYYY-MM-DD", /\d{4}-\d\d-\d\d/], ["GGGG-[W]WW-E", /\d{4}-W\d\d-\d/], ["GGGG-[W]WW", /\d{4}-W\d\d/, !1], ["YYYY-DDD", /\d{4}-\d{3}/], ["YYYY-MM", /\d{4}-\d\d/, !1], ["YYYYYYMMDD", /[+-]\d{10}/], ["YYYYMMDD", /\d{8}/], ["GGGG[W]WWE", /\d{4}W\d{3}/], ["GGGG[W]WW", /\d{4}W\d{2}/, !1], ["YYYYDDD", /\d{7}/]], Td = [["HH:mm:ss.SSSS", /\d\d:\d\d:\d\d\.\d+/], ["HH:mm:ss,SSSS", /\d\d:\d\d:\d\d,\d+/], ["HH:mm:ss", /\d\d:\d\d:\d\d/], ["HH:mm", /\d\d:\d\d/], ["HHmmss.SSSS", /\d\d\d\d\d\d\.\d+/], ["HHmmss,SSSS", /\d\d\d\d\d\d,\d+/], ["HHmmss", /\d\d\d\d\d\d/], ["HHmm", /\d\d\d\d/], ["HH", /\d\d/]], Ud = /^\/?Date\((\-?\d+)/i; a.createFromInputFallback = u("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.", function(a) { a._d = new Date(a._i + (a._useUTC ? " UTC" : "")) }), R("Y", 0, 0, function() { var a = this.year(); return 9999 >= a ? "" + a : "+" + a }), R(0, ["YY", 2], 0, function() { return this.year() % 100 }), R(0, ["YYYY", 4], 0, "year"), R(0, ["YYYYY", 5], 0, "year"), R(0, ["YYYYYY", 6, !0], 0, "year"), J("year", "y"), W("Y", ud), W("YY", nd, jd), W("YYYY", rd, ld), W("YYYYY", sd, md), W("YYYYYY", sd, md), $(["YYYYY", "YYYYYY"], Bd), $("YYYY", function(b, c) {
        c[Bd] = 2 === b.length ? a.parseTwoDigitYear(b) : r(b);
    }), $("YY", function(b, c) { c[Bd] = a.parseTwoDigitYear(b) }), $("Y", function(a, b) { b[Bd] = parseInt(a, 10) }), a.parseTwoDigitYear = function(a) { return r(a) + (r(a) > 68 ? 1900 : 2e3) }; var Vd = M("FullYear", !1); a.ISO_8601 = function() { }; var Wd = u("moment().min is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548", function() { var a = Ja.apply(null, arguments); return this.isValid() && a.isValid() ? this > a ? this : a : l() }), Xd = u("moment().max is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548", function() { var a = Ja.apply(null, arguments); return this.isValid() && a.isValid() ? a > this ? this : a : l() }), Yd = function() { return Date.now ? Date.now() : +new Date }; Pa("Z", ":"), Pa("ZZ", ""), W("Z", wd), W("ZZ", wd), $(["Z", "ZZ"], function(a, b, c) { c._useUTC = !0, c._tzm = Qa(wd, a) }); var Zd = /([\+\-]|\d\d)/gi; a.updateOffset = function() { }; var $d = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?\d*)?$/, _d = /^(-)?P(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)W)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?$/; cb.fn = Na.prototype; var ae = hb(1, "add"), be = hb(-1, "subtract"); a.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ"; var ce = u("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.", function(a) { return void 0 === a ? this.localeData() : this.locale(a) }); R(0, ["gg", 2], 0, function() { return this.weekYear() % 100 }), R(0, ["GG", 2], 0, function() { return this.isoWeekYear() % 100 }), Ob("gggg", "weekYear"), Ob("ggggg", "weekYear"), Ob("GGGG", "isoWeekYear"), Ob("GGGGG", "isoWeekYear"), J("weekYear", "gg"), J("isoWeekYear", "GG"), W("G", ud), W("g", ud), W("GG", nd, jd), W("gg", nd, jd), W("GGGG", rd, ld), W("gggg", rd, ld), W("GGGGG", sd, md), W("ggggg", sd, md), _(["gggg", "ggggg", "GGGG", "GGGGG"], function(a, b, c, d) { b[d.substr(0, 2)] = r(a) }), _(["gg", "GG"], function(b, c, d, e) { c[e] = a.parseTwoDigitYear(b) }), R("Q", 0, "Qo", "quarter"), J("quarter", "Q"), W("Q", id), $("Q", function(a, b) { b[Cd] = 3 * (r(a) - 1) }), R("w", ["ww", 2], "wo", "week"), R("W", ["WW", 2], "Wo", "isoWeek"), J("week", "w"), J("isoWeek", "W"), W("w", nd), W("ww", nd, jd), W("W", nd), W("WW", nd, jd), _(["w", "ww", "W", "WW"], function(a, b, c, d) { b[d.substr(0, 1)] = r(a) }); var de = { dow: 0, doy: 6 }; R("D", ["DD", 2], "Do", "date"), J("date", "D"), W("D", nd), W("DD", nd, jd), W("Do", function(a, b) { return a ? b._ordinalParse : b._ordinalParseLenient }), $(["D", "DD"], Dd), $("Do", function(a, b) { b[Dd] = r(a.match(nd)[0], 10) }); var ee = M("Date", !0); R("d", 0, "do", "day"), R("dd", 0, 0, function(a) { return this.localeData().weekdaysMin(this, a) }), R("ddd", 0, 0, function(a) { return this.localeData().weekdaysShort(this, a) }), R("dddd", 0, 0, function(a) { return this.localeData().weekdays(this, a) }), R("e", 0, 0, "weekday"), R("E", 0, 0, "isoWeekday"), J("day", "d"), J("weekday", "e"), J("isoWeekday", "E"), W("d", nd), W("e", nd), W("E", nd), W("dd", yd), W("ddd", yd), W("dddd", yd), _(["dd", "ddd", "dddd"], function(a, b, c, d) { var e = c._locale.weekdaysParse(a, d, c._strict); null != e ? b.d = e : j(c).invalidWeekday = a }), _(["d", "e", "E"], function(a, b, c, d) { b[d] = r(a) }); var fe = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), ge = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"), he = "Su_Mo_Tu_We_Th_Fr_Sa".split("_"); R("DDD", ["DDDD", 3], "DDDo", "dayOfYear"), J("dayOfYear", "DDD"), W("DDD", qd), W("DDDD", kd), $(["DDD", "DDDD"], function(a, b, c) { c._dayOfYear = r(a) }), R("H", ["HH", 2], 0, "hour"), R("h", ["hh", 2], 0, ic), R("hmm", 0, 0, function() { return "" + ic.apply(this) + Q(this.minutes(), 2) }), R("hmmss", 0, 0, function() { return "" + ic.apply(this) + Q(this.minutes(), 2) + Q(this.seconds(), 2) }), R("Hmm", 0, 0, function() { return "" + this.hours() + Q(this.minutes(), 2) }), R("Hmmss", 0, 0, function() { return "" + this.hours() + Q(this.minutes(), 2) + Q(this.seconds(), 2) }), jc("a", !0), jc("A", !1), J("hour", "h"), W("a", kc), W("A", kc), W("H", nd), W("h", nd), W("HH", nd, jd), W("hh", nd, jd), W("hmm", od), W("hmmss", pd), W("Hmm", od), W("Hmmss", pd), $(["H", "HH"], Ed), $(["a", "A"], function(a, b, c) { c._isPm = c._locale.isPM(a), c._meridiem = a }), $(["h", "hh"], function(a, b, c) { b[Ed] = r(a), j(c).bigHour = !0 }), $("hmm", function(a, b, c) { var d = a.length - 2; b[Ed] = r(a.substr(0, d)), b[Fd] = r(a.substr(d)), j(c).bigHour = !0 }), $("hmmss", function(a, b, c) { var d = a.length - 4, e = a.length - 2; b[Ed] = r(a.substr(0, d)), b[Fd] = r(a.substr(d, 2)), b[Gd] = r(a.substr(e)), j(c).bigHour = !0 }), $("Hmm", function(a, b, c) { var d = a.length - 2; b[Ed] = r(a.substr(0, d)), b[Fd] = r(a.substr(d)) }), $("Hmmss", function(a, b, c) { var d = a.length - 4, e = a.length - 2; b[Ed] = r(a.substr(0, d)), b[Fd] = r(a.substr(d, 2)), b[Gd] = r(a.substr(e)) }); var ie = /[ap]\.?m?\.?/i, je = M("Hours", !0); R("m", ["mm", 2], 0, "minute"), J("minute", "m"), W("m", nd), W("mm", nd, jd), $(["m", "mm"], Fd); var ke = M("Minutes", !1); R("s", ["ss", 2], 0, "second"), J("second", "s"), W("s", nd), W("ss", nd, jd), $(["s", "ss"], Gd); var le = M("Seconds", !1); R("S", 0, 0, function() { return ~~(this.millisecond() / 100) }), R(0, ["SS", 2], 0, function() { return ~~(this.millisecond() / 10) }), R(0, ["SSS", 3], 0, "millisecond"), R(0, ["SSSS", 4], 0, function() { return 10 * this.millisecond() }), R(0, ["SSSSS", 5], 0, function() { return 100 * this.millisecond() }), R(0, ["SSSSSS", 6], 0, function() { return 1e3 * this.millisecond() }), R(0, ["SSSSSSS", 7], 0, function() { return 1e4 * this.millisecond() }), R(0, ["SSSSSSSS", 8], 0, function() { return 1e5 * this.millisecond() }), R(0, ["SSSSSSSSS", 9], 0, function() { return 1e6 * this.millisecond() }), J("millisecond", "ms"), W("S", qd, id), W("SS", qd, jd), W("SSS", qd, kd); var me; for (me = "SSSS"; me.length <= 9; me += "S")W(me, td); for (me = "S"; me.length <= 9; me += "S")$(me, nc); var ne = M("Milliseconds", !1); R("z", 0, 0, "zoneAbbr"), R("zz", 0, 0, "zoneName"); var oe = o.prototype; oe.add = ae, oe.calendar = jb, oe.clone = kb, oe.diff = rb, oe.endOf = Db, oe.format = vb, oe.from = wb, oe.fromNow = xb, oe.to = yb, oe.toNow = zb, oe.get = P, oe.invalidAt = Mb, oe.isAfter = lb, oe.isBefore = mb, oe.isBetween = nb, oe.isSame = ob, oe.isSameOrAfter = pb, oe.isSameOrBefore = qb, oe.isValid = Kb, oe.lang = ce, oe.locale = Ab, oe.localeData = Bb, oe.max = Xd, oe.min = Wd, oe.parsingFlags = Lb, oe.set = P, oe.startOf = Cb, oe.subtract = be, oe.toArray = Hb, oe.toObject = Ib, oe.toDate = Gb, oe.toISOString = ub, oe.toJSON = Jb, oe.toString = tb, oe.unix = Fb, oe.valueOf = Eb, oe.creationData = Nb, oe.year = Vd, oe.isLeapYear = sa, oe.weekYear = Pb, oe.isoWeekYear = Qb, oe.quarter = oe.quarters = Vb, oe.month = ga, oe.daysInMonth = ha, oe.week = oe.weeks = Zb, oe.isoWeek = oe.isoWeeks = $b, oe.weeksInYear = Sb, oe.isoWeeksInYear = Rb, oe.date = ee, oe.day = oe.days = ec, oe.weekday = fc, oe.isoWeekday = gc, oe.dayOfYear = hc, oe.hour = oe.hours = je, oe.minute = oe.minutes = ke, oe.second = oe.seconds = le, oe.millisecond = oe.milliseconds = ne, oe.utcOffset = Ta, oe.utc = Va, oe.local = Wa, oe.parseZone = Xa, oe.hasAlignedHourOffset = Ya, oe.isDST = Za, oe.isDSTShifted = $a, oe.isLocal = _a, oe.isUtcOffset = ab, oe.isUtc = bb, oe.isUTC = bb, oe.zoneAbbr = oc, oe.zoneName = pc, oe.dates = u("dates accessor is deprecated. Use date instead.", ee), oe.months = u("months accessor is deprecated. Use month instead", ga), oe.years = u("years accessor is deprecated. Use year instead", Vd), oe.zone = u("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779", Ua); var pe = oe, qe = { sameDay: "[Today at] LT", nextDay: "[Tomorrow at] LT", nextWeek: "dddd [at] LT", lastDay: "[Yesterday at] LT", lastWeek: "[Last] dddd [at] LT", sameElse: "L" }, re = { LTS: "h:mm:ss A", LT: "h:mm A", L: "MM/DD/YYYY", LL: "MMMM D, YYYY", LLL: "MMMM D, YYYY h:mm A", LLLL: "dddd, MMMM D, YYYY h:mm A" }, se = "Invalid date", te = "%d", ue = /\d{1,2}/, ve = { future: "in %s", past: "%s ago", s: "a few seconds", m: "a minute", mm: "%d minutes", h: "an hour", hh: "%d hours", d: "a day", dd: "%d days", M: "a month", MM: "%d months", y: "a year", yy: "%d years" }, we = A.prototype; we._calendar = qe, we.calendar = sc, we._longDateFormat = re, we.longDateFormat = tc, we._invalidDate = se, we.invalidDate = uc, we._ordinal = te, we.ordinal = vc, we._ordinalParse = ue, we.preparse = wc, we.postformat = wc, we._relativeTime = ve, we.relativeTime = xc, we.pastFuture = yc, we.set = y, we.months = ca, we._months = Ld, we.monthsShort = da, we._monthsShort = Md, we.monthsParse = ea, we._monthsRegex = Od, we.monthsRegex = ja, we._monthsShortRegex = Nd, we.monthsShortRegex = ia, we.week = Wb, we._week = de, we.firstDayOfYear = Yb, we.firstDayOfWeek = Xb, we.weekdays = ac, we._weekdays = fe, we.weekdaysMin = cc, we._weekdaysMin = he, we.weekdaysShort = bc, we._weekdaysShort = ge, we.weekdaysParse = dc, we.isPM = lc, we._meridiemParse = ie, we.meridiem = mc, E("en", { ordinalParse: /\d{1,2}(th|st|nd|rd)/, ordinal: function(a) { var b = a % 10, c = 1 === r(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th"; return a + c } }), a.lang = u("moment.lang is deprecated. Use moment.locale instead.", E), a.langData = u("moment.langData is deprecated. Use moment.localeData instead.", H); var xe = Math.abs, ye = Qc("ms"), ze = Qc("s"), Ae = Qc("m"), Be = Qc("h"), Ce = Qc("d"), De = Qc("w"), Ee = Qc("M"), Fe = Qc("y"), Ge = Sc("milliseconds"), He = Sc("seconds"), Ie = Sc("minutes"), Je = Sc("hours"), Ke = Sc("days"), Le = Sc("months"), Me = Sc("years"), Ne = Math.round, Oe = { s: 45, m: 45, h: 22, d: 26, M: 11 }, Pe = Math.abs, Qe = Na.prototype; Qe.abs = Gc, Qe.add = Ic, Qe.subtract = Jc, Qe.as = Oc, Qe.asMilliseconds = ye, Qe.asSeconds = ze, Qe.asMinutes = Ae, Qe.asHours = Be, Qe.asDays = Ce, Qe.asWeeks = De, Qe.asMonths = Ee, Qe.asYears = Fe, Qe.valueOf = Pc, Qe._bubble = Lc, Qe.get = Rc, Qe.milliseconds = Ge, Qe.seconds = He, Qe.minutes = Ie, Qe.hours = Je, Qe.days = Ke, Qe.weeks = Tc, Qe.months = Le, Qe.years = Me, Qe.humanize = Xc, Qe.toISOString = Yc, Qe.toString = Yc, Qe.toJSON = Yc, Qe.locale = Ab, Qe.localeData = Bb, Qe.toIsoString = u("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", Yc), Qe.lang = ce, R("X", 0, 0, "unix"), R("x", 0, 0, "valueOf"), W("x", ud), W("X", xd), $("X", function(a, b, c) { c._d = new Date(1e3 * parseFloat(a, 10)) }), $("x", function(a, b, c) { c._d = new Date(r(a)) }), a.version = "2.12.0", b(Ja), a.fn = pe, a.min = La, a.max = Ma, a.now = Yd, a.utc = h, a.unix = qc, a.months = Bc, a.isDate = d, a.locale = E, a.invalid = l, a.duration = cb, a.isMoment = p, a.weekdays = Dc, a.parseZone = rc, a.localeData = H, a.isDuration = Oa, a.monthsShort = Cc, a.weekdaysMin = Fc, a.defineLocale = F, a.updateLocale = G, a.locales = I, a.weekdaysShort = Ec, a.normalizeUnits = K, a.relativeTimeThreshold = Wc, a.prototype = pe; var Re = a; return Re
});

/*!
    moment.phpDateFormat.js
*/
; (function(m) {
    /*
     * PHP => moment.js
     * Will take a php date format and convert it into a JS format for moment
     * http://www.php.net/manual/en/function.date.php
     * http://momentjs.com/docs/#/displaying/format/
     */
    var formatMap = {
        d: 'DD',
        D: 'ddd',
        j: 'D',
        l: 'dddd',
        N: 'E',
        S: function() {
            return '[' + this.format('Do').replace(/\d*/g, '') + ']';
        },
        w: 'd',
        z: function() {
            return this.format('DDD') - 1;
        },
        W: 'W',
        F: 'MMMM',
        m: 'MM',
        M: 'MMM',
        n: 'M',
        t: function() {
            return this.daysInMonth();
        },
        L: function() {
            return this.isLeapYear() ? 1 : 0;
        },
        o: 'GGGG',
        Y: 'YYYY',
        y: 'YY',
        a: 'a',
        A: 'A',
        B: function() {
            var thisUTC = this.clone().utc(),
                // Shamelessly stolen from http://javascript.about.com/library/blswatch.htm
                swatch = ((thisUTC.hours() + 1) % 24) + (thisUTC.minutes() / 60) + (thisUTC.seconds() / 3600);
            return Math.floor(swatch * 1000 / 24);
        },
        g: 'h',
        G: 'H',
        h: 'hh',
        H: 'HH',
        i: 'mm',
        s: 'ss',
        u: '[u]', // not sure if moment has this
        e: '[e]', // moment does not have this
        I: function() {
            return this.isDST() ? 1 : 0;
        },
        O: 'ZZ',
        P: 'Z',
        T: '[T]', // deprecated in moment
        Z: function() {
            return parseInt(this.format('ZZ'), 10) * 36;
        },
        c: 'YYYY-MM-DD[T]HH:mm:ssZ',
        r: 'ddd, DD MMM YYYY HH:mm:ss ZZ',
        U: 'X'
    },
        formatEx = /[dDjlNSwzWFmMntLoYyaABgGhHisueIOPTZcrU]/g;

    moment.fn.formatPHP = function(format) {
        var that = this;

        //removed this.format(); wrap
        return format.replace(formatEx, function(phpStr) {
            return typeof formatMap[phpStr] === 'function' ? formatMap[phpStr].call(that) : formatMap[phpStr];
        });

    };
}(moment));


/*!
    moment.parseFormat.js
*/
; (function(root, factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        define(['moment'], function(moment) {
            moment.parseFormat = factory(moment);
            return moment.parseFormat;
        });
    } else if (typeof exports === 'object') {
        module.exports = factory(require('moment'));
    } else {
        root.moment.parseFormat = factory(root.moment);
    }
})(this, function(/*moment*/) { // jshint ignore:line
    var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var abbreviatedDayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    var shortestDayNames = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
    var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var abbreviatedMonthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var amDesignator = 'AM';
    var pmDesignator = 'PM';

    var regexDayNames = new RegExp(dayNames.join('|'), 'i');
    var regexAbbreviatedDayNames = new RegExp(abbreviatedDayNames.join('|'), 'i');
    var regexShortestDayNames = new RegExp('\\b(' + shortestDayNames.join('|') + ')\\b', 'i');
    var regexMonthNames = new RegExp(monthNames.join('|'), 'i');
    var regexAbbreviatedMonthNames = new RegExp(abbreviatedMonthNames.join('|'), 'i');

    var regexFirstSecondThirdFourth = /(\d+)(st|nd|rd|th)\b/i;
    var regexEndian = /(\d{1,4})([\/\.\-])(\d{1,2})[\/\.\-](\d{1,4})/;

    var regexTimezone = /\+\d\d:\d\d$/
    var amOrPm = '(' + [amDesignator, pmDesignator].join('|') + ')';
    var regexHoursWithLeadingZeroDigitMinutesSecondsAmPm = new RegExp('0\\d\\:\\d{1,2}\\:\\d{1,2}(\\s*)' + amOrPm, 'i');
    var regexHoursWithLeadingZeroDigitMinutesAmPm = new RegExp('0\\d\\:\\d{1,2}(\\s*)' + amOrPm, 'i');
    var regexHoursWithLeadingZeroDigitAmPm = new RegExp('0\\d(\\s*)' + amOrPm, 'i');
    var regexHoursMinutesSecondsAmPm = new RegExp('\\d{1,2}\\:\\d{1,2}\\:\\d{1,2}(\\s*)' + amOrPm, 'i');
    var regexHoursMinutesAmPm = new RegExp('\\d{1,2}\\:\\d{1,2}(\\s*)' + amOrPm, 'i');
    var regexHoursAmPm = new RegExp('\\d{1,2}(\\s*)' + amOrPm, 'i');

    var regexHoursWithLeadingZeroMinutesSeconds = /0\d:\d{2}:\d{2}/;
    var regexHoursWithLeadingZeroMinutes = /0\d:\d{2}/;
    var regexHoursMinutesSeconds = /\d{1,2}:\d{2}:\d{2}/;
    var regexHoursMinutes = /\d{1,2}:\d{2}/;
    var regexYearLong = /\d{4}/;
    var regexDayLeadingZero = /0\d/;
    var regexDay = /\d{1,2}/;
    var regexYearShort = /\d{2}/;

    var regexFillingWords = /\b(at)\b/i;

    // option defaults
    var defaultOrder = {
        '/': 'MDY',
        '.': 'DMY',
        '-': 'YMD'
    };

    function parseDateFormat(dateString, options) {
        var format = dateString;

        // default options
        options = options || {};
        options.preferredOrder = options.preferredOrder || defaultOrder;

        // escape filling words
        format = format.replace(regexFillingWords, '[$1]');

        //  DAYS

        // Monday ☛ dddd
        format = format.replace(regexDayNames, 'dddd');
        // Mon ☛ ddd
        format = format.replace(regexAbbreviatedDayNames, 'ddd');
        // Mo ☛ dd
        format = format.replace(regexShortestDayNames, 'dd');

        // 1st, 2nd, 23rd ☛ do
        format = format.replace(regexFirstSecondThirdFourth, 'Do');

        // MONTHS

        // January ☛ MMMM
        format = format.replace(regexMonthNames, 'MMMM');
        // Jan ☛ MMM
        format = format.replace(regexAbbreviatedMonthNames, 'MMM');

        // replace endians, like 8/20/2010, 20.8.2010 or 2010-8-20
        format = format.replace(regexEndian, replaceEndian.bind(null, options));

        // TIME

        // timezone +02:00 ☛ Z
        format = format.replace(regexTimezone, 'Z');

        // 05:30:20pm ☛ hh:mm:ssa
        format = format.replace(regexHoursWithLeadingZeroDigitMinutesSecondsAmPm, 'hh:mm:ss$1a');
        // 10:30:20pm ☛ h:mm:ssa
        format = format.replace(regexHoursMinutesSecondsAmPm, 'h:mm:ss$1a');
        // 05:30pm ☛ hh:mma
        format = format.replace(regexHoursWithLeadingZeroDigitMinutesAmPm, 'hh:mm$1a');
        // 10:30pm ☛ h:mma
        format = format.replace(regexHoursMinutesAmPm, 'h:mm$1a');
        // 05pm ☛ hha
        format = format.replace(regexHoursWithLeadingZeroDigitAmPm, 'hh$1a');
        // 10pm ☛ ha
        format = format.replace(regexHoursAmPm, 'h$1a');
        // 05:30:20 ☛ HH:mm:ss
        format = format.replace(regexHoursWithLeadingZeroMinutesSeconds, 'HH:mm:ss');
        // 10:30:20 ☛ H:mm:ss
        format = format.replace(regexHoursMinutesSeconds, 'H:mm:ss');
        // 05:30 ☛ H:mm
        format = format.replace(regexHoursWithLeadingZeroMinutes, 'HH:mm');
        // 10:30 ☛ HH:mm
        format = format.replace(regexHoursMinutes, 'H:mm');

        // do we still have numbers left?

        // Lets check for 4 digits first, these are years for sure
        format = format.replace(regexYearLong, 'YYYY');

        // now, the next number, if existing, must be a day
        format = format.replace(regexDayLeadingZero, 'DD');
        format = format.replace(regexDay, 'D');

        // last but not least, there could still be a year left
        format = format.replace(regexYearShort, 'YY');

        return format;
    }

    // if we can't find an endian based on the separator, but
    // there still is a short date with day, month & year,
    // we try to make a smart decision to identify the order
    function replaceEndian(options, matchedPart, first, separator, second, third) {
        var parts;
        var hasSingleDigit = Math.min(first.length, second.length, third.length) === 1;
        var hasQuadDigit = Math.max(first.length, second.length, third.length) === 4;
        var index = -1;
        var preferredOrder = typeof options.preferredOrder === 'string' ? options.preferredOrder : options.preferredOrder[separator];

        first = parseInt(first, 10);
        second = parseInt(second, 10);
        third = parseInt(third, 10);
        parts = [first, second, third];
        preferredOrder = preferredOrder.toUpperCase();

        // If first is a year, order will always be Year-Month-Day
        if (first > 31) {
            parts[0] = hasQuadDigit ? 'YYYY' : 'YY';
            parts[1] = hasSingleDigit ? 'M' : 'MM';
            parts[2] = hasSingleDigit ? 'D' : 'DD';
            return parts.join(separator);
        }

        // Second will never be the year. And if it is a day,
        // the order will always be Month-Day-Year
        if (second > 12) {
            parts[0] = hasSingleDigit ? 'M' : 'MM';
            parts[1] = hasSingleDigit ? 'D' : 'DD';
            parts[2] = hasQuadDigit ? 'YYYY' : 'YY';
            return parts.join(separator);
        }

        // if third is a year ...
        if (third > 31) {
            parts[2] = hasQuadDigit ? 'YYYY' : 'YY';

            // ... try to find day in first and second.
            // If found, the remaining part is the month.
            switch (true) {
                case first > 12:
                    parts[0] = hasSingleDigit ? 'D' : 'DD';
                    parts[1] = hasSingleDigit ? 'M' : 'MM';
                    return parts.join(separator);
                case second > 12:
                    parts[0] = hasSingleDigit ? 'M' : 'MM';
                    parts[1] = hasSingleDigit ? 'D' : 'DD';
                    return parts.join(separator);
                default: // sorry
                    if (preferredOrder[0] === 'M') {
                        parts[0] = hasSingleDigit ? 'M' : 'MM';
                        parts[1] = hasSingleDigit ? 'D' : 'DD';
                        return parts.join(separator);
                    }
                    parts[0] = hasSingleDigit ? 'D' : 'DD';
                    parts[1] = hasSingleDigit ? 'M' : 'MM';
                    return parts.join(separator);
            }
        }

        // if we had no luck until here, we use the preferred order
        parts[preferredOrder.indexOf('D')] = hasSingleDigit ? 'D' : 'DD';
        parts[preferredOrder.indexOf('M')] = hasSingleDigit ? 'M' : 'MM';
        parts[preferredOrder.indexOf('Y')] = hasQuadDigit ? 'YYYY' : 'YY';

        return parts.join(separator);
    }

    return parseDateFormat;
});

/*!
    moment.formatDetails.js
*/
; (function(root, factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        define(['moment'], function(moment) {
            moment.formatDetails = factory(moment);
            return moment.formatDetails;
        });
    } else if (typeof exports === 'object') {
        module.exports = factory(require('moment'));
    } else {
        root.moment.formatDetails = factory(root.moment);
    }
})(this, function(/*moment*/) { // jshint ignore:line

    function FormatDetails(format, options) {

        format = '- ' + format;
        var details = {};

        if (format.indexOf('M') >= 0) details.month = 1;
        if (format.indexOf('D') >= 0) details.day = 1;
        if (format.indexOf('Y') >= 0) details.year = 1;
        if (format.indexOf('H') >= 0) details.hour = 1;
        if (format.indexOf('h') >= 0) details.hour = 1;
        if (format.indexOf('m') >= 0) details.minute = 1;
        if (format.indexOf('s') >= 0) details.second = 1;

        return details;

    }

    return FormatDetails;

});

/*! rangeslider.js - v2.3.2 | (c) 2018 @andreruffert | MIT license | https://github.com/andreruffert/rangeslider.js */
!function(a) { "use strict"; "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a(require("jquery")) : a(jQuery) }(function(a) { "use strict"; function b() { var a = document.createElement("input"); return a.setAttribute("type", "range"), "text" !== a.type } function c(a, b) { var c = Array.prototype.slice.call(arguments, 2); return setTimeout(function() { return a.apply(null, c) }, b) } function d(a, b) { return b = b || 100, function() { if (!a.debouncing) { var c = Array.prototype.slice.apply(arguments); a.lastReturnVal = a.apply(window, c), a.debouncing = !0 } return clearTimeout(a.debounceTimeout), a.debounceTimeout = setTimeout(function() { a.debouncing = !1 }, b), a.lastReturnVal } } function e(a) { return a && (0 === a.offsetWidth || 0 === a.offsetHeight || !1 === a.open) } function f(a) { for (var b = [], c = a.parentNode; e(c);)b.push(c), c = c.parentNode; return b } function g(a, b) { function c(a) { void 0 !== a.open && (a.open = !a.open) } var d = f(a), e = d.length, g = [], h = a[b]; if (e) { for (var i = 0; i < e; i++)g[i] = d[i].style.cssText, d[i].style.setProperty ? d[i].style.setProperty("display", "block", "important") : d[i].style.cssText += ";display: block !important", d[i].style.height = "0", d[i].style.overflow = "hidden", d[i].style.visibility = "hidden", c(d[i]); h = a[b]; for (var j = 0; j < e; j++)d[j].style.cssText = g[j], c(d[j]) } return h } function h(a, b) { var c = parseFloat(a); return Number.isNaN(c) ? b : c } function i(a) { return a.charAt(0).toUpperCase() + a.substr(1) } function j(b, e) { if (this.$window = a(window), this.$document = a(document), this.$element = a(b), this.options = a.extend({}, n, e), this.polyfill = this.options.polyfill, this.orientation = this.$element[0].getAttribute("data-orientation") || this.options.orientation, this.onInit = this.options.onInit, this.onSlide = this.options.onSlide, this.onSlideEnd = this.options.onSlideEnd, this.DIMENSION = o.orientation[this.orientation].dimension, this.DIRECTION = o.orientation[this.orientation].direction, this.DIRECTION_STYLE = o.orientation[this.orientation].directionStyle, this.COORDINATE = o.orientation[this.orientation].coordinate, this.polyfill && m) return !1; this.identifier = "js-" + k + "-" + l++, this.startEvent = this.options.startEvent.join("." + this.identifier + " ") + "." + this.identifier, this.moveEvent = this.options.moveEvent.join("." + this.identifier + " ") + "." + this.identifier, this.endEvent = this.options.endEvent.join("." + this.identifier + " ") + "." + this.identifier, this.toFixed = (this.step + "").replace(".", "").length - 1, this.$fill = a('<div class="' + this.options.fillClass + '" />'), this.$handle = a('<div class="' + this.options.handleClass + '" />'), this.$range = a('<div class="' + this.options.rangeClass + " " + this.options[this.orientation + "Class"] + '" id="' + this.identifier + '" />').insertAfter(this.$element).prepend(this.$fill, this.$handle), this.$element.css({ position: "absolute", width: "1px", height: "1px", overflow: "hidden", opacity: "0" }), this.handleDown = a.proxy(this.handleDown, this), this.handleMove = a.proxy(this.handleMove, this), this.handleEnd = a.proxy(this.handleEnd, this), this.init(); var f = this; this.$window.on("resize." + this.identifier, d(function() { c(function() { f.update(!1, !1) }, 300) }, 20)), this.$document.on(this.startEvent, "#" + this.identifier + ":not(." + this.options.disabledClass + ")", this.handleDown), this.$element.on("change." + this.identifier, function(a, b) { if (!b || b.origin !== f.identifier) { var c = a.target.value, d = f.getPositionFromValue(c); f.setPosition(d) } }) } Number.isNaN = Number.isNaN || function(a) { return "number" == typeof a && a !== a }; var k = "rangeslider", l = 0, m = b(), n = { polyfill: !0, orientation: "horizontal", rangeClass: "rangeslider", disabledClass: "rangeslider--disabled", activeClass: "rangeslider--active", horizontalClass: "rangeslider--horizontal", verticalClass: "rangeslider--vertical", fillClass: "rangeslider__fill", handleClass: "rangeslider__handle", startEvent: ["mousedown", "touchstart", "pointerdown"], moveEvent: ["mousemove", "touchmove", "pointermove"], endEvent: ["mouseup", "touchend", "pointerup"] }, o = { orientation: { horizontal: { dimension: "width", direction: "left", directionStyle: "left", coordinate: "x" }, vertical: { dimension: "height", direction: "top", directionStyle: "bottom", coordinate: "y" } } }; return j.prototype.init = function() { this.update(!0, !1), this.onInit && "function" == typeof this.onInit && this.onInit() }, j.prototype.update = function(a, b) { a = a || !1, a && (this.min = h(this.$element[0].getAttribute("min"), 0), this.max = h(this.$element[0].getAttribute("max"), 100), this.value = h(this.$element[0].value, Math.round(this.min + (this.max - this.min) / 2)), this.step = h(this.$element[0].getAttribute("step"), 1)), this.handleDimension = g(this.$handle[0], "offset" + i(this.DIMENSION)), this.rangeDimension = g(this.$range[0], "offset" + i(this.DIMENSION)), this.maxHandlePos = this.rangeDimension - this.handleDimension, this.grabPos = this.handleDimension / 2, this.position = this.getPositionFromValue(this.value), this.$element[0].disabled ? this.$range.addClass(this.options.disabledClass) : this.$range.removeClass(this.options.disabledClass), this.setPosition(this.position, b) }, j.prototype.handleDown = function(a) { if (a.preventDefault(), !(a.button && 0 !== a.button || (this.$document.on(this.moveEvent, this.handleMove), this.$document.on(this.endEvent, this.handleEnd), this.$range.addClass(this.options.activeClass), (" " + a.target.className + " ").replace(/[\n\t]/g, " ").indexOf(this.options.handleClass) > -1))) { var b = this.getRelativePosition(a), c = this.$range[0].getBoundingClientRect()[this.DIRECTION], d = this.getPositionFromNode(this.$handle[0]) - c, e = "vertical" === this.orientation ? this.maxHandlePos - (b - this.grabPos) : b - this.grabPos; this.setPosition(e), b >= d && b < d + this.handleDimension && (this.grabPos = b - d) } }, j.prototype.handleMove = function(a) { a.preventDefault(); var b = this.getRelativePosition(a), c = "vertical" === this.orientation ? this.maxHandlePos - (b - this.grabPos) : b - this.grabPos; this.setPosition(c) }, j.prototype.handleEnd = function(a) { a.preventDefault(), this.$document.off(this.moveEvent, this.handleMove), this.$document.off(this.endEvent, this.handleEnd), this.$range.removeClass(this.options.activeClass), this.$element.trigger("change", { origin: this.identifier }), this.onSlideEnd && "function" == typeof this.onSlideEnd && this.onSlideEnd(this.position, this.value) }, j.prototype.cap = function(a, b, c) { return a < b ? b : a > c ? c : a }, j.prototype.setPosition = function(a, b) { var c, d; void 0 === b && (b = !0), c = this.getValueFromPosition(this.cap(a, 0, this.maxHandlePos)), d = this.getPositionFromValue(c), this.$fill[0].style[this.DIMENSION] = d + this.grabPos + "px", this.$handle[0].style[this.DIRECTION_STYLE] = d + "px", this.setValue(c), this.position = d, this.value = c, b && this.onSlide && "function" == typeof this.onSlide && this.onSlide(d, c) }, j.prototype.getPositionFromNode = function(a) { for (var b = 0; null !== a;)b += a.offsetLeft, a = a.offsetParent; return b }, j.prototype.getRelativePosition = function(a) { var b = i(this.COORDINATE), c = this.$range[0].getBoundingClientRect()[this.DIRECTION], d = 0; return void 0 !== a.originalEvent["client" + b] ? d = a.originalEvent["client" + b] : a.originalEvent.touches && a.originalEvent.touches[0] && void 0 !== a.originalEvent.touches[0]["client" + b] ? d = a.originalEvent.touches[0]["client" + b] : a.currentPoint && void 0 !== a.currentPoint[this.COORDINATE] && (d = a.currentPoint[this.COORDINATE]), d - c }, j.prototype.getPositionFromValue = function(a) { var b; return b = (a - this.min) / (this.max - this.min), Number.isNaN(b) ? 0 : b * this.maxHandlePos }, j.prototype.getValueFromPosition = function(a) { var b, c; return b = a / (this.maxHandlePos || 1), c = this.step * Math.round(b * (this.max - this.min) / this.step) + this.min, Number(c.toFixed(this.toFixed)) }, j.prototype.setValue = function(a) { a === this.value && "" !== this.$element[0].value || this.$element.val(a).trigger("input", { origin: this.identifier }) }, j.prototype.destroy = function() { this.$document.off("." + this.identifier), this.$window.off("." + this.identifier), this.$element.off("." + this.identifier).removeAttr("style").removeData("plugin_" + k), this.$range && this.$range.length && this.$range[0].parentNode.removeChild(this.$range[0]) }, a.fn[k] = function(b) { var c = Array.prototype.slice.call(arguments, 1); return this.each(function() { var d = a(this), e = d.data("plugin_" + k); e || d.data("plugin_" + k, e = new j(this, b)), "string" == typeof b && e[b].apply(e, c) }) }, "rangeslider.js is available in jQuery context e.g $(selector).rangeslider(options);" });

/*!
    SerializeJSON jQuery plugin.
    https://github.com/marioizquierdo/jquery.serializeJSON
    version 2.4.1 (Oct, 2014)
*/
!(function(a) { a.fn.serializeJSON = function(k) { var g, e, j, h, i, c, d, b; d = a.serializeJSON; b = d.optsWithDefaults(k); d.validateOptions(b); e = this.serializeArray(); d.readCheckboxUncheckedValues(e, this, b); g = {}; a.each(e, function(l, f) { j = d.splitInputNameIntoKeysArray(f.name); h = j.pop(); if (h !== "skip") { i = d.parseValue(f.value, h, b); if (b.parseWithFunction && h === "_") { i = b.parseWithFunction(i, f.name) } d.deepSet(g, j, i, b) } }); return g }; a.serializeJSON = { defaultOptions: { parseNumbers: false, parseBooleans: false, parseNulls: false, parseAll: false, parseWithFunction: null, checkboxUncheckedValue: undefined, useIntKeysAsArrayIndex: false }, optsWithDefaults: function(c) { var d, b; if (c == null) { c = {} } d = a.serializeJSON; b = d.optWithDefaults("parseAll", c); return { parseNumbers: b || d.optWithDefaults("parseNumbers", c), parseBooleans: b || d.optWithDefaults("parseBooleans", c), parseNulls: b || d.optWithDefaults("parseNulls", c), parseWithFunction: d.optWithDefaults("parseWithFunction", c), checkboxUncheckedValue: d.optWithDefaults("checkboxUncheckedValue", c), useIntKeysAsArrayIndex: d.optWithDefaults("useIntKeysAsArrayIndex", c) } }, optWithDefaults: function(c, b) { return (b[c] !== false) && (b[c] !== "") && (b[c] || a.serializeJSON.defaultOptions[c]) }, validateOptions: function(d) { var b, c; c = ["parseNumbers", "parseBooleans", "parseNulls", "parseAll", "parseWithFunction", "checkboxUncheckedValue", "useIntKeysAsArrayIndex"]; for (b in d) { if (c.indexOf(b) === -1) { throw new Error("serializeJSON ERROR: invalid option '" + b + "'. Please use one of " + c.join(",")) } } }, parseValue: function(g, b, c) { var e, d; d = a.serializeJSON; if (b == "string") { return g } if (b == "number" || (c.parseNumbers && d.isNumeric(g))) { return Number(g) } if (b == "boolean" || (c.parseBooleans && (g === "true" || g === "false"))) { return (["false", "null", "undefined", "", "0"].indexOf(g) === -1) } if (b == "null" || (c.parseNulls && g == "null")) { return ["false", "null", "undefined", "", "0"].indexOf(g) !== -1 ? null : g } if (b == "array" || b == "object") { return JSON.parse(g) } if (b == "auto") { return d.parseValue(g, null, { parseNumbers: true, parseBooleans: true, parseNulls: true }) } return g }, isObject: function(b) { return b === Object(b) }, isUndefined: function(b) { return b === void 0 }, isValidArrayIndex: function(b) { return /^[0-9]+$/.test(String(b)) }, isNumeric: function(b) { return b - parseFloat(b) >= 0 }, splitInputNameIntoKeysArray: function(c) { var e, b, d, h, g; g = a.serializeJSON; h = g.extractTypeFromInputName(c), b = h[0], d = h[1]; e = b.split("["); e = a.map(e, function(f) { return f.replace(/]/g, "") }); if (e[0] === "") { e.shift() } e.push(d); return e }, extractTypeFromInputName: function(c) { var b, d; d = a.serializeJSON; if (b = c.match(/(.*):([^:]+)$/)) { var e = ["string", "number", "boolean", "null", "array", "object", "skip", "auto"]; if (e.indexOf(b[2]) !== -1) { return [b[1], b[2]] } else { throw new Error("serializeJSON ERROR: Invalid type " + b[2] + " found in input name '" + c + "', please use one of " + e.join(", ")) } } else { return [c, "_"] } }, deepSet: function(c, l, j, b) { var k, h, g, i, d, e; if (b == null) { b = {} } e = a.serializeJSON; if (e.isUndefined(c)) { throw new Error("ArgumentError: param 'o' expected to be an object or array, found undefined") } if (!l || l.length === 0) { throw new Error("ArgumentError: param 'keys' expected to be an array with least one element") } k = l[0]; if (l.length === 1) { if (k === "") { c.push(j) } else { c[k] = j } } else { h = l[1]; if (k === "") { i = c.length - 1; d = c[i]; if (e.isObject(d) && (e.isUndefined(d[h]) || l.length > 2)) { k = i } else { k = i + 1 } } if (e.isUndefined(c[k])) { if (h === "") { c[k] = [] } else { if (b.useIntKeysAsArrayIndex && e.isValidArrayIndex(h)) { c[k] = [] } else { c[k] = {} } } } g = l.slice(1); e.deepSet(c[k], g, j, b) } }, readCheckboxUncheckedValues: function(e, d, i) { var b, h, g, c, j; if (i == null) { i = {} } j = a.serializeJSON; b = "input[type=checkbox][name]:not(:checked)"; h = d.find(b).add(d.filter(b)); h.each(function(f, k) { g = a(k); c = g.attr("data-unchecked-value"); if (c) { e.push({ name: k.name, value: c }) } else { if (!j.isUndefined(i.checkboxUncheckedValue)) { e.push({ name: k.name, value: i.checkboxUncheckedValue }) } } }) } } }(window.jQuery || window.Zepto || window.$));

/*!
    Hogan.js v3.0.1
    https://github.com/twitter/hogan.js
*/
var Hogan = {}; !function(t) { function n(t, n, i) { var r; return n && "object" == typeof n && (null != n[t] ? r = n[t] : i && n.get && "function" == typeof n.get && (r = n.get(t))), r } function i(t, n, i, r, e, s) { function a() { } function o() { } a.prototype = t, o.prototype = t.subs; var u, c = new a; c.subs = new o, c.subsText = {}, c.buf = "", r = r || {}, c.stackSubs = r, c.subsText = s; for (u in n) r[u] || (r[u] = n[u]); for (u in r) c.subs[u] = r[u]; e = e || {}, c.stackPartials = e; for (u in i) e[u] || (e[u] = i[u]); for (u in e) c.partials[u] = e[u]; return c } function r(t) { return (null === t || void 0 === t ? "" : t) + "" } function e(t) { return t = r(t), l.test(t) ? t.replace(s, "&amp;").replace(a, "&lt;").replace(o, "&gt;").replace(u, "&#39;").replace(c, "&quot;") : t } t.Template = function(t, n, i, r) { t = t || {}, this.r = t.code || this.r, this.c = i, this.options = r || {}, this.text = n || "", this.partials = t.partials || {}, this.subs = t.subs || {}, this.buf = "" }, t.Template.prototype = { r: function() { return "" }, v: e, t: r, render: function(t, n, i) { return this.ri([t], n || {}, i) }, ri: function(t, n, i) { return this.r(t, n, i) }, ep: function(t, n) { var r = this.partials[t], e = n[r.name]; if (r.instance && r.base == e) return r.instance; if ("string" == typeof e) { if (!this.c) throw Error("No compiler available."); e = this.c.compile(e, this.options) } if (!e) return null; if (this.partials[t].base = e, r.subs) { n.stackText || (n.stackText = {}); for (key in r.subs) n.stackText[key] || (n.stackText[key] = void 0 !== this.activeSub && n.stackText[this.activeSub] ? n.stackText[this.activeSub] : this.text); e = i(e, r.subs, r.partials, this.stackSubs, this.stackPartials, n.stackText) } return this.partials[t].instance = e, e }, rp: function(t, n, i, r) { var e = this.ep(t, i); return e ? e.ri(n, i, r) : "" }, rs: function(t, n, i) { var r = t[t.length - 1]; if (!f(r)) return void i(t, n, this); for (var e = 0; e < r.length; e++)t.push(r[e]), i(t, n, this), t.pop() }, s: function(t, n, i, r, e, s, a) { var o; return f(t) && 0 === t.length ? !1 : ("function" == typeof t && (t = this.ms(t, n, i, r, e, s, a)), o = !!t, !r && o && n && n.push("object" == typeof t ? t : n[n.length - 1]), o) }, d: function(t, i, r, e) { var s, a = t.split("."), o = this.f(a[0], i, r, e), u = this.options.modelGet, c = null; if ("." === t && f(i[i.length - 2])) o = i[i.length - 1]; else for (var l = 1; l < a.length; l++)s = n(a[l], o, u), null != s ? (c = o, o = s) : o = ""; return e && !o ? !1 : (e || "function" != typeof o || (i.push(c), o = this.mv(o, i, r), i.pop()), o) }, f: function(t, i, r, e) { for (var s = !1, a = null, o = !1, u = this.options.modelGet, c = i.length - 1; c >= 0; c--)if (a = i[c], s = n(t, a, u), null != s) { o = !0; break } return o ? (e || "function" != typeof s || (s = this.mv(s, i, r)), s) : e ? !1 : "" }, ls: function(t, n, i, e, s) { var a = this.options.delimiters; return this.options.delimiters = s, this.b(this.ct(r(t.call(n, e)), n, i)), this.options.delimiters = a, !1 }, ct: function(t, n, i) { if (this.options.disableLambda) throw Error("Lambda features disabled."); return this.c.compile(t, this.options).render(n, i) }, b: function(t) { this.buf += t }, fl: function() { var t = this.buf; return this.buf = "", t }, ms: function(t, n, i, r, e, s, a) { var o, u = n[n.length - 1], c = t.call(u); return "function" == typeof c ? r ? !0 : (o = this.activeSub && this.subsText && this.subsText[this.activeSub] ? this.subsText[this.activeSub] : this.text, this.ls(c, u, i, o.substring(e, s), a)) : c }, mv: function(t, n, i) { var e = n[n.length - 1], s = t.call(e); return "function" == typeof s ? this.ct(r(s.call(e)), e, i) : s }, sub: function(t, n, i, r) { var e = this.subs[t]; e && (this.activeSub = t, e(n, i, this, r), this.activeSub = !1) } }; var s = /&/g, a = /</g, o = />/g, u = /\'/g, c = /\"/g, l = /[&<>\"\']/, f = Array.isArray || function(t) { return "[object Array]" === Object.prototype.toString.call(t) } }("undefined" != typeof exports ? exports : Hogan), function(t) { function n(t) { "}" === t.n.substr(t.n.length - 1) && (t.n = t.n.substring(0, t.n.length - 1)) } function i(t) { return t.trim ? t.trim() : t.replace(/^\s*|\s*$/g, "") } function r(t, n, i) { if (n.charAt(i) != t.charAt(0)) return !1; for (var r = 1, e = t.length; e > r; r++)if (n.charAt(i + r) != t.charAt(r)) return !1; return !0 } function e(n, i, r, o) { var u = [], c = null, l = null, f = null; for (l = r[r.length - 1]; n.length > 0;) { if (f = n.shift(), l && "<" == l.tag && !(f.tag in x)) throw Error("Illegal content in < super tag."); if (t.tags[f.tag] <= t.tags.$ || s(f, o)) r.push(f), f.nodes = e(n, f.tag, r, o); else { if ("/" == f.tag) { if (0 === r.length) throw Error("Closing tag without opener: /" + f.n); if (c = r.pop(), f.n != c.n && !a(f.n, c.n, o)) throw Error("Nesting error: " + c.n + " vs. " + f.n); return c.end = f.i, u } "\n" == f.tag && (f.last = 0 == n.length || "\n" == n[0].tag) } u.push(f) } if (r.length > 0) throw Error("missing closing tag: " + r.pop().n); return u } function s(t, n) { for (var i = 0, r = n.length; r > i; i++)if (n[i].o == t.n) return t.tag = "#", !0 } function a(t, n, i) { for (var r = 0, e = i.length; e > r; r++)if (i[r].c == t && i[r].o == n) return !0 } function o(t) { var n = []; for (var i in t) n.push('"' + c(i) + '": function(c,p,t,i) {' + t[i] + "}"); return "{ " + n.join(",") + " }" } function u(t) { var n = []; for (var i in t.partials) n.push('"' + c(i) + '":{name:"' + c(t.partials[i].name) + '", ' + u(t.partials[i]) + "}"); return "partials: {" + n.join(",") + "}, subs: " + o(t.subs) } function c(t) { return t.replace(m, "\\\\").replace(b, '\\"').replace(v, "\\n").replace(d, "\\r") } function l(t) { return ~t.indexOf(".") ? "d" : "f" } function f(t, n) { var i = "<" + (n.prefix || ""), r = i + t.n + k++; return n.partials[r] = { name: t.n, partials: {} }, n.code += 't.b(t.rp("' + c(r) + '",c,p,"' + (t.indent || "") + '"));', r } function h(t, n) { n.code += "t.b(t.t(t." + l(t.n) + '("' + c(t.n) + '",c,p,0)));' } function p(t) { return "t.b(" + t + ");" } var g = /\S/, b = /\"/g, v = /\n/g, d = /\r/g, m = /\\/g; t.tags = { "#": 1, "^": 2, "<": 3, $: 4, "/": 5, "!": 6, ">": 7, "=": 8, _v: 9, "{": 10, "&": 11, _t: 12 }, t.scan = function(e, s) { function a() { m.length > 0 && (x.push({ tag: "_t", text: new String(m) }), m = "") } function o() { for (var n = !0, i = w; i < x.length; i++)if (n = t.tags[x[i].tag] < t.tags._v || "_t" == x[i].tag && null === x[i].text.match(g), !n) return !1; return n } function u(t, n) { if (a(), t && o()) for (var i, r = w; r < x.length; r++)x[r].text && ((i = x[r + 1]) && ">" == i.tag && (i.indent = "" + x[r].text), x.splice(r, 1)); else n || x.push({ tag: "\n" }); k = !1, w = x.length } function c(t, n) { var r = "=" + S, e = t.indexOf(r, n), s = i(t.substring(t.indexOf("=", n) + 1, e)).split(" "); return T = s[0], S = s[s.length - 1], e + r.length - 1 } var l = e.length, f = 0, h = 1, p = 2, b = f, v = null, d = null, m = "", x = [], k = !1, y = 0, w = 0, T = "{{", S = "}}"; for (s && (s = s.split(" "), T = s[0], S = s[1]), y = 0; l > y; y++)b == f ? r(T, e, y) ? (--y, a(), b = h) : "\n" == e.charAt(y) ? u(k) : m += e.charAt(y) : b == h ? (y += T.length - 1, d = t.tags[e.charAt(y + 1)], v = d ? e.charAt(y + 1) : "_v", "=" == v ? (y = c(e, y), b = f) : (d && y++, b = p), k = y) : r(S, e, y) ? (x.push({ tag: v, n: i(m), otag: T, ctag: S, i: "/" == v ? k - T.length : y + S.length }), m = "", y += S.length - 1, b = f, "{" == v && ("}}" == S ? y++ : n(x[x.length - 1]))) : m += e.charAt(y); return u(k, !0), x }; var x = { _t: !0, "\n": !0, $: !0, "/": !0 }; t.stringify = function(n) { return "{code: function (c,p,i) { " + t.wrapMain(n.code) + " }," + u(n) + "}" }; var k = 0; t.generate = function(n, i, r) { k = 0; var e = { code: "", subs: {}, partials: {} }; return t.walk(n, e), r.asString ? this.stringify(e, i, r) : this.makeTemplate(e, i, r) }, t.wrapMain = function(t) { return 'var t=this;t.b(i=i||"");' + t + "return t.fl();" }, t.template = t.Template, t.makeTemplate = function(t, n, i) { var r = this.makePartials(t); return r.code = Function("c", "p", "i", this.wrapMain(t.code)), new this.template(r, n, this, i) }, t.makePartials = function(t) { var n, i = { subs: {}, partials: t.partials, name: t.name }; for (n in i.partials) i.partials[n] = this.makePartials(i.partials[n]); for (n in t.subs) i.subs[n] = Function("c", "p", "t", "i", t.subs[n]); return i }, t.codegen = { "#": function(n, i) { i.code += "if(t.s(t." + l(n.n) + '("' + c(n.n) + '",c,p,1),c,p,0,' + n.i + "," + n.end + ',"' + n.otag + " " + n.ctag + '")){t.rs(c,p,function(c,p,t){', t.walk(n.nodes, i), i.code += "});c.pop();}" }, "^": function(n, i) { i.code += "if(!t.s(t." + l(n.n) + '("' + c(n.n) + '",c,p,1),c,p,1,0,0,"")){', t.walk(n.nodes, i), i.code += "};" }, ">": f, "<": function(n, i) { var r = { partials: {}, code: "", subs: {}, inPartial: !0 }; t.walk(n.nodes, r); var e = i.partials[f(n, i)]; e.subs = r.subs, e.partials = r.partials }, $: function(n, i) { var r = { subs: {}, code: "", partials: i.partials, prefix: n.n }; t.walk(n.nodes, r), i.subs[n.n] = r.code, i.inPartial || (i.code += 't.sub("' + c(n.n) + '",c,p,i);') }, "\n": function(t, n) { n.code += p('"\\n"' + (t.last ? "" : " + i")) }, _v: function(t, n) { n.code += "t.b(t.v(t." + l(t.n) + '("' + c(t.n) + '",c,p,0)));' }, _t: function(t, n) { n.code += p('"' + c(t.text) + '"') }, "{": h, "&": h }, t.walk = function(n, i) { for (var r, e = 0, s = n.length; s > e; e++)r = t.codegen[n[e].tag], r && r(n[e], i); return i }, t.parse = function(t, n, i) { return i = i || {}, e(t, "", [], i.sectionTags || []) }, t.cache = {}, t.cacheKey = function(t, n) { return [t, !!n.asString, !!n.disableLambda, n.delimiters, !!n.modelGet].join("||") }, t.compile = function(n, i) { i = i || {}; var r = t.cacheKey(n, i), e = this.cache[r]; if (e) { var s = e.partials; for (var a in s) delete s[a].instance; return e } return e = this.generate(this.parse(this.scan(n, i.delimiters), n, i), n, i), this.cache[r] = e } }("undefined" != typeof exports ? exports : Hogan);

/*!
    jQuery Iframe Transport Plugin 1.8.3
    https://github.com/blueimp/jQuery-File-Upload
    * Required by file upload
*/
!function(t) { "use strict"; "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : window.jQuery) }(function(t) { "use strict"; var n = 0; t.ajaxTransport("iframe", function(e) { if (e.async) { var r, i, a, s = e.initialIframeSrc || "javascript:false;"; return { send: function(o, u) { r = t('<form style="display:none;"></form>'), r.attr("accept-charset", e.formAcceptCharset), a = /\?/.test(e.url) ? "&" : "?", "DELETE" === e.type ? (e.url = e.url + a + "_method=DELETE", e.type = "POST") : "PUT" === e.type ? (e.url = e.url + a + "_method=PUT", e.type = "POST") : "PATCH" === e.type && (e.url = e.url + a + "_method=PATCH", e.type = "POST"), n += 1, i = t('<iframe src="' + s + '" name="iframe-transport-' + n + '"></iframe>').bind("load", function() { var n, a = t.isArray(e.paramName) ? e.paramName : [e.paramName]; i.unbind("load").bind("load", function() { var n; try { if (n = i.contents(), !n.length || !n[0].firstChild) throw Error() } catch (e) { n = void 0 } u(200, "success", { iframe: n }), t('<iframe src="' + s + '"></iframe>').appendTo(r), window.setTimeout(function() { r.remove() }, 0) }), r.prop("target", i.prop("name")).prop("action", e.url).prop("method", e.type), e.formData && t.each(e.formData, function(n, e) { t('<input type="hidden"/>').prop("name", e.name).val(e.value).appendTo(r) }), e.fileInput && e.fileInput.length && "POST" === e.type && (n = e.fileInput.clone(), e.fileInput.after(function(t) { return n[t] }), e.paramName && e.fileInput.each(function(n) { t(this).prop("name", a[n] || e.paramName) }), r.append(e.fileInput).prop("enctype", "multipart/form-data").prop("encoding", "multipart/form-data"), e.fileInput.removeAttr("form")), r.submit(), n && n.length && e.fileInput.each(function(e, r) { var i = t(n[e]); t(r).prop("name", i.prop("name")).attr("form", i.attr("form")), i.replaceWith(r) }) }), r.append(i).appendTo(document.body) }, abort: function() { i && i.unbind("load").prop("src", s), r && r.remove() } } } }), t.ajaxSetup({ converters: { "iframe text": function(n) { return n && t(n[0].body).text() }, "iframe json": function(n) { return n && t.parseJSON(t(n[0].body).text()) }, "iframe html": function(n) { return n && t(n[0].body).html() }, "iframe xml": function(n) { var e = n && n[0]; return e && t.isXMLDoc(e) ? e : t.parseXML(e.XMLDocument && e.XMLDocument.xml || t(e.body).html()) }, "iframe script": function(n) { return n && t.globalEval(t(n[0].body).text()) } } }) });

/*!
    jQuery UI - v1.11.1+CommonJS - 2014-09-17
    * Required by file upload
*/
!function(t) { "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : jQuery) }(function(t) {/*!
 * jQuery UI Widget 1.11.1
 * http://jqueryui.com
 *
 * Copyright 2014 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/jQuery.widget/
 */
    var e = 0, n = Array.prototype.slice; t.cleanData = function(e) { return function(n) { var i, r, s; for (s = 0; null != (r = n[s]); s++)try { i = t._data(r, "events"), i && i.remove && t(r).triggerHandler("remove") } catch (a) { } e(n) } }(t.cleanData), t.widget = function(e, n, i) { var r, s, a, o, u = {}, c = e.split(".")[0]; return e = e.split(".")[1], r = c + "-" + e, i || (i = n, n = t.Widget), t.expr[":"][r.toLowerCase()] = function(e) { return !!t.data(e, r) }, t[c] = t[c] || {}, s = t[c][e], a = t[c][e] = function(t, e) { return this._createWidget ? void (arguments.length && this._createWidget(t, e)) : new a(t, e) }, t.extend(a, s, { version: i.version, _proto: t.extend({}, i), _childConstructors: [] }), o = new n, o.options = t.widget.extend({}, o.options), t.each(i, function(e, i) { return t.isFunction(i) ? void (u[e] = function() { var t = function() { return n.prototype[e].apply(this, arguments) }, r = function(t) { return n.prototype[e].apply(this, t) }; return function() { var e, n = this._super, s = this._superApply; return this._super = t, this._superApply = r, e = i.apply(this, arguments), this._super = n, this._superApply = s, e } }()) : void (u[e] = i) }), a.prototype = t.widget.extend(o, { widgetEventPrefix: s ? o.widgetEventPrefix || e : e }, u, { constructor: a, namespace: c, widgetName: e, widgetFullName: r }), s ? (t.each(s._childConstructors, function(e, n) { var i = n.prototype; t.widget(i.namespace + "." + i.widgetName, a, n._proto) }), delete s._childConstructors) : n._childConstructors.push(a), t.widget.bridge(e, a), a }, t.widget.extend = function(e) { for (var i, r, s = n.call(arguments, 1), a = 0, o = s.length; o > a; a++)for (i in s[a]) r = s[a][i], s[a].hasOwnProperty(i) && void 0 !== r && (e[i] = t.isPlainObject(r) ? t.isPlainObject(e[i]) ? t.widget.extend({}, e[i], r) : t.widget.extend({}, r) : r); return e }, t.widget.bridge = function(e, i) { var r = i.prototype.widgetFullName || e; t.fn[e] = function(s) { var a = "string" == typeof s, o = n.call(arguments, 1), u = this; return s = !a && o.length ? t.widget.extend.apply(null, [s].concat(o)) : s, this.each(a ? function() { var n, i = t.data(this, r); return "instance" === s ? (u = i, !1) : i ? t.isFunction(i[s]) && "_" !== s.charAt(0) ? (n = i[s].apply(i, o), n !== i && void 0 !== n ? (u = n && n.jquery ? u.pushStack(n.get()) : n, !1) : void 0) : t.error("no such method '" + s + "' for " + e + " widget instance") : t.error("cannot call methods on " + e + " prior to initialization; attempted to call method '" + s + "'") } : function() { var e = t.data(this, r); e ? (e.option(s || {}), e._init && e._init()) : t.data(this, r, new i(s, this)) }), u } }, t.Widget = function() { }, t.Widget._childConstructors = [], t.Widget.prototype = { widgetName: "widget", widgetEventPrefix: "", defaultElement: "<div>", options: { disabled: !1, create: null }, _createWidget: function(n, i) { i = t(i || this.defaultElement || this)[0], this.element = t(i), this.uuid = e++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), n), this.bindings = t(), this.hoverable = t(), this.focusable = t(), i !== this && (t.data(i, this.widgetFullName, this), this._on(!0, this.element, { remove: function(t) { t.target === i && this.destroy() } }), this.document = t(i.style ? i.ownerDocument : i.document || i), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init() }, _getCreateOptions: t.noop, _getCreateEventData: t.noop, _create: t.noop, _init: t.noop, destroy: function() { this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus") }, _destroy: t.noop, widget: function() { return this.element }, option: function(e, n) { var i, r, s, a = e; if (0 === arguments.length) return t.widget.extend({}, this.options); if ("string" == typeof e) if (a = {}, i = e.split("."), e = i.shift(), i.length) { for (r = a[e] = t.widget.extend({}, this.options[e]), s = 0; s < i.length - 1; s++)r[i[s]] = r[i[s]] || {}, r = r[i[s]]; if (e = i.pop(), 1 === arguments.length) return void 0 === r[e] ? null : r[e]; r[e] = n } else { if (1 === arguments.length) return void 0 === this.options[e] ? null : this.options[e]; a[e] = n } return this._setOptions(a), this }, _setOptions: function(t) { var e; for (e in t) this._setOption(e, t[e]); return this }, _setOption: function(t, e) { return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!e), e && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this }, enable: function() { return this._setOptions({ disabled: !1 }) }, disable: function() { return this._setOptions({ disabled: !0 }) }, _on: function(e, n, i) { var r, s = this; "boolean" != typeof e && (i = n, n = e, e = !1), i ? (n = r = t(n), this.bindings = this.bindings.add(n)) : (i = n, n = this.element, r = this.widget()), t.each(i, function(i, a) { function o() { return e || s.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof a ? s[a] : a).apply(s, arguments) : void 0 } "string" != typeof a && (o.guid = a.guid = a.guid || o.guid || t.guid++); var u = i.match(/^([\w:-]*)\s*(.*)$/), c = u[1] + s.eventNamespace, l = u[2]; l ? r.delegate(l, c, o) : n.bind(c, o) }) }, _off: function(t, e) { e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e) }, _delay: function(t, e) { function n() { return ("string" == typeof t ? i[t] : t).apply(i, arguments) } var i = this; return setTimeout(n, e || 0) }, _hoverable: function(e) { this.hoverable = this.hoverable.add(e), this._on(e, { mouseenter: function(e) { t(e.currentTarget).addClass("ui-state-hover") }, mouseleave: function(e) { t(e.currentTarget).removeClass("ui-state-hover") } }) }, _focusable: function(e) { this.focusable = this.focusable.add(e), this._on(e, { focusin: function(e) { t(e.currentTarget).addClass("ui-state-focus") }, focusout: function(e) { t(e.currentTarget).removeClass("ui-state-focus") } }) }, _trigger: function(e, n, i) { var r, s, a = this.options[e]; if (i = i || {}, n = t.Event(n), n.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), n.target = this.element[0], s = n.originalEvent) for (r in s) r in n || (n[r] = s[r]); return this.element.trigger(n, i), !(t.isFunction(a) && a.apply(this.element[0], [n].concat(i)) === !1 || n.isDefaultPrevented()) } }, t.each({ show: "fadeIn", hide: "fadeOut" }, function(e, n) { t.Widget.prototype["_" + e] = function(i, r, s) { "string" == typeof r && (r = { effect: r }); var a, o = r ? r === !0 || "number" == typeof r ? n : r.effect || n : e; r = r || {}, "number" == typeof r && (r = { duration: r }), a = !t.isEmptyObject(r), r.complete = s, r.delay && i.delay(r.delay), a && t.effects && t.effects.effect[o] ? i[e](r) : o !== e && i[o] ? i[o](r.duration, r.easing, s) : i.queue(function(n) { t(this)[e](), s && s.call(i[0]), n() }) } }); t.widget
});

/*!
    jQuery File Upload Plugin 5.42.3
    https://github.com/blueimp/jQuery-File-Upload
*/
!function(t) { "use strict"; "function" == typeof define && define.amd ? define(["jquery", "jquery.ui.widget"], t) : "object" == typeof exports ? t(require("jquery"), require("./vendor/jquery.ui.widget")) : t(window.jQuery) }(function(t) { "use strict"; function e(e) { var n = "dragover" === e; return function(i) { i.dataTransfer = i.originalEvent && i.originalEvent.dataTransfer; var r = i.dataTransfer; r && -1 !== t.inArray("Files", r.types) && this._trigger(e, t.Event(e, { delegatedEvent: i })) !== !1 && (i.preventDefault(), n && (r.dropEffect = "copy")) } } t.support.fileInput = !(RegExp("(Android (1\\.[0156]|2\\.[01]))|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)|(w(eb)?OSBrowser)|(webOS)|(Kindle/(1\\.0|2\\.[05]|3\\.0))").test(window.navigator.userAgent) || t('<input type="file">').prop("disabled")), t.support.xhrFileUpload = !(!window.ProgressEvent || !window.FileReader), t.support.xhrFormDataFileUpload = !!window.FormData, t.support.blobSlice = window.Blob && (Blob.prototype.slice || Blob.prototype.webkitSlice || Blob.prototype.mozSlice), t.widget("blueimp.fileupload", { options: { dropZone: t(document), pasteZone: void 0, fileInput: void 0, replaceFileInput: !0, paramName: void 0, singleFileUploads: !0, limitMultiFileUploads: void 0, limitMultiFileUploadSize: void 0, limitMultiFileUploadSizeOverhead: 512, sequentialUploads: !1, limitConcurrentUploads: void 0, forceIframeTransport: !1, redirect: void 0, redirectParamName: void 0, postMessage: void 0, multipart: !0, maxChunkSize: void 0, uploadedBytes: void 0, recalculateProgress: !0, progressInterval: 100, bitrateInterval: 500, autoUpload: !0, messages: { uploadedBytes: "Uploaded bytes exceed file size" }, i18n: function(e, n) { return e = this.messages[e] || "" + e, n && t.each(n, function(t, n) { e = e.replace("{" + t + "}", n) }), e }, formData: function(t) { return t.serializeArray() }, add: function(e, n) { return e.isDefaultPrevented() ? !1 : void ((n.autoUpload || n.autoUpload !== !1 && t(this).fileupload("option", "autoUpload")) && n.process().done(function() { n.submit() })) }, processData: !1, contentType: !1, cache: !1 }, _specialOptions: ["fileInput", "dropZone", "pasteZone", "multipart", "forceIframeTransport"], _blobSlice: t.support.blobSlice && function() { var t = this.slice || this.webkitSlice || this.mozSlice; return t.apply(this, arguments) }, _BitrateTimer: function() { this.timestamp = Date.now ? Date.now() : (new Date).getTime(), this.loaded = 0, this.bitrate = 0, this.getBitrate = function(t, e, n) { var i = t - this.timestamp; return this.bitrate && n && n >= i || (this.bitrate = (e - this.loaded) * (1e3 / i) * 8, this.loaded = e, this.timestamp = t), this.bitrate } }, _isXHRUpload: function(e) { return !e.forceIframeTransport && (!e.multipart && t.support.xhrFileUpload || t.support.xhrFormDataFileUpload) }, _getFormData: function(e) { var n; return "function" === t.type(e.formData) ? e.formData(e.form) : t.isArray(e.formData) ? e.formData : "object" === t.type(e.formData) ? (n = [], t.each(e.formData, function(t, e) { n.push({ name: t, value: e }) }), n) : [] }, _getTotal: function(e) { var n = 0; return t.each(e, function(t, e) { n += e.size || 1 }), n }, _initProgressObject: function(e) { var n = { loaded: 0, total: 0, bitrate: 0 }; e._progress ? t.extend(e._progress, n) : e._progress = n }, _initResponseObject: function(t) { var e; if (t._response) for (e in t._response) t._response.hasOwnProperty(e) && delete t._response[e]; else t._response = {} }, _onProgress: function(e, n) { if (e.lengthComputable) { var i, r = Date.now ? Date.now() : (new Date).getTime(); if (n._time && n.progressInterval && r - n._time < n.progressInterval && e.loaded !== e.total) return; n._time = r, i = Math.floor(e.loaded / e.total * (n.chunkSize || n._progress.total)) + (n.uploadedBytes || 0), this._progress.loaded += i - n._progress.loaded, this._progress.bitrate = this._bitrateTimer.getBitrate(r, this._progress.loaded, n.bitrateInterval), n._progress.loaded = n.loaded = i, n._progress.bitrate = n.bitrate = n._bitrateTimer.getBitrate(r, i, n.bitrateInterval), this._trigger("progress", t.Event("progress", { delegatedEvent: e }), n), this._trigger("progressall", t.Event("progressall", { delegatedEvent: e }), this._progress) } }, _initProgressListener: function(e) { var n = this, i = e.xhr ? e.xhr() : t.ajaxSettings.xhr(); i.upload && (t(i.upload).bind("progress", function(t) { var i = t.originalEvent; t.lengthComputable = i.lengthComputable, t.loaded = i.loaded, t.total = i.total, n._onProgress(t, e) }), e.xhr = function() { return i }) }, _isInstanceOf: function(t, e) { return Object.prototype.toString.call(e) === "[object " + t + "]" }, _initXHRData: function(e) { var n, i = this, r = e.files[0], s = e.multipart || !t.support.xhrFileUpload, o = "array" === t.type(e.paramName) ? e.paramName[0] : e.paramName; e.headers = t.extend({}, e.headers), e.contentRange && (e.headers["Content-Range"] = e.contentRange), s && !e.blob && this._isInstanceOf("File", r) || (e.headers["Content-Disposition"] = 'attachment; filename="' + encodeURI(r.name) + '"'), s ? t.support.xhrFormDataFileUpload && (e.postMessage ? (n = this._getFormData(e), e.blob ? n.push({ name: o, value: e.blob }) : t.each(e.files, function(i, r) { n.push({ name: "array" === t.type(e.paramName) && e.paramName[i] || o, value: r }) })) : (i._isInstanceOf("FormData", e.formData) ? n = e.formData : (n = new FormData, t.each(this._getFormData(e), function(t, e) { n.append(e.name, e.value) })), e.blob ? n.append(o, e.blob, r.name) : t.each(e.files, function(r, s) { (i._isInstanceOf("File", s) || i._isInstanceOf("Blob", s)) && n.append("array" === t.type(e.paramName) && e.paramName[r] || o, s, s.uploadName || s.name) })), e.data = n) : (e.contentType = r.type || "application/octet-stream", e.data = e.blob || r), e.blob = null }, _initIframeSettings: function(e) { var n = t("<a></a>").prop("href", e.url).prop("host"); e.dataType = "iframe " + (e.dataType || ""), e.formData = this._getFormData(e), e.redirect && n && n !== location.host && e.formData.push({ name: e.redirectParamName || "redirect", value: e.redirect }) }, _initDataSettings: function(t) { this._isXHRUpload(t) ? (this._chunkedUpload(t, !0) || (t.data || this._initXHRData(t), this._initProgressListener(t)), t.postMessage && (t.dataType = "postmessage " + (t.dataType || ""))) : this._initIframeSettings(t) }, _getParamName: function(e) { var n = t(e.fileInput), i = e.paramName; return i ? t.isArray(i) || (i = [i]) : (i = [], n.each(function() { for (var e = t(this), n = e.prop("name") || "files[]", r = (e.prop("files") || [1]).length; r;)i.push(n), r -= 1 }), i.length || (i = [n.prop("name") || "files[]"])), i }, _initFormSettings: function(e) { e.form && e.form.length || (e.form = t(e.fileInput.prop("form")), e.form.length || (e.form = t(this.options.fileInput.prop("form")))), e.paramName = this._getParamName(e), e.url || (e.url = e.form.prop("action") || location.href), e.type = (e.type || "string" === t.type(e.form.prop("method")) && e.form.prop("method") || "").toUpperCase(), "POST" !== e.type && "PUT" !== e.type && "PATCH" !== e.type && (e.type = "POST"), e.formAcceptCharset || (e.formAcceptCharset = e.form.attr("accept-charset")) }, _getAJAXSettings: function(e) { var n = t.extend({}, this.options, e); return this._initFormSettings(n), this._initDataSettings(n), n }, _getDeferredState: function(t) { return t.state ? t.state() : t.isResolved() ? "resolved" : t.isRejected() ? "rejected" : "pending" }, _enhancePromise: function(t) { return t.success = t.done, t.error = t.fail, t.complete = t.always, t }, _getXHRPromise: function(e, n, i) { var r = t.Deferred(), s = r.promise(); return n = n || this.options.context || s, e === !0 ? r.resolveWith(n, i) : e === !1 && r.rejectWith(n, i), s.abort = r.promise, this._enhancePromise(s) }, _addConvenienceMethods: function(e, n) { var i = this, r = function(e) { return t.Deferred().resolveWith(i, e).promise() }; n.process = function(e, s) { return (e || s) && (n._processQueue = this._processQueue = (this._processQueue || r([this])).pipe(function() { return n.errorThrown ? t.Deferred().rejectWith(i, [n]).promise() : r(arguments) }).pipe(e, s)), this._processQueue || r([this]) }, n.submit = function() { return "pending" !== this.state() && (n.jqXHR = this.jqXHR = i._trigger("submit", t.Event("submit", { delegatedEvent: e }), this) !== !1 && i._onSend(e, this)), this.jqXHR || i._getXHRPromise() }, n.abort = function() { return this.jqXHR ? this.jqXHR.abort() : (this.errorThrown = "abort", i._trigger("fail", null, this), i._getXHRPromise(!1)) }, n.state = function() { return this.jqXHR ? i._getDeferredState(this.jqXHR) : this._processQueue ? i._getDeferredState(this._processQueue) : void 0 }, n.processing = function() { return !this.jqXHR && this._processQueue && "pending" === i._getDeferredState(this._processQueue) }, n.progress = function() { return this._progress }, n.response = function() { return this._response } }, _getUploadedBytes: function(t) { var e = t.getResponseHeader("Range"), n = e && e.split("-"), i = n && n.length > 1 && parseInt(n[1], 10); return i && i + 1 }, _chunkedUpload: function(e, n) { e.uploadedBytes = e.uploadedBytes || 0; var i, r, s = this, o = e.files[0], a = o.size, l = e.uploadedBytes, u = e.maxChunkSize || a, p = this._blobSlice, c = t.Deferred(), f = c.promise(); return this._isXHRUpload(e) && p && (l || a > u) && !e.data ? n ? !0 : a > l ? (r = function() { var n = t.extend({}, e), f = n._progress.loaded; n.blob = p.call(o, l, l + u, o.type), n.chunkSize = n.blob.size, n.contentRange = "bytes " + l + "-" + (l + n.chunkSize - 1) + "/" + a, s._initXHRData(n), s._initProgressListener(n), i = (s._trigger("chunksend", null, n) !== !1 && t.ajax(n) || s._getXHRPromise(!1, n.context)).done(function(i, o, u) { l = s._getUploadedBytes(u) || l + n.chunkSize, f + n.chunkSize - n._progress.loaded && s._onProgress(t.Event("progress", { lengthComputable: !0, loaded: l - n.uploadedBytes, total: l - n.uploadedBytes }), n), e.uploadedBytes = n.uploadedBytes = l, n.result = i, n.textStatus = o, n.jqXHR = u, s._trigger("chunkdone", null, n), s._trigger("chunkalways", null, n), a > l ? r() : c.resolveWith(n.context, [i, o, u]) }).fail(function(t, e, i) { n.jqXHR = t, n.textStatus = e, n.errorThrown = i, s._trigger("chunkfail", null, n), s._trigger("chunkalways", null, n), c.rejectWith(n.context, [t, e, i]) }) }, this._enhancePromise(f), f.abort = function() { return i.abort() }, r(), f) : (o.error = e.i18n("uploadedBytes"), this._getXHRPromise(!1, e.context, [null, "error", o.error])) : !1 }, _beforeSend: function(t, e) { 0 === this._active && (this._trigger("start"), this._bitrateTimer = new this._BitrateTimer, this._progress.loaded = this._progress.total = 0, this._progress.bitrate = 0), this._initResponseObject(e), this._initProgressObject(e), e._progress.loaded = e.loaded = e.uploadedBytes || 0, e._progress.total = e.total = this._getTotal(e.files) || 1, e._progress.bitrate = e.bitrate = 0, this._active += 1, this._progress.loaded += e.loaded, this._progress.total += e.total }, _onDone: function(e, n, i, r) { var s = r._progress.total, o = r._response; r._progress.loaded < s && this._onProgress(t.Event("progress", { lengthComputable: !0, loaded: s, total: s }), r), o.result = r.result = e, o.textStatus = r.textStatus = n, o.jqXHR = r.jqXHR = i, this._trigger("done", null, r) }, _onFail: function(t, e, n, i) { var r = i._response; i.recalculateProgress && (this._progress.loaded -= i._progress.loaded, this._progress.total -= i._progress.total), r.jqXHR = i.jqXHR = t, r.textStatus = i.textStatus = e, r.errorThrown = i.errorThrown = n, this._trigger("fail", null, i) }, _onAlways: function(t, e, n, i) { this._trigger("always", null, i) }, _onSend: function(e, n) { n.submit || this._addConvenienceMethods(e, n); var i, r, s, o, a = this, l = a._getAJAXSettings(n), u = function() { return a._sending += 1, l._bitrateTimer = new a._BitrateTimer, i = i || ((r || a._trigger("send", t.Event("send", { delegatedEvent: e }), l) === !1) && a._getXHRPromise(!1, l.context, r) || a._chunkedUpload(l) || t.ajax(l)).done(function(t, e, n) { a._onDone(t, e, n, l) }).fail(function(t, e, n) { a._onFail(t, e, n, l) }).always(function(t, e, n) { if (a._onAlways(t, e, n, l), a._sending -= 1, a._active -= 1, l.limitConcurrentUploads && l.limitConcurrentUploads > a._sending) for (var i = a._slots.shift(); i;) { if ("pending" === a._getDeferredState(i)) { i.resolve(); break } i = a._slots.shift() } 0 === a._active && a._trigger("stop") }) }; return this._beforeSend(e, l), this.options.sequentialUploads || this.options.limitConcurrentUploads && this.options.limitConcurrentUploads <= this._sending ? (this.options.limitConcurrentUploads > 1 ? (s = t.Deferred(), this._slots.push(s), o = s.pipe(u)) : (this._sequence = this._sequence.pipe(u, u), o = this._sequence), o.abort = function() { return r = [void 0, "abort", "abort"], i ? i.abort() : (s && s.rejectWith(l.context, r), u()) }, this._enhancePromise(o)) : u() }, _onAdd: function(e, n) { var i, r, s, o, a = this, l = !0, u = t.extend({}, this.options, n), p = n.files, c = p.length, f = u.limitMultiFileUploads, h = u.limitMultiFileUploadSize, d = u.limitMultiFileUploadSizeOverhead, g = 0, m = this._getParamName(u), v = 0; if (!h || c && void 0 !== p[0].size || (h = void 0), (u.singleFileUploads || f || h) && this._isXHRUpload(u)) if (u.singleFileUploads || h || !f) if (!u.singleFileUploads && h) for (s = [], i = [], o = 0; c > o; o += 1)g += p[o].size + d, (o + 1 === c || g + p[o + 1].size + d > h || f && o + 1 - v >= f) && (s.push(p.slice(v, o + 1)), r = m.slice(v, o + 1), r.length || (r = m), i.push(r), v = o + 1, g = 0); else i = m; else for (s = [], i = [], o = 0; c > o; o += f)s.push(p.slice(o, o + f)), r = m.slice(o, o + f), r.length || (r = m), i.push(r); else s = [p], i = [m]; return n.originalFiles = p, t.each(s || p, function(r, o) { var u = t.extend({}, n); return u.files = s ? o : [o], u.paramName = i[r], a._initResponseObject(u), a._initProgressObject(u), a._addConvenienceMethods(e, u), l = a._trigger("add", t.Event("add", { delegatedEvent: e }), u) }), l }, _replaceFileInput: function(e) { var n = e.fileInput, i = n.clone(!0); e.fileInputClone = i, t("<form></form>").append(i)[0].reset(), n.after(i).detach(), t.cleanData(n.unbind("remove")), this.options.fileInput = this.options.fileInput.map(function(t, e) { return e === n[0] ? i[0] : e }), n[0] === this.element[0] && (this.element = i) }, _handleFileTreeEntry: function(e, n) { var i, r = this, s = t.Deferred(), o = function(t) { t && !t.entry && (t.entry = e), s.resolve([t]) }, a = function(t) { r._handleFileTreeEntries(t, n + e.name + "/").done(function(t) { s.resolve(t) }).fail(o) }, l = function() { i.readEntries(function(t) { t.length ? (u = u.concat(t), l()) : a(u) }, o) }, u = []; return n = n || "", e.isFile ? e._file ? (e._file.relativePath = n, s.resolve(e._file)) : e.file(function(t) { t.relativePath = n, s.resolve(t) }, o) : e.isDirectory ? (i = e.createReader(), l()) : s.resolve([]), s.promise() }, _handleFileTreeEntries: function(e, n) { var i = this; return t.when.apply(t, t.map(e, function(t) { return i._handleFileTreeEntry(t, n) })).pipe(function() { return Array.prototype.concat.apply([], arguments) }) }, _getDroppedFiles: function(e) { e = e || {}; var n = e.items; return n && n.length && (n[0].webkitGetAsEntry || n[0].getAsEntry) ? this._handleFileTreeEntries(t.map(n, function(t) { var e; return t.webkitGetAsEntry ? (e = t.webkitGetAsEntry(), e && (e._file = t.getAsFile()), e) : t.getAsEntry() })) : t.Deferred().resolve(t.makeArray(e.files)).promise() }, _getSingleFileInputFiles: function(e) { e = t(e); var n, i, r = e.prop("webkitEntries") || e.prop("entries"); if (r && r.length) return this._handleFileTreeEntries(r); if (n = t.makeArray(e.prop("files")), n.length) void 0 === n[0].name && n[0].fileName && t.each(n, function(t, e) { e.name = e.fileName, e.size = e.fileSize }); else { if (i = e.prop("value"), !i) return t.Deferred().resolve([]).promise(); n = [{ name: i.replace(/^.*\\/, "") }] } return t.Deferred().resolve(n).promise() }, _getFileInputFiles: function(e) { return e instanceof t && 1 !== e.length ? t.when.apply(t, t.map(e, this._getSingleFileInputFiles)).pipe(function() { return Array.prototype.concat.apply([], arguments) }) : this._getSingleFileInputFiles(e) }, _onChange: function(e) { var n = this, i = { fileInput: t(e.target), form: t(e.target.form) }; this._getFileInputFiles(i.fileInput).always(function(r) { i.files = r, n.options.replaceFileInput && n._replaceFileInput(i), n._trigger("change", t.Event("change", { delegatedEvent: e }), i) !== !1 && n._onAdd(e, i) }) }, _onPaste: function(e) { var n = e.originalEvent && e.originalEvent.clipboardData && e.originalEvent.clipboardData.items, i = { files: [] }; n && n.length && (t.each(n, function(t, e) { var n = e.getAsFile && e.getAsFile(); n && i.files.push(n) }), this._trigger("paste", t.Event("paste", { delegatedEvent: e }), i) !== !1 && this._onAdd(e, i)) }, _onDrop: function(e) { e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer; var n = this, i = e.dataTransfer, r = {}; i && i.files && i.files.length && (e.preventDefault(), this._getDroppedFiles(i).always(function(i) { r.files = i, n._trigger("drop", t.Event("drop", { delegatedEvent: e }), r) !== !1 && n._onAdd(e, r) })) }, _onDragOver: e("dragover"), _onDragEnter: e("dragenter"), _onDragLeave: e("dragleave"), _initEventHandlers: function() { this._isXHRUpload(this.options) && (this._on(this.options.dropZone, { dragover: this._onDragOver, drop: this._onDrop, dragenter: this._onDragEnter, dragleave: this._onDragLeave }), this._on(this.options.pasteZone, { paste: this._onPaste })), t.support.fileInput && this._on(this.options.fileInput, { change: this._onChange }) }, _destroyEventHandlers: function() { this._off(this.options.dropZone, "dragenter dragleave dragover drop"), this._off(this.options.pasteZone, "paste"), this._off(this.options.fileInput, "change") }, _setOption: function(e, n) { var i = -1 !== t.inArray(e, this._specialOptions); i && this._destroyEventHandlers(), this._super(e, n), i && (this._initSpecialOptions(), this._initEventHandlers()) }, _initSpecialOptions: function() { var e = this.options; void 0 === e.fileInput ? e.fileInput = this.element.is('input[type="file"]') ? this.element : this.element.find('input[type="file"]') : e.fileInput instanceof t || (e.fileInput = t(e.fileInput)), e.dropZone instanceof t || (e.dropZone = t(e.dropZone)), e.pasteZone instanceof t || (e.pasteZone = t(e.pasteZone)) }, _getRegExp: function(t) { var e = t.split("/"), n = e.pop(); return e.shift(), RegExp(e.join("/"), n) }, _isRegExpOption: function(e, n) { return "url" !== e && "string" === t.type(n) && /^\/.*\/[igm]{0,3}$/.test(n) }, _initDataAttributes: function() { var e = this, n = this.options, i = this.element.data(); t.each(this.element[0].attributes, function(t, r) { var s, o = r.name.toLowerCase(); /^data-/.test(o) && (o = o.slice(5).replace(/-[a-z]/g, function(t) { return t.charAt(1).toUpperCase() }), s = i[o], e._isRegExpOption(o, s) && (s = e._getRegExp(s)), n[o] = s) }) }, _create: function() { this._initDataAttributes(), this._initSpecialOptions(), this._slots = [], this._sequence = this._getXHRPromise(!0), this._sending = this._active = 0, this._initProgressObject(this), this._initEventHandlers() }, active: function() { return this._active }, progress: function() { return this._progress }, add: function(e) { var n = this; e && !this.options.disabled && (e.fileInput && !e.files ? this._getFileInputFiles(e.fileInput).always(function(t) { e.files = t, n._onAdd(null, e) }) : (e.files = t.makeArray(e.files), this._onAdd(null, e))) }, send: function(e) { if (e && !this.options.disabled) { if (e.fileInput && !e.files) { var n, i, r = this, s = t.Deferred(), o = s.promise(); return o.abort = function() { return i = !0, n ? n.abort() : (s.reject(null, "abort", "abort"), o) }, this._getFileInputFiles(e.fileInput).always(function(t) { if (!i) { if (!t.length) return void s.reject(); e.files = t, n = r._onSend(null, e), n.then(function(t, e, n) { s.resolve(t, e, n) }, function(t, e, n) { s.reject(t, e, n) }) } }), this._enhancePromise(o) } if (e.files = t.makeArray(e.files), e.files.length) return this._onSend(null, e) } return this._getXHRPromise(!1, e && e.context) } }) });



$.fn.isOnScreen = function() {

    var win = $(window);

    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};
