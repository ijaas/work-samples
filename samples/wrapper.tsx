import React, { useMemo, useEffect, useState } from 'react';
import { Platform, UIManager } from 'react-native';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import { RootSiblingParent } from 'react-native-root-siblings';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { useStore, useUser, useAsyncEffect, useDataStore, Events } from '@dex/bubl-ui';
import { get, parseNum } from '@dex/bubl-helpers';
import * as Font from 'expo-font'
import config from '../Config/Config';
import LoginApi from '../Login/LoginApi';
import AppNavigator from '../Navigator/Navigator';
import { apiSetHeader, apiClearCache } from '@dex/bubl-fetch';

if (Platform.OS === "android" && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

if (get(config, 'settings.notifications.enabled')) {

    Notifications.setNotificationHandler({
        handleNotification: async () => ({
            shouldShowAlert: get(config, 'settings.notifications.shouldShowAlert', true),
            shouldPlaySound: get(config, 'settings.notifications.shouldPlaySound', true),
            shouldSetBadge: get(config, 'settings.notifications.shouldSetBadge', false),
        }),
    });

}

interface AppWrapperProps {
    renderBeforeNavigator?: () => React.ReactNode,
    renderAfterNavigator?: () => React.ReactNode,
    renderLoading?: () => React.ReactNode,
    renderWrapper?: React.FC,
    uiStyles?: any,
    fonts?: { [key: string]: string },
};

const AppWrapper: React.FC<AppWrapperProps> = (props) => {

    const [key] = useStore('appKey');

    const [loaded, setLoaded] = useStore("appLoaded", false);

    const [initialState, setInitialState] = useStore("appInitialStateState", '');

    const [loading, setLoading] = useState(false);

    const user = useUser("appUser");

    const done = {
        me: useState(false),
        fonts: useState(false),
        navigation: useState(false),
    }

    const me = LoginApi.me({
        onError: () => {
            //do nothing
        }
    });

    useEffect(() => {

        if (!get(config, 'settings.notifications.enabled')) return;

        const notificationListener = Notifications.addNotificationReceivedListener((notification: any) => {

            if (__DEV__) console.log("notification received", notification);

        });

        const notificationResponseListener = Notifications.addNotificationResponseReceivedListener((response: any) => {

            if (__DEV__) console.log("notification tapped", response);

        });

        return () => {

            Notifications.removeNotificationSubscription(notificationListener);
            Notifications.removeNotificationSubscription(notificationResponseListener);

        };

    }, []);

    useAsyncEffect(async (isMounted) => {

        if (loaded || loading) return;

        setLoading(true);

        apiClearCache();

        const token = await useDataStore.get('appUserToken');

        if (config.endpoint === "local") {

            apiSetHeader("authorization", token);

        }

        let pushToken = await useDataStore.get('appPushToken');

        if (token && !pushToken && get(config, 'settings.notifications.enabled')) {

            if (Constants.isDevice) {

                const { status: existingStatus } = await Notifications.getPermissionsAsync();

                let finalStatus = existingStatus;

                if (existingStatus !== 'granted') {

                    const { status } = await Notifications.requestPermissionsAsync();

                    finalStatus = status;

                }

                if (finalStatus === 'granted') {

                    pushToken = (await Notifications.getExpoPushTokenAsync()).data;

                    useDataStore.set('appPushToken', pushToken);

                } else {

                    Events.emit("ui", {
                        action: 'info',
                        title: 'Failed to generate token for push notifications.',
                    });

                }

            } else {

                console.error('Must use physical device for Push Notifications');

            }

            const notificationChannels = get(config, 'settings.notifications.channels', null);

            if (Platform.OS === 'android' && notificationChannels) {

                notificationChannels.map((channelInput: any) => {

                    Notifications.setNotificationChannelAsync(channelInput.name, {
                        importance: Notifications.AndroidImportance.MAX,
                        vibrationPattern: [0, 250, 250, 250],
                        lightColor: '#FF231F7C',
                        ...channelInput,

                        // // Optional attributes
                        // bypassDnd?: boolean;
                        // description?: string | null;
                        // groupId?: string | null;
                        // lightColor?: string;
                        // lockscreenVisibility?: AndroidNotificationVisibility;
                        // showBadge?: boolean;
                        // sound?: string | null;
                        // audioAttributes?: Partial<AudioAttributes>;
                        // vibrationPattern?: number[] | null;
                        // enableLights?: boolean;
                        // enableVibrate?: boolean;
                    });

                });

            }

        }

        if (!isMounted()) return;

        new Promise((resolve, reject) => {

            const send: any = [
                "version"
                //translations //once a day
            ];

            me.run({
                params: {
                    pushToken: pushToken,
                    platform: "app",
                    send: send
                },
                onEnd: async (error: any, data: any) => {

                    if (error) {

                        //use user from data store
                        const currentUser = await useDataStore.get('currentUser' + config.storeKey);

                        if (currentUser && currentUser.id) {

                            user.setState(currentUser);

                        }

                    } else {

                        if (data.user) {

                            useDataStore.set('currentUser' + config.storeKey, data.user);

                            user.setState(data.user);

                        } else if (user.loggedIn) {

                            useDataStore.set('currentUser' + config.storeKey, null);

                            user.setState(null);

                        }

                        if (data.version && parseNum(data.version.replace(/\./g, "")) > parseNum(config.version.replace(/\./g, ""))) {

                            // todo: ask user to update from apple or play store
                            // can be skipped

                        }

                    }


                    resolve(true);

                }
            });

        }).finally(() => {

            done.me[1](true);

        });

        Font.loadAsync(props.fonts || {}).finally(() => {

            done.fonts[1](true);

        });

        const navState = await useDataStore.get("appNavigationState" + config.storeKey);
        const appReloaded = await useDataStore.get("appReloaded" + config.storeKey);

        if (navState && appReloaded) {

            setInitialState(navState);

            useDataStore.set("appReloaded" + config.storeKey, false);

        } else if (navState && __DEV__) {

            setInitialState(navState);

        }

        done.navigation[1](true);

        if (!isMounted()) return;

    }, [key]);

    useEffect(() => {

        if (done.me[0] && done.fonts[0] && done.navigation[0]) {

            config.loaded();

            setLoaded(true);

            setLoading(false);

            done.me[1](false);
            done.fonts[1](false);
            done.navigation[1](false);

        }

    }, [done.me[0], done.fonts[0], done.navigation[0]]);

    return useMemo(() => {

        const Wrapper = props.renderWrapper ? props.renderWrapper : React.Fragment;

        return (

            <SafeAreaProvider>

                <RootSiblingParent>

                    {loaded &&

                        <Wrapper>

                            <AppNavigator
                                renderBefore={props.renderBeforeNavigator}
                                renderAfter={props.renderAfterNavigator}
                                renderLoading={props.renderLoading}
                                uiStyles={props.uiStyles}
                            />

                        </Wrapper>

                    }

                </RootSiblingParent>

            </SafeAreaProvider>

        )

    }, [loaded]);

}

export default AppWrapper;

export { AppWrapperProps };
