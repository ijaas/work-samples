import React from 'react';
import { renderProp } from '../../functions/renderProp';
import { sizeClasses } from '../../functions/sizeClasses';

const Row: React.FC<rowProps> = (props: rowProps) => {

    const defaults = {}

    const options: rowProps = { ...defaults, ...props };

    const classes = ["flex-row"];

    if (options.nowrap) classes.push("flex-nowrap");
    if (options.align) classes.push("flex-align-" + options.align);
    if (options.justify) classes.push("flex-" + options.justify);
    if (options.edge) classes.push("edge");

    if (options.gutter) classes.push(sizeClasses("gutter", options.gutter));

    if (options.className) classes.push(options.className);

    return (

        <div className={classes.join(" ")} onClick={props.onClick}>

            {props.children}

        </div>

    );

}

const Col: React.FC<colProps> = (props: colProps) => {

    const defaults = {
        // fluid: true,
        // col: 24,
    }

    const options: colProps = { ...defaults, ...props };

    const classes = ["flex-col"];

    const style = options.style || {};
    const box = options.box || {};

    if (options.grow) style.flexGrow = options.grow;
    if (options.basis) style.flexBasis = options.basis;
    if (options.shrink) style.flexShrink = options.shrink;

    if (options.align) box.align = options.align;
    if (options.justify) box.justify = options.justify;
    if (options.fluid) box.fluid = options.fluid;
    if (options.bg) box.bg = options.bg;
    if (options.windowHeight) box.windowHeight = options.windowHeight;

    if (options.col) classes.push(sizeClasses("col", options.col));

    if (options.className) classes.push(options.className);

    return (

        <div className={classes.join(" ")} style={style} onClick={props.onClick}>

            <Box {...box}>

                {props.children}

            </Box>

        </div>

    );

}

const Box: React.FC<boxProps> = (props: boxProps) => {

    const defaults = {}

    const options: boxProps = { ...defaults, ...props };

    const classes = ["flex-box"];

    if (options.align) classes.push("flex-align-" + options.align);
    if (options.justify) classes.push("flex-" + options.justify);
    if (options.fluid) classes.push("flex-box-fluid");
    if (options.windowHeight) classes.push("flex-box-window-height");

    if (options.gutter) classes.push(sizeClasses("gutter-box", options.gutter));
    if (options.gutter && options.edge === true) classes.push(sizeClasses("gutter-edge", options.gutter));
    if (options.gutter && options.edge?.top) classes.push(sizeClasses("gutter-edge-top", options.gutter));
    if (options.gutter && options.edge?.left) classes.push(sizeClasses("gutter-edge-left", options.gutter));
    if (options.gutter && options.edge?.right) classes.push(sizeClasses("gutter-edge-right", options.gutter));
    if (options.gutter && options.edge?.bottom) classes.push(sizeClasses("gutter-edge-bottom", options.gutter));

    if (options.className) classes.push(options.className);

    return (

        <div className={classes.join(" ")} onClick={props.onClick}>

            <div className={"flex-inner"}>

                {props.children}

            </div>

            {renderProp(props.bg)}

        </div>

    );

}

const Widgets: React.FC<widgetsProps> = (props: widgetsProps) => {

    const defaults = {
        gutter: "auto",
        edge: true,
    }

    const options: widgetsProps = { ...defaults, ...props };

    const classes = ["widgets"];

    if (options.className) classes.push(options.className);

    options.className = classes.join(" ");

    delete (options.children);

    let children = props.children || [];

    if (!Array.isArray(children)) children = [children];

    return (

        <Row {...options}>

            {children.map((child, index) => {

                return (

                    <div className="widget" key={index}>

                        {child}

                    </div>

                )

            })}

        </Row>

    );

}

const Space: React.FC<spaceProps> = (props: spaceProps) => {

    const defaults = {
        height: "auto",
    }

    const options: spaceProps = { ...defaults, ...props };

    const classes = ["space"];

    if (options.className) classes.push(options.className);

    if (options.height === "auto") {
        options.height = window['bublGutter'];
    } else if (options.height === "small") {
        options.height = window['bublGutterSmall'];
    } else if (options.height === "big") {
        options.height = window['bublGutterBig'];
    }

    if (typeof options.height === "object") {

        const keys = Object.keys(options.height);
        const heights = options.height || {};

        return (
            <>
                {keys.map(key => (
                    <div key={key} className={classes.join(" ") + " hidden " + key + "-show"} style={{ height: heights[key] }}></div>
                ))}
            </>
        );

    } else {

        return (

            <div className={classes.join(" ")} style={{ height: options.height }}></div>

        );

    }

}

interface sizeProps {
    xs?: number | string,
    sm?: number | string,
    md?: number | string,
    lg?: number | string,
    xl?: number | string,
}

interface rowProps {
    gutter?: sizeProps | string | number | object,
    align?: string,
    justify?: string,
    edge?: boolean,
    nowrap?: boolean,
    className?: string,
    [key: string]: any
}

interface colProps {
    col?: sizeProps | string | number | object,
    grow?: string | number,
    shrink?: string | number,
    basis?: string | number,
    align?: string,
    justify?: string,
    className?: string,
    fluid?: boolean,
    windowHeight?: boolean,
    bg?: React.ReactNode,
    [key: string]: any
}

interface boxProps {
    gutter?: sizeProps | string | number | object,
    align?: string,
    justify?: string,
    className?: string,
    fluid?: boolean,
    edge?: boolean | any | {
        top?: boolean,
        left?: boolean,
        right?: boolean,
        bottom?: boolean,
    },
    windowHeight?: boolean,
    bg?: React.ReactNode,
    [key: string]: any
}

interface spaceProps {
    height?: sizeProps | string | number | object,
    className?: string,
    [key: string]: any
}

interface widgetsProps {
    gutter?: sizeProps | string | number | object,
    edge?: boolean,
    className?: string,
    [key: string]: any
}

export { Row, Col, Box, Space, Widgets };
