<?php

function rea_form($id, $props = array(), $preview = null){

	global $rea, $wp, $wp_query, $post;

	$original = array(
		'wp_query'	=> $wp_query,
		'post'		=> $post,
	);

	$form		= _post($id);
	$rows		= get_field('rows', $form->ID);
	$button		= get_field('button', $form->ID);
	$disclaimer = get_field('disclaimer', $form->ID);
	$tooltips   = get_field('disclaimer_tooltip_bubbles', $form->ID);

    foreach($tooltips as $n => $tooltip){
        register_tooltip_bubble($tooltip['id'], $tooltip['contents']);
    };

	if($preview){
		$form->meta = (object) $preview;
		$rows = $preview['rows'];
		$button = $preview['button'];
		$disclaimer = $preview['disclaimer'];
	    $tooltips   = $preview['disclaimer_tooltip_bubbles'];
    }

	echo "<form class='rea-form {$props['class']} {$form->meta->class} needs-validation' method='post' novalidate data-id='{$form->ID}' data-name='".($form->meta->tracking_id? : __($form->post_title))."' data-fb-pixel-event='{$form->meta->facebook_pixel_event_name}' enctype='multipart/form-data'>";

        //added using js fn on submit
        //echo "<input type='hidden' name='rea-form-submission' value='{$form->ID}' />";

        //Honeypot
        echo "<div class='hidden'>";
            echo "<input type='text' name='type' value='1' autocomplete='off' />";
            echo "<input type='text' name='name' autocomplete='off' />";
        echo "</div>";

		foreach($rows as $row){

			$class = ($row['columns'] == 2 || $row['columns'] == "full")? "w-740" : ($row['columns'] == "hidden"? "hidden" : "w-350");
			$columnClass = $row['columns'] == 2? "col-12 col-md-6" : "col-12";

			if($row['columns'] == 2) $hasTwoColumms = true;

			echo "<div class='rea-form-row rea-form-row-{$row['columns']} container-fluid {$class}'>";
				echo "<div class='row'>";

					echo "<div class='col {$columnClass}'>";
						foreach($row['column_1'] as $field){
							rea_form_field($field['acf_fc_layout'], $field);

						}
					echo "</div>";

					if($row['columns'] == 2){
						echo "<div class='col {$columnClass}'>";
							foreach($row['column_2'] as $field){
								rea_form_field($field['acf_fc_layout'], $field);
							}
						echo "</div>";
					}

				echo "</div>";
			echo "</div>";

		}



        $class = $disclaimer && $hasTwoColumms? "w-740" : "w-350";

		echo "<div class='rea-form-row container-fluid {$class}'>";

            echo "<div class='row align-items-center'>";

            if($form->meta->tol_recaptcha_site_key){

				echo "<div class='col col-12 recaptcha'>";

                    $recaptcha_site_key = $form->meta->tol_recaptcha_site_key;
                    @include(locate_template("forms/recaptcha.php"));

				echo "</div>";

            }

			if($disclaimer && $hasTwoColumms){

				echo "<div class='col col-lg-7 col-12 text-left disclaimer-text'>";

					echo do_shortcode(nl2br($disclaimer));

				echo "</div>";

				echo "<div class='col col-lg-5 col-12 text-right'>";

					rea_module('buttons', $button);

				echo "</div>";

			} else {

				if($disclaimer){
					echo "<div class='col col-12 text-left disclaimer-text'>";

						echo do_shortcode(nl2br($disclaimer));

					echo "</div>";
				}

				echo "<div class='col col-12 text-right'>";

					// if($button && $button['buttons'] && $button['buttons'][0] && !trim($button['buttons'][0]['link'])){
					// 	$button['buttons'][0]['link'] = 'submit';
					// }

					if($button && $button['buttons']){

						foreach($button['buttons'] as $key => $btn){

							if(!$btn['type']) $btn['type'] = "submit";

							if($btn['type'] == "submit") $btn['link'] = "submit";
							if($btn['type'] == "reset") $btn['link'] = "reset";

							$button['buttons'][$key] = $btn;

						}

					}

					rea_module('buttons', $button);

				echo "</div>";

			}

			echo "</div>";

		echo "</div>";

		if($form->meta->action_on_complete == "message"){
			echo "<div class='on-complete-message hidden'>";

				rea_module('voted', array('heading' => $form->meta->on_complete_message_heading, 'sub_heading' => $form->meta->on_complete_message_sub_heading, 'ctas' => get_field("on_complete_message_ctas_buttons", $form->ID)));

			echo "</div>";
		}

		if($form->meta->action_on_complete == "redirect" && $form->meta->on_complete_redirect){
			echo "<div class='on-complete-redirect hidden' data-redirect='{$form->meta->on_complete_redirect}'></div>";
		}


	echo "</form>";

	// if($rea->modules[$index]){
	// 	echo "<div class='rea-module rea-module-{$index}'>";
	// 		@include($rea->modules[$index]."view.php");
	// 	echo "</div>";
	// }

	extract($original);

	return;

}

function rea_form_field($type, $props){

	if($type == "content" || $type == "loan_calculator"){

		@include(locate_template("forms/{$type}.php"));

	} else if($type == "custom_validation"){

		@include(locate_template("forms/validation.php"));

	} else {

        $field = _post($props['field']);

		if($field->ID){
			@include(locate_template("forms/{$field->meta->type}.php"));
		}

	}

}

function rea_form_encrypt($fid, $value){

	require_once(__DIR__."/lib/aws_kms.php");

	if(is_array($value)){

		$value = serialize($value);

		// $values = array();
		// foreach($value as $v) {
		// 	$values[] = rea_form_encrypt($fid, $value);
		// }
		// return $values;

	}

	return $value? _kms_encrypt($value) : $value;

}

function rea_form_decrypt($fid, $value){

	require_once(__DIR__."/lib/aws_kms.php");

	if(!is_string($value)) return $value;

	if(is_numeric($value)) return $value;

	if(!rea_test_base64($value)) return $value;

	return maybe_unserialize(_kms_decrypt($value));

}

function rea_test_base64($string){

    $decoded = base64_decode($string, true);

    // Check if there is no invalid character in string
    if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string)) return false;

    // Decode the string in strict mode and send the response
    if (!base64_decode($string, true)) return false;

    // Encode and compare it to original one
    if (base64_encode($decoded) != $string) return false;

	return true;

}

if(isset($_POST['rea-form-submission'])){
    add_action('init', 'rea_form_save_submission', 9);
    add_action('init', 'rea_form_save_submission_rea_close_and_exit', 10); //force rea_close_and_exit
}

function rea_form_save_submission_rea_close_and_exit(){

    echo json_encode(['error' => 'Unknown Error']);

    rea_close_and_exit();

}

function rea_form_save_submission(){

    global $wpdb, $rea;

	$data = $_POST? : array();
	$files = $_FILES? : array();
    $form = _post($data['rea-form-submission']);

    if($data['name'] || !$data['type']){
        echo json_encode(['error' => 'Input Error: Bender']);
	    rea_close_and_exit();
    }

    if(!$form){
        echo json_encode(['error' => 'Input Error: Invalid Form']);
	    rea_close_and_exit();
    }

    if($form->post_status != "publish"){
        echo json_encode(['error' => 'Input Error: Form Submissions Closed']);
	    rea_close_and_exit();
    }

	$fields = array();
	$plain = array();

    $mail = null;

	$meta = (object) array(
		'formId' => $form->ID,
		'status' => "New Entry",
		'user-agent' => $_SERVER['HTTP_USER_AGENT'],
		'referrer' => $_SERVER['HTTP_REFERER'],
        'ip' => getRealIpAddr(),
        'source' => $data['source']? : 'Web',
        'leadgenId' => $data['leadgenId']? : ''
    );

    // if($data['votes'] && $form->meta->enable_validation && $form->meta->validation_fields){

    //     sort($form->meta->validation_fields);

    //     $votehash = "";
    //     $or = "";

    //     foreach($form->meta->validation_fields as $n => $fid) {

    //         $hash = "/".md5(trim($data['field'][$fid]))."/";
    //         $votehash .= $hash;

    //         if($n) $or .= " OR ";

    //         $or .= "`form_hash` LIKE '%{$hash}%'";

    //     }

    //     $matches = $wpdb->get_row("SELECT * FROM `{$wpdb->reavotes}` WHERE `form_id` = '{$form->ID}' AND ({$or})");

    //     if($matches){
    //         echo json_encode(['error' => __($form->meta->validation_failed_message)]);
    // 	    rea_close_and_exit();
    //     }

    // }

    if($form->meta->enable_validation && $form->meta->validation_fields) {

        sort($form->meta->validation_fields);

        $entryhash = "";
        $or = "";

        foreach($form->meta->validation_fields as $n => $fid) {

            $data['field'][$fid] = strtolower($data['field'][$fid]);

            $hash = "/".md5(trim($data['field'][$fid]))."/";
            $entryhash .= $hash;

            if($n) $or .= " OR ";

            $or .= "`meta_value` LIKE '%{$hash}%'";

        }

        $matches = $wpdb->get_row("SELECT * FROM `{$wpdb->postmeta}` WHERE `meta_key` = 'entry_hash_{$form->ID}' AND ({$or})");

        if($matches){
            echo json_encode(['error' => __($form->meta->validation_failed_message)]);
    	    rea_close_and_exit();
        }

        $meta->{'entry_hash_'.$form->ID} = $entryhash;

    }

    if($form->meta->enable_email_verification && $form->meta->email_field && $form->meta->debounce_api_key) {

        $email = $data['field'][$form->meta->email_field];

        $verify = rea_debounce_verify($email, $form->meta->debounce_api_key);

        if(!$verify){

            echo json_encode(['error' => _strings('email_verification_failed', 'Email Verification Failed')]);
            rea_close_and_exit();

        }

    }

    $um = (object) array(
        'endpoint' => $rea->region->umApi->endpoint,
        'headers' => $rea->region->umApi->headers,
        'data' => array(
            'source' => $rea->region->umApi->source,
            'userType' => $form->meta->um_user_type? : "consumer",
            'form' => $form->meta->um_form_name? : __($form->post_title),
        ),
        'workshops' => array(),
        'hasData' => false,
    );

	$emarsys = (object) array(
		'endpoint' => $rea->region->emarsys->endpoint,
        'username' => $rea->region->emarsys->username,
        'secret' => $rea->region->emarsys->secret,
		'cid' => $rea->region->emarsys->cid,
		'formId' => $form->meta->emarsys_form_id,
		'data' => array(
			'CID' => $rea->region->emarsys->cid,
			'f' => $form->meta->emarsys_form_id,
			'p' => 2,
			'a' => 'r',
			'SID' => '',
			'el' => '',
			'llid' => '',
			'counted' => '',
			'c' => '',
			'optin' => "y"
        ),
        'fields' => array(
            31 => true,
            // 33 => 3,
            // 34 => $form->meta->emarsys_form_id,
        ),
		'optin' => true,
    );

    if($rea->region->emarsys->langId){

        $emarsys->data['inp_'.$rea->region->emarsys->langId] = qtranxf_getLanguage();
        // $emarsys->fields[$rea->region->emarsys->langId] = [qtranxf_getLanguage()];

    }

    if($form->meta->enable_user_sms){

        $meta->sms_code = rea_sms_code();

    }

    if($rea->book_collection && $form->meta->enable_book_collection){

        $meta->book = 1;
        $meta->collected_status = "Not Collected";

    }

    if($rea->region->eventxtra && $form->meta->eventxtra_party_id){

        $eventxtra = (object) array(
            'endpoint' => $rea->region->eventxtra->endpoint."api/parties/".$form->meta->eventxtra_party_id."/attendees/many.json",
            'token' => rea_eventxtra_token(),
            'data' => array(
                "auto_confirmation_email" => true,
                "answers_attributes" => [],
                "custom_attendee_fields" => [],
            )
        );

    }

	$output = array(
		'formId' => $form->ID,
        'emarsysId' => $emarsys->formId,
	);

	foreach($files as $fid => $file){

		$data['field'][$fid] = rea_form_save_file($fid, $form->ID);

	}

	foreach($data['field'] as $fid => $value){

		$field = _post($fid);

		if(!$field) continue;

		$plain[$fid] = $value;

		if($field->meta->type == 'text' && $field->meta->input_format == "email"){

            $email = $value;

		} else if($field->meta->type == 'text' && $field->meta->input_format == "phone"){

            $phone = $value;

		}

        if($field->meta->um_field_name){

            //do not send data to emarsys
            //disabled since they want to send data to emarsys
            //if($field->meta->um_field_name !== "email") $field->meta->emarsys_field_id = null;

            if($field->meta->um_field_name === "preferenceCodes" && !is_array($value)) $value = array($value);

            $value = ($value === "false" || $value === false)? "" : $value;

            if($field->meta->um_field_name === "workshop"){

                $date = explode("/", $value);

                if($date) $value = "{$date[2]}-{$date[1]}-{$date[0]}";

                $um->workshops[] = array(
                    array(
                        "propertyName" => "workshop_name",
                        "propertyType" => "string",
                        "propertyValue" => $field->meta->um_workshop_name
                    ),
                    array(
                        "propertyName" => "workshop_date",
                        "propertyType" => "time",
                        "propertyValue" => "{$value}T00:00:00.000{$rea->region->timezone}"
                    )
                );

            } else {

                if(strpos($field->meta->um_field_name, "-")){

                    foreach(explode('-', $field->meta->um_field_name) as $um_field_name){

                        $um->data[$um_field_name] = $value;

                    }

                } else {

                    $um->data[$field->meta->um_field_name] = $value;

                }

            }

            $um->hasData = true;

        }

        //Emarsys Integration
		if($field->meta->emarsys_field_id){

            $v = $value;

            $emarsys->data["inp_".$field->meta->emarsys_field_id] = is_array($v)? implode('|', $v) : $v;

            if($field->meta->type === "optin"){
                if($value === "true" || $value === true) $v = 1;
                else if($value === "false" || $value === false) $v = 2;
            }

            $emarsys->fields[$field->meta->emarsys_field_id] = is_array($v)? $v : $v;

        }

        //EventXtra Integration
        if($eventxtra && $field->meta->eventxtra_field_id){

            if($field->meta->eventxtra_field_id == "answers_attribute"){

                $eventxtra->data["answers_attributes"][] = array(
                    'question_id' => $field->meta->eventxtra_field_attribute_question_id,
                    'chosen_choice_ids' => is_array($value)? $value : array($value)
                );

            } elseif($field->meta->eventxtra_field_id == "custom_field"){

                $eventxtra->data["custom_attendee_fields"][$field->meta->eventxtra_field_custom_name] =  is_array($value)? implode(", ", $value) : $value;

            } else {

                $eventxtra->data[$field->meta->eventxtra_field_id] = is_array($value)? implode('|', $value) : $value;

            }

        }

        //Book Collection
        if($rea->book_collection && $form->meta->enable_book_collection && $field->meta->book_field_name){

            $meta->{'book_'.$field->meta->book_field_name} = $value;

        }

		if($field->meta->is_encrypted){

            $value = rea_form_encrypt($field->ID, $value);

		}

		$fields[$fid] = $value;

    }

	if($form->meta->emarsys_optin_field){

		$emarsys->optin = false;

		if($fields[$form->meta->emarsys_optin_field]) $emarsys->optin = true;

    }

	$save = (object) array(
		'post' => (object) array(
			'ID' => PostNew(),
			'post_type' => 'rea_form_submision',
			'post_status' => 'publish',
		),
		'meta' => $meta,
		'form' => $fields
    );

    $save->post->post_title = str_pad($save->post->ID, 8, "0", STR_PAD_LEFT);

	if($emarsys->formId && $submit){
		$save->meta->emarsys_response = json_encode($submit);
		$save->meta->emarsys_formId = $emarsys->formId;
	}

	if($data['votes']){
		$save->meta->votes = $data['votes'];
		$output['hasVotes'] = true;
    }

    if($um && $um->hasData && $um->endpoint){

        $save->meta->um_response = rea_um_form_submit($um);

        if(ENV !== "prod") $output['um'] = $save->meta->um_response;

    }

    PostUpdate($save);

    if($emarsys->formId && $emarsys->endpoint && $emarsys->optin){

        $url = $emarsys->endpoint . "/api/v2/contact/?create_if_not_exists=1";

        $auth = rea_form_emarsys_auth($emarsys->username, $emarsys->secret);

        $args = array(
            'sslverify' => false,
            'method' => 'PUT',
            'headers' => array(
                'X-WSSE' => $auth,
                'content-type' => 'application/json;charset="utf-8"'
            ),
            'body' => json_encode($emarsys->fields)
        );

		$submit = wp_remote_post($url, $args);

		if(is_array($submit)){

			if(wp_remote_retrieve_response_code($submit) == 200){

                $output['emarsys'] = 'sent';

                if($emarsys->fields[3]){

                    $url = $emarsys->endpoint . "/api/v2/form/".$emarsys->formId."/trigger";

                    $auth = rea_form_emarsys_auth($emarsys->username, $emarsys->secret);

                    $args = array(
                        'sslverify' => false,
                        'method' => 'POST',
                        'headers' => array(
                            'X-WSSE' => $auth,
                            'content-type' => 'application/json;charset="utf-8"'
                        ),
                        'body' => json_encode((object) array("key_id" => 3, "external_id" => $emarsys->fields[3]))
                    );

            		$submitTrigger = wp_remote_post($url, $args);

                    if(ENV !== "prod" || is_user_logged_in()){
                        $output['emarsysTrigger'] = $submitTrigger['body'];
                    }

                }

			} else {

                $output['emarsys'] = 'failed';

			}

            if(ENV !== "prod" || is_user_logged_in()){
                $output['emarsysFields'] = $emarsys->fields;
                $output['emarsysResponse'] = $submit['body'];
            }

			//pr($submit, 'debug');

		} else {

            if(ENV !== "prod" || is_user_logged_in()) $output['emarsysResponse'] = $submit;
			//pr($submit, 'error');

		}

    }

    if($eventxtra){

        $eventxtra->data['id'] = $save->post->ID;

        $args = array(
            'data_format' => 'body',
            'timeout' => 10,
            'sslverify' => false,
            'headers' => array('Authorization' => 'Bearer '.$eventxtra->token, 'Content-Type' => 'application/json'),
            'body' => json_encode(array('attendees' => array($eventxtra->data)))
        );

        $submit = wp_remote_post($eventxtra->endpoint, $args);

		if(is_array($submit)){

			if(wp_remote_retrieve_response_code($submit) == 200){
				$output['eventxtra'] = 'sent';
			} else {
				$output['eventxtra'] = 'failed';
			}

            update_post_meta($save->post->ID, 'eventxtra_status', wp_remote_retrieve_response_code($submit));
            update_post_meta($save->post->ID, 'eventxtra_response', $submit['body']);

			// pr($submit['response']);
			// pr($submit['body']);
            // pr($eventxtra->data, "END");

		} else {

			// pr($submit, 'error');

		}

	}

	if($data['votes']){

		foreach($save->meta->votes as $vote){
			$vote['form_id'] = $form->ID;
            $vote['form_submission_id'] = $save->post->ID;
            //$vote['form_hash'] = $votehash; // changed to entry hash meta on post object
			$vote['date'] = current_time('mysql');
			$wpdb->insert($wpdb->reavotes, $vote);
		}

	}

	$output['id'] = $save->post->ID;

	$table = "<div class='fields'>";

		$values = rea_form_get_values($save->post->ID, false, false);

		foreach($values as $k => $v){
			$table .= "<div class='field'>";
			$table .= "<div class='label'>{$v['label']}</div>";
			$table .= "<div class='value'>".nl2br($v['value'])."</div>";
			$table .= "</div>";
		}

	$table .= "</div>";


	//send email to admin
	if($form->meta->enable_admin_email && $form->meta->admin_mail_to){

        $msg = $form->meta->admin_mail_message != "Array"? $form->meta->admin_mail_message : "";

		$body = file_get_contents(locate_template('forms/email-template.html'));
		$body = str_replace("{{message}}", _html($msg), $body);
		$body = str_replace("{{fields}}",  $form->meta->admin_mail_fields? $table : "", $body);

		$mail = wp_mail($form->meta->admin_mail_to, $form->meta->admin_mail_subject, $body);

	}

	//send email to user
	if($form->meta->enable_user_email && $email){

        $msg = $form->meta->user_mail_message != "Array"? $form->meta->user_mail_message : "";

		$msg = str_replace(array("{SMS_CODE}", "{LOCATION}"), array($meta->sms_code, $meta->book_location), $msg);

		$body = file_get_contents(locate_template('forms/email-template.html'));
		$body = str_replace("{{message}}", _html($msg), $body);
        $body = str_replace("{{fields}}",  $form->meta->user_mail_fields? $table : "", $body);

		$mail = wp_mail($email, $form->meta->user_mail_subject, $body);

    }

	//send sms to user
	if($form->meta->enable_user_sms && $form->meta->sms_message && $phone){

        $msg = $form->meta->sms_message;

		$msg = str_replace(array("{SMS_CODE}", "{LOCATION}"), array($meta->sms_code, $meta->book_location), $msg);

        rea_sms_send($phone, $msg);

	}

	echo json_encode($output);
	rea_close_and_exit();

}


if(isset($_POST['rea-form-upload'])) add_action('init', 'rea_form_upload_file');
function rea_form_upload_file(){

	global $wpdb, $rea;

	$data = $_POST? : array();
	$files = $_FILES? : array();
    $form = _post($data['rea-form-upload']);

	$id = rea_form_save_file('file', $form->ID);

    echo json_encode( array("id" => $id));
    rea_close_and_exit();

}

function rea_form_save_file($file, $formId){

	if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) return 0;

	$mime = mime_content_type($_FILES[$file]["tmp_name"]);

	$allowed = array(
		"image/png",
		"image/gif",
		"image/jpeg",
		"application/pdf",
	);

	if(!in_array($mime, $allowed)) return 0;

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
	require_once(ABSPATH . "wp-admin" . '/includes/file.php');
	require_once(ABSPATH . "wp-admin" . '/includes/media.php');

	$id = media_handle_upload( $file, $formId );

	if(is_wp_error($id)) return 0;

	update_post_meta($id, 'user_upload', 1);

	return $id;

}


function rea_eventxtra_token(){

    global $rea;

    if(!$rea->region->eventxtra) return;

    $token = get_transient('eventXtraToken');

    if($token) return $token;

    $url = $rea->region->eventxtra->endpoint."oauth/token";

    $req = wp_remote_post($url, array("body" => $rea->region->eventxtra->credentials));

    if($req['body']){

        $body = json_decode($req['body'], true);

        $token = $body['access_token'];

        set_transient('eventXtraToken', $token, strtotime('+1 day'));

    }

    return $token? : "tokenNotAvailable";

}

function getRealIpAddr(){

    if(!empty($_SERVER['HTTP_CLIENT_IP'])) {

        $ip = $_SERVER['HTTP_CLIENT_IP'];

    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

    } else {

        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;

}

function rea_form_get_values($id, $includeMeta = false, $includePrivate = false, $deliminator = ",", $filter = "display"){

	$post = _post($id, 0, 0);
	$form = _post($post->meta->formId);

	$values = array();

	if($includeMeta){

		$values[] = array(
			'label' => 'Form',
			'value' => qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($form->post_title)
		);

		$values[] = array(
			'label' => 'Date',
			'value' => date_i18n("d F Y, g:i a", strtotime($post->post_date))
		);

	}

	foreach($post->form? : array() as $fid => $value){

		$field = _post($fid);

		if(!$field) continue;
		//if(!$includePrivate && $field->meta->is_pii) continue;

		if($field->meta->is_encrypted){
			$value = rea_form_decrypt($field->ID, $value);
		}

		if($field->meta->type == "file" && $value){
			$filename = basename(get_attached_file($value));
			$url = wp_get_attachment_url($value);
			if($filter == "raw") $value = $url;
			else $value = "<a target='_blank' href='{$url}'>{$filename}</a>";
		}

		if(is_array($value)) $value = implode($deliminator, $value);

		//mask private fields
		if($field->meta->is_pii){

			if(!current_user_can('read_pii')) $value = "-- pii masked --";
			if(!$includePrivate) $value = "-- pii masked --";

		}


		if($field->meta->type == "data"){

            $value = json_decode($value);

            if($value){

                if($value->type === "listing"){

                    $columns = array(
                        "id" => "Listing ID",
                        "developer" => "Developer",
                        "project_name" => "Project Name",
                        "property_type" => "Property Type",
                        "location" => "Location",
                        "bedrooms" => "Bedrooms",
                        "bathrooms" => "Bathrooms",
                        "carparks" => "Carparks",
                        "storeys" => "Storeys",
                        "price" => "Price",
                        "built_up" => "Built-up",
                        "land_size" => "Land Size",
                    );

                }

                if(!$columns) continue;

                if($filter === "raw"){

                    // foreach($columns as $key => $label){

                    //     $data = array();

                    //     foreach($value->data as $n => $item){

                    //         $data[] = $item->$key;

                    //     }

                    //     $values[] = array(
                    //         'fid'	=> $field->ID,
                    //         'isData' => true,
                	// 		'label' => apply_filters('translate_text', $label),
                	// 		'value' => $data
                    //     );

                    // }

                    $data = array();

                    foreach($value->data as $n => $item){

                        $data[$n] = (object) array();

                        foreach($columns as $key => $label){
                            $data[$n]->$label = $item->$key;
                        }

                    }

                    $values[] = array(
                        'fid'	=> $field->ID,
            			'label' => apply_filters('translate_text', $field->post_title),
                        'isData' => true,
            			'columns' => $columns,
            			'value' => $data
                    );

                } else {

                    $str = "";

                    foreach($value->data as $n => $item){

                        if($n) $str .= "<hr />";

                        foreach($columns as $key => $label){

                            $str .= "<strong>{$label}</strong>: {$item->$key}\n";

                        }

                    }

                    $values[] = array(
            			'fid'	=> $field->ID,
            			'label' => apply_filters('translate_text', $field->post_title),
            			'value' => $str
                    );

                }

            }

        } else {

    		$values[] = array(
    			'fid'	=> $field->ID,
    			'label' => apply_filters('translate_text', $field->post_title),
    			'value' => $value
            );

        }

	}

	foreach($post->votes? : array() as $vote){

		$values[] = array(
			'vote' => 1,
			'label' => $vote['title'],
			'value' => implode("<br /> - ", array_filter(array($vote['candidate'], $vote['reason'])))
		);

	}

	if($includeMeta){

		foreach(array('user-agent' => "User Agent", 'referrer' => "Referrer", 'ip' => "IP Address", 'emarsys_response' => 'Emarsys HTTP Response', 'tol_response' => 'TOL Book API Response') as $key => $label){

			if(!$post->meta->{$key}) continue;

			$values[] = array(
				'label' => $label,
				'value' => $post->meta->{$key}
			);

		}

	}

	return $values;

}

function rea_form_get_values_split_data($values) {

    $split = array();

    $data = array();
    $hasData = false;

    foreach($values as $column){

        if($column['isData']){

            $hasData = true;

            foreach($column['value'] as $k => $v){

                $data[$k][$column['fid']] = $v;

            }

        }

    }

    if($hasData){

        foreach($data as $k => $dataSet){

            $row = array();

            foreach($values as $column){

                if($column['isData']){

                    $column['value'] = $dataSet[$column['fid']];

                    $row[] = $column;

                } else {

                    $row[] = $column;

                }

            }

            $split[] = $row;

        }

    } else {

        $split[] = $values;

    }

    return $split;

}


add_filter('acf/get_fields', 'rea_form_data_acf_get_fields', 9, 2);
function rea_form_data_acf_get_fields($fields, $group){

	if($_GET['post']){

		if($group['title'] == 'Form Data'){

			$post = _post($_GET['post']);

			$dataField = null;

			foreach($fields as $key => $field){

				if($field['label'] == "{formDataLabel}"){
					$dataField = $field;
					unset($fields[$key]);
				}

			}

			if($dataField){

				$values = rea_form_get_values($post->ID, true, true);

				foreach($values as $k => $v){

                    if($v['label'] == "TOL Book API Response") $v['value'] = json_encode($v['value']);

					$newFeild = $dataField;
					$newFeild['label'] = $v['label'];
					$newFeild['message'] = $v['value'];
					$fields[] = $newFeild;

				}

			}

        }

        if(!is_super_admin() && $group['title'] == 'REA Form Field'){

            foreach($fields as $key => $field){

				if($field['name'] == "um_field_name"){
					unset($fields[$key]);
				}

			}

        }

	}

	return $fields;

}

function rea_debounce_verify($email, $key){

    global $rea;

    if(!$email) return false;

    $req = wp_remote_get("https://api.debounce.io/v1/?api=".$key."&email=".$email, array('sslverify' => false));

    if(is_wp_error($req)) return false;

    $res = json_decode(wp_remote_retrieve_body($req));

    if($res){

        if($res->debounce && $res->debounce->code && in_array($res->debounce->code, array('4', '5', '7', '8'))){

            return true;

        }

    }

    return false;

}

add_action('admin_init', 'rea_form_field_protect');
function rea_form_field_protect(){

    if($_GET['post']){

        $post = _post($_GET['post']);

        if($post->post_type === "rea_form_field" && $post->meta->um_field_name && !is_super_admin()){

            wp_rea_close_and_exit('Super Admin Access Required To Edit This Field');

        }

    }

}

function rea_um_form_submit($um){

    require_once("graphql.php");

    if(count($um->workshops)){

        $responses = [];

        $workshops = $um->workshops;

        $um->workshops = null;

        $_um = clone $um;

        foreach($workshops as $workshop){

            $_um->data["eventProperties"] = $workshop;

            $responses[] = rea_graphql_subscribe($_um);

        }

        return $responses;

    } else {

        return rea_graphql_subscribe($um);

    }

}

function rea_form_emarsys_auth($username, $secret){

    $nonce = wp_generate_password(32, false, false); //'d36e316282959a9ed4c89851497a717f';

    $timestamp = gmdate("c");

    $passwordDigest = base64_encode(sha1($nonce . $timestamp . $secret, false));

    $str = "UsernameToken Username=\"${username}\", PasswordDigest=\"${passwordDigest}\", Nonce=\"${nonce}\", Created=\"${timestamp}\"";

    return $str;

}
